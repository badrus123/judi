(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["/js/app"],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_navHeader_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/navHeader.vue */ "./resources/js/components/navHeader.vue");
/* harmony import */ var _components_navFooter_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/navFooter.vue */ "./resources/js/components/navFooter.vue");
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  components: {
    navHeader: _components_navHeader_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    navFooter: _components_navFooter_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/authTooltip.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cardCarouser.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_donutChart_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/donutChart.vue */ "./resources/js/components/donutChart.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
 // import doughnutChart from '../components/doughnutChart.js'

/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Carousel',
  components: {
    donutChart: _components_donutChart_vue__WEBPACK_IMPORTED_MODULE_0__["default"] // doughnutChart

  },
  data: function data() {
    return {
      sections: [{
        label: 'Win',
        value: 100 - this.rateWin,
        color: '#DE0035'
      }, {
        label: 'Lose',
        value: this.rateWin,
        color: '#48C364'
      }]
    };
  },
  props: {
    titles: String,
    imageUrl: String,
    bgColor: String,
    rateWin: Number
  },
  //   watch: {
  //     rateWin(val) {
  //      this.sections.value = val;
  //    }
  //   },
  computed: {
    bgStyles: function bgStyles() {
      return {
        "background-color": this.bgColor
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donutChart.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      rate1: '.' + this.rateDay,
      ratemin: 100 - this.rateDay,
      rate2: '.' + (100 - this.rateDay)
    };
  },
  props: {
    rateDay: Number
  },
  computed: {// numberCount() {
    //   return
    //     {--first; '.${this.rateDay}'}
    //     // --second: .'+100-this.rateDay+';'
    // },
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/inviteRaf.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      inviteLink: 'www.huatbet.com/F35289494',
      affCode: 'F35289494',
      games: [{
        name: 'Dice',
        commision: 0.15
      }, {
        name: 'Gor Ki Pi',
        commision: 0.15
      }, {
        name: 'Mines',
        commision: 0.15
      }, {
        name: 'Moonshot',
        commision: 0.15
      }, {
        name: 'Si Ki Pi',
        commision: 0.15
      }]
    };
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/modalPocket.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      succesDraw: false,
      modalVal: false,
      depValue: "",
      amValue: "",
      tokenName: "Algo",
      userWallets: [],
      selectedToken: 0,
      withdrawAddress: "",
      withdrawAmount: 0,
      withdrawalLoading: false,
      isError: false,
      errorMessage: "",
      headerNames: ["Deposit", "Withdraw"],
      selectedTab: 0,
      tokenList: [// {
        //   name: "algo",
        //   icon: "",
        //   address: "",
        // },
        // {
        //   name: "BTC",
        //   icon: "",
        //   address: "",
        // },
        // {
        //   name: "ETH",
        //   icon: "",
        //   address: "",
        // },
        // {
        //   name: "usdt",
        //   icon: "",
        //   address: "",
        // },
      ]
    };
  },
  created: function created() {
    var _this = this;

    return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
      return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              // Get User Balances
              _this.$store.dispatch("wallet/GET_BALANCES"); // Get user wallets


              _this.$store.dispatch("wallet/GET_DEPOSIT_ADDRESS"); // Get Minimal Withdraw


              _this.$store.dispatch("wallet/GET_MINIMUM_WITHDRAW", _this.tokenName);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }))();
  },
  methods: {
    changeModal: function changeModal() {
      this.modalVal = true;
    },

    /**
     * When tab changed
     * from deposit to withdrawal, vice versa
     */
    tabChanged: function tabChanged(value) {
      this.selectedTab = value; // When user click withdrawal, get withdrawal fees

      if (value == 1) this.$store.dispatch("wallet/GET_WITHDRAWAL_FEES", this.tokenName);
    },

    /**
     * Copy the deposit address
     */
    copy: function copy() {
      var testing = document.querySelector("#deposit-address");
      testing.setAttribute("type", "text");
      testing.select();
      document.execCommand("copy");
    },
    modHide: function modHide() {
      this.modalVal = false;
      this.succesDraw = false;
    },

    /**
     * Do the withdrawal
     */
    withdraw: function withdraw() {
      var _this2 = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var result;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _this2.withdrawalLoading = true; // Dispatch the withdrawal

                _context2.next = 3;
                return _this2.$store.dispatch("wallet/WITHDRAW", {
                  address: _this2.withdrawAddress,
                  amount: _this2.withdrawAmount,
                  currency: _this2.tokenName.toUpperCase()
                });

              case 3:
                result = _context2.sent;
                _this2.withdrawalLoading = false; // Get the error (if exists)

                _this2.isError = _this2.$store.state.wallet.isError;
                _this2.errorMessage = _this2.$store.state.wallet.errorMessage; // If transaction success

                if (!_this2.isError) {
                  _this2.succesDraw = true;
                }

              case 8:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },

    /**
     * Fetch the amount that the user has
     * for the specified token,
     * Then fill it in amount input
     */
    maxAmount: function maxAmount() {
      var balance = this.$store.getters["wallet/balance"](this.tokenName);
      this.withdrawAmount = balance.balance;
    },

    /**
     * When user change the token
     */
    changeDepositToken: function changeDepositToken(index) {
      this.selectedToken = index; // Set Address

      this.depValue = this.tokenList[index].address; // Set Token Name

      this.tokenName = this.tokenList[index].name; // Get Minimal Withdraw

      this.$store.dispatch("wallet/GET_MINIMUM_WITHDRAW", this.tokenName); // Get Withdrawal Fees

      this.$store.dispatch("wallet/GET_WITHDRAWAL_FEES", this.tokenName);
    }
  },
  watch: {
    /**
     * When User Wallet fetched
     * parse it to the token list table
     */
    userWallet: function userWallet(value) {
      this.userWallets = value;

      for (var property in this.userWallets) {
        this.tokenList.push({
          name: property,
          icon: "",
          address: this.userWallets[property]
        });
      }

      this.depValue = this.tokenList[this.selectedToken].address;
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    userWallet: "wallet/wallet"
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navFooter.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {};
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navHeader.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modalPocket_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modalPocket.vue */ "./resources/js/components/modalPocket.vue");
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      isViewedInUsd: false,
      tokenValue: "0.00000000",
      selectedToken: 0,
      tokenList: [// {
        //   value: "0.00000000",
        //   name: "algo",
        //   icon: "",
        // },
        // {
        //   value: "0.00000000",
        //   name: "BTC",
        //   icon: "",
        // },
        // {
        //   value: "0.00000000",
        //   name: "ETH",
        //   icon: "",
        // },
        // {
        //   value: "0.00000000",
        //   name: "usdt",
        //   icon: "",
        // },
      ]
    };
  },
  created: function created() {
    var _this2 = this;

    /**
     * If Auth
     */
    if (this.$store.state.isAuth) {
      // Interval every 1 minute, to get user balance
      window.setInterval(function () {
        _this2.$store.dispatch("wallet/GET_BALANCES");
      }, 60000); // Get Websocket token

      this.$store.dispatch("WEBSOCKET");
    }
  },
  watch: {
    balances: function balances() {
      this.tokenList = this.parseBalances();
    },
    isViewedInUsd: function isViewedInUsd(value) {
      this.tokenList = this.parseBalances();
    },
    websocketToken: function websocketToken(value) {
      if (value == null || value == undefined) return;
      window.swoole = new WebSocket("wss://socket.algofair.io/ws?access_token=" + value + "&session_id=" + this.$store.state.websocket_session);

      window.swoole.onopen = function (event) {
        window.interval = setInterval(function () {
          window.swoole.send("PING");
        }, 20000);
      };

      window.swoole.onclose = function (event) {
        clearInterval(window.interval);
      };

      var _this = this;

      window.swoole.onmessage = function (event) {
        try {
          if (event.data != "PONG") {
            var obj = JSON.parse(event.data);

            if (obj.action != undefined && obj.action == "updateBalances") {
              console.log("WEBOCKSET UDPAT EBALANCE");

              _this.$store.dispatch("wallet/GET_BALANCES");
            }
          }
        } catch (e) {
          console.log(e);
        }
      };
    },
    isAuth: function isAuth(value) {
      var _this3 = this;

      if (value) {
        window.setInterval(function () {
          _this3.$store.dispatch("wallet/GET_BALANCES");
        }, 60000); // Get Websocket token

        this.$store.dispatch("WEBSOCKET");
      }
    }
  },
  components: {
    modalDeposit: _modalPocket_vue__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {
    /**
     * Get User Balances
     * and parse to the table
     */
    parseBalances: function parseBalances() {
      var _this4 = this;

      var tokenList = [];
      this.balances.forEach(function (balance) {
        tokenList.push({
          value: _this4.isViewedInUsd ? parseFloat(balance.converted).toFixed(8) : parseFloat(balance.balance).toFixed(8),
          name: balance.token,
          icon: ""
        });
      });
      return tokenList;
    },

    /**
     * When user change/click the token
     */
    clickToken: function clickToken(index) {
      this.selectedToken = index;
    },

    /**
     * Dispatch Logout Action
     * inside auth module
     */
    logout: function logout() {
      this.$store.dispatch("auth/LOGOUT", {
        redirectTo: "/"
      });
    },
    transaction: function transaction() {
      this.$router.push({
        name: "Transactions"
      });
    },
    referFriend: function referFriend() {
      this.$router.push({
        name: "ReferFriend"
      });
    },
    settings: function settings() {
      this.$router.push({
        name: "Settings"
      });
    },
    openDeposit: function openDeposit() {
      this.$refs.showdep.changeModal();
    }
  },
  computed: _objectSpread({
    /**
     * Get Token Balance selected by user
     */
    getTokenbalance: function getTokenbalance() {
      if (this.tokenList.length != 0) {
        return parseFloat(this.tokenList[this.selectedToken].value).toFixed(8);
      }

      return "0.00000000";
    }
  }, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    balances: "wallet/balances",
    websocketToken: "websocket_token",
    isAuth: "isAuth"
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/passwordSetting.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      passNew: 'test',
      passNow: 'test',
      passConfirm: 'test'
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_chartStats_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/chartStats.js */ "./resources/js/components/chartStats.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      chartData: [20, 30, 10, 80, 40, 70],
      chartLabels: ['January', 'February', 'March', 'April', 'May', 'June']
    };
  },
  components: {
    chartStats: _components_chartStats_js__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=script&lang=js&":
/*!************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsReferrer.vue?vue&type=script&lang=js& ***!
  \************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_chartStats_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/chartStats.js */ "./resources/js/components/chartStats.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      chartData: [40, 39, 10, 40, 39, 80],
      chartLabels: ['January', 'February', 'March', 'April', 'May', 'June']
    };
  },
  components: {
    chartStats: _components_chartStats_js__WEBPACK_IMPORTED_MODULE_0__["default"]
  },
  methods: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/tableHome.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Table',
  data: function data() {
    return {
      columns: ['game', 'user', 'time', 'bet', 'multiplier', 'wl'],
      columnsMob: ['game', 'user', 'wl'],
      options: {
        headings: {
          game: 'Game',
          user: 'User',
          time: 'Time',
          bet: 'Bet',
          multiplier: 'Multiplier',
          wl: 'Win/Loss'
        },
        sortable: [],
        filterable: false,
        perPage: 10,
        pagination: {
          show: false,
          chunk: 4,
          dropdown: false,
          nav: 'fixed'
        },
        sortIcon: {
          base: 'fa',
          is: 'fa-sort',
          up: 'fa-sort-asc',
          down: 'fa-sort-desc'
        },
        showChildRowToggler: true,
        childRowTogglerFirst: true
      },
      optionsMob: {
        headings: {
          game: 'Game',
          user: 'User',
          wl: 'Win/Loss'
        },
        sortable: [],
        filterable: false,
        perPage: 10,
        pagination: {
          show: false,
          chunk: 4,
          dropdown: false,
          nav: 'fixed'
        },
        sortIcon: {
          base: 'fa',
          is: 'fa-sort',
          up: 'fa-sort-asc',
          down: 'fa-sort-desc'
        },
        showChildRowToggler: true,
        childRowTogglerFirst: true
      },
      tableData: [{
        game: 'Si Ki Pi',
        user: 'abcd1234',
        time: '11:38:00',
        bet: 0.32000000,
        multiplier: '0.00x',
        wl: -0.32000000
      }]
    };
  },
  mounted: function mounted() {
    while (this.tableData.length < 30) {
      this.tableData.push({
        game: 'Si Ki Pi',
        user: 'abcd1234',
        time: '11:38:00',
        bet: 0.32000000,
        multiplier: '0.00x',
        wl: 0.32000000
      });
    }
  },
  components: {//HelloWorld
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/telegramSetting.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      activeTele: false,
      linkTele: 'https://t.me/huatbet?start=5BBDzU3F0964B7GPjbfuG8',
      codeqr: '5BBDzU3F0964B7GPjbfuG8',
      selectLang: 1,
      optionsLang: [{
        value: 1,
        text: 'English'
      }, {
        value: 2,
        text: 'Spanish'
      }, {
        value: 3,
        text: 'Arabic'
      }]
    };
  },
  methods: {
    actifedTele: function actifedTele() {
      this.activeTele = true;
    },
    cancelTele: function cancelTele() {
      this.activeTele = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      activeTfa: false,
      codeqr: 'DV2SJDIAXH3DKYCD',
      otpCode: '',
      confirmCode: 'not_accepted'
    };
  },
  methods: {
    actifedTfa: function actifedTfa() {
      this.activeTfa = true;
    },
    cancelTfa: function cancelTfa() {
      this.activeTfa = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userProfile.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  data: function data() {
    return {
      titleCard: "User profile",
      selected: 1,
      userName: "",
      displayName: "",
      status: "0",
      selectTime: "-17",
      optionsTime: [{
        value: -12,
        text: "(GMT -12:00) Eniwetok, Kwajalein"
      }, {
        value: -11,
        text: "(GMT -11:00) Midway Island, Samoa"
      }, {
        value: -10,
        text: "(GMT -10:00) Hawaii"
      }, {
        value: -9,
        text: "(GMT -9:00) Alaska"
      }, {
        value: -8,
        text: "(GMT -8:00) Pacific Time (US & Canada)"
      }, {
        value: -7,
        text: "(GMT -7:00) Mountain Time (US & Canada)"
      }, {
        value: -6,
        text: "(GMT -6:00) Central Time (US & Canada), Mexico City"
      }, {
        value: -5,
        text: "(GMT -5:00) Eastern Time (US & Canada), Bogota, Lima"
      }, {
        value: -4,
        text: "(GMT -4:00) Atlantic Time (Canada), Caracas, La Paz"
      }, {
        value: -3,
        text: "(GMT -3:00) Brazil, Buenos Aires, Georgetown"
      }, {
        value: -2,
        text: "(GMT -2:00) Mid-Atlantic"
      }, {
        value: -1,
        text: "(GMT -1:00) Azores, Cape Verde Islands"
      }, {
        value: +0,
        text: "(GMT) Western Europe Time, London, Lisbon, Casablanca"
      }, {
        value: +1,
        text: "(GMT +1:00) Brussels, Copenhagen, Madrid, Paris"
      }, {
        value: +2,
        text: "(GMT +2:00) Kaliningrad, South Africa"
      }, {
        value: +3,
        text: "(GMT +3:00) Baghdad, Riyadh, Moscow, St. Petersburg"
      }, {
        value: +4,
        text: "(GMT +4:00) Abu Dhabi, Muscat, Baku, Tbilisi"
      }, {
        value: +5,
        text: "(GMT +5:00) Ekaterinburg, Islamabad, Karachi, Tashkent"
      }, {
        value: +6,
        text: "(GMT +6:00) Almaty, Dhaka, Colombo"
      }, {
        value: +7,
        text: "(GMT +7:00) Bangkok, Hanoi, Jakarta"
      }, {
        value: +8,
        text: "(GMT +8:00) Beijing, Perth, Singapore, Hong Kong"
      }, {
        value: +9,
        text: "(GMT +9:00) Tokyo, Seoul, Osaka, Sapporo, Yakutsk"
      }, {
        value: +10,
        text: "(GMT +10:00) Eastern Australia, Guam, Vladivostok"
      }, {
        value: +11,
        text: "(GMT +11:00) Magadan, Solomon Islands, New Caledonia"
      }, {
        value: +12,
        text: "(GMT +12:00) Auckland, Wellington, Fiji, Kamchatka"
      }, {
        value: +13,
        text: "(GMT +13:00) Apia, Nukualofa"
      }, {
        value: +14,
        text: "(GMT +14:00) Line Islands, Tokelau"
      }]
    };
  },
  created: function created() {
    this.$store.dispatch("profile/GET_PROFILE");
  },
  methods: {
    update: function update() {
      this.$store.dispatch("profile/UPDATE_PROFILE", {
        displayName: this.displayName,
        privacy: this.status,
        timezone: this.selectTime
      });
    }
  },
  watch: {
    username: function username(value) {
      this.userName = value;
    },
    display_name: function display_name(value) {
      this.displayName = value;
    },
    timezone: function timezone(value) {
      this.selectTime = value;
    },
    privacy: function privacy(value) {
      if (value) this.status = "1";else this.status = "0";
    }
  },
  computed: _objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_0__["mapGetters"])({
    username: "profile/username",
    display_name: "profile/displayName",
    timezone: "profile/timezone",
    privacy: "profile/privacy"
  }))
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_tableHome_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/tableHome.vue */ "./resources/js/components/tableHome.vue");
/* harmony import */ var _components_cardCarouser_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/cardCarouser.vue */ "./resources/js/components/cardCarouser.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Dashboard',
  components: {
    tableHome: _components_tableHome_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    cardCarousel: _components_cardCarouser_vue__WEBPACK_IMPORTED_MODULE_1__["default"] // owlCarousel

  },
  data: function data() {
    return {
      slide: 0,
      sliding: null
    };
  },
  methods: {
    onSlideStart: function onSlideStart() {
      this.sliding = true;
    },
    onSlideEnd: function onSlideEnd() {
      this.sliding = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=script&lang=js&":
/*!*********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Faq.vue?vue&type=script&lang=js& ***!
  \*********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Faq',
  components: {},
  data: function data() {
    return {
      selected: 1,
      options: [{
        value: 1,
        text: 'General'
      }, {
        value: 2,
        text: 'Option 2'
      }, {
        value: 3,
        text: 'Option 3'
      }],
      value: ''
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js&":
/*!********************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js& ***!
  \********************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Forgot",
  data: function data() {
    return {
      form: {
        name: "",
        pass: ""
      },
      show: true,
      isLoading: false
    };
  },
  methods: {
    onSubmit: function onSubmit(event) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                event.preventDefault();
                _this.isLoading = true;
                _context.next = 4;
                return _this.$store.dispatch("auth/FORGOT_PASSWORD", {
                  username: _this.form.name
                });

              case 4:
                _this.isLoading = false;

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=script&lang=js&":
/*!**********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Home.vue?vue&type=script&lang=js& ***!
  \**********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_tableHome_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/tableHome.vue */ "./resources/js/components/tableHome.vue");
/* harmony import */ var _components_cardCarouser_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/cardCarouser.vue */ "./resources/js/components/cardCarouser.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
// import owlCarousel from 'vue-owl-carousel'


/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Home',
  components: {
    tableHome: _components_tableHome_vue__WEBPACK_IMPORTED_MODULE_0__["default"],
    cardCarousel: _components_cardCarouser_vue__WEBPACK_IMPORTED_MODULE_1__["default"] // owlCarousel

  },
  data: function data() {
    return {
      slide: 0,
      sliding: null
    };
  },
  methods: {
    onSlideStart: function onSlideStart() {
      this.sliding = true;
    },
    onSlideEnd: function onSlideEnd() {
      this.sliding = false;
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'HowToPlay',
  data: function data() {
    return {
      selected: 1,
      options: [{
        value: 1,
        text: 'Dice'
      }, {
        value: 2,
        text: 'Si Ki Pi'
      }, {
        value: 3,
        text: 'Moon Shot'
      }],
      value: ''
    };
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=script&lang=js&":
/*!***********************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login.vue?vue&type=script&lang=js& ***!
  \***********************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_authTooltip_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/authTooltip.vue */ "./resources/js/components/authTooltip.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  components: {
    authTooltip: _components_authTooltip_vue__WEBPACK_IMPORTED_MODULE_1__["default"]
  },
  name: "Login",
  data: function data() {
    return {
      form: {
        name: "",
        pass: ""
      },
      loading: false,
      errorMessage: "",
      show: true,
      isError: true
    };
  },
  created: function created() {},
  methods: {
    onSubmit: function onSubmit(event) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                event.preventDefault(); // Check if the form is filled

                if (!(_this.form.name && _this.form.pass)) {
                  _context.next = 8;
                  break;
                }

                _this.loading = true; // Dispatch

                _context.next = 5;
                return _this.$store.dispatch("auth/LOGIN", {
                  username: _this.form.name,
                  password: _this.form.pass
                });

              case 5:
                _this.loading = false;
                _this.isError = _this.$store.state.auth.isError;
                _this.errorMessage = _this.$store.state.auth.errorMessage;

              case 8:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login2fa.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Login",
  data: function data() {
    return {
      form: {
        pass: ""
      },
      username: "",
      password: "",
      show: true,
      isError: true,
      errorMessage: ""
    };
  },
  created: function created() {
    // Get credentials from Route Params
    this.username = this.$route.params.username;
    this.password = this.$route.params.password;

    if (this.username == null) {
      this.$router.push({
        name: "Login"
      });
    }
  },
  methods: {
    onSubmit: function onSubmit(event) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                event.preventDefault(); // Dispatch

                _context.next = 3;
                return _this.$store.dispatch("auth/LOGIN", {
                  username: _this.username,
                  password: _this.password,
                  secret: _this.form.pass
                });

              case 3:
                _this.loading = false;
                _this.isError = _this.$store.state.auth.isError;
                _this.errorMessage = _this.$store.state.auth.errorMessage;

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Provably-fair',
  components: {},
  data: function data() {
    return {
      serverHash: '0f597126a87bab30a21581aa47467d560f4d572a0',
      serverSeed: '0f597126a87bab30a21581aa47467d560f4d572a0',
      clientSeed: '0f597126a87bab30a21581aa47467d560f4d572a0',
      nonce: 98,
      selected: 1,
      options: [{
        value: 1,
        text: 'Crash'
      }, {
        value: 2,
        text: 'Option 2'
      }, {
        value: 3,
        text: 'Option 3'
      }],
      value: ''
    };
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_inviteRaf_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/inviteRaf.vue */ "./resources/js/components/inviteRaf.vue");
/* harmony import */ var _components_statsReferrer_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/statsReferrer.vue */ "./resources/js/components/statsReferrer.vue");
/* harmony import */ var _components_statsAffiliates_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/statsAffiliates.vue */ "./resources/js/components/statsAffiliates.vue");
/* harmony import */ var vuejs_tree__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! vuejs-tree */ "./node_modules/vuejs-tree/dist/vuejs-tree.common.js");
/* harmony import */ var vuejs_tree__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vuejs_tree__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Refer-A-Friend",
  components: {
    inviteRaf: _components_inviteRaf_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    statsReferrer: _components_statsReferrer_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    statsAffiliates: _components_statsAffiliates_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    Tree: vuejs_tree__WEBPACK_IMPORTED_MODULE_4___default.a
  },
  data: function data() {
    return {
      titleCard: 'Invite',
      referrer: false,
      selected: 1,
      selectTime: 1,
      optionsTime: [{
        value: 1,
        text: 'Year'
      }, {
        value: 2,
        text: 'Month'
      }, {
        value: 3,
        text: 'Days'
      }],
      selectTime2: 2,
      optionsTime2: [{
        value: 1,
        text: 'Year'
      }, {
        value: 2,
        text: 'Month'
      }, {
        value: 3,
        text: 'Days'
      }],
      netJoin: [{
        name: 'Jane Cooper',
        date: '01/01/2021',
        active: 'Yes'
      }, {
        name: 'John Smith',
        date: '01/01/2021',
        active: 'Yes'
      }, {
        name: 'Cameron Williamson',
        date: '01/01/2021',
        active: 'Yes'
      }, {
        name: 'Esther Howard',
        date: '01/01/2021',
        active: 'Yes'
      }, {
        name: 'Wade Warren',
        date: '01/01/2021',
        active: 'Yes'
      }],
      netAffiliates: [{
        name: 'Jonathan Kay',
        turn: '1,000,000.00',
        net: '1,000,000.00',
        commision: '1,000,000.00',
        date: '01/01/2021'
      }, {
        name: 'Z9018239',
        turn: '1,000,000.00',
        net: '1,000,000.00',
        commision: '1,000,000.00',
        date: '01/01/2021'
      }, {
        name: 'Cameron Williamson',
        turn: '1,000,000.00',
        net: '1,000,000.00',
        commision: '1,000,000.00',
        date: '01/01/2021'
      }, {
        name: 'Andy Wong',
        turn: '1,000,000.00',
        net: '1,000,000.00',
        commision: '1,000,000.00',
        date: '01/01/2021'
      }, {
        name: 'W3456789SD',
        turn: '1,000,000.00',
        net: '1,000,000.00',
        commision: '1,000,000.00',
        date: '01/01/2021'
      }],
      treeDisplayData: [{
        text: "Root 1",
        id: 1,
        checkable: false,
        selectable: false,
        nodes: [{
          text: "Child 1",
          id: 3,
          checkable: false,
          selectable: false,
          nodes: [{
            text: "Grandchild 1",
            id: 5,
            checkable: false,
            selectable: false
          }, {
            text: "Grandchild 2",
            id: 6,
            checkable: false,
            selectable: false
          }]
        }, {
          text: "Child 2",
          id: 4,
          checkable: false,
          selectable: false
        }]
      }, {
        text: "Root 2",
        id: 2,
        checkable: false,
        selectable: false
      }]
    };
  },
  computed: {
    myCustomStyles: function myCustomStyles() {
      return {
        tree: {
          height: "auto",
          maxHeight: "300px",
          overflowY: "visible",
          display: "inline-block"
        },
        row: {
          width: "200px",
          cursor: "pointer",
          child: {
            height: "35px"
          }
        },
        text: {
          style: {},
          active: {
            style: {
              "font-weight": "bold",
              color: "#2ECC71"
            }
          }
        }
      };
    },
    myCustomOptions: function myCustomOptions() {
      return {
        treeEvents: {
          expanded: {
            state: true
          },
          collapsed: {
            state: false
          },
          selected: {
            state: false,
            fn: this.mySelectedFunction
          },
          checked: {
            state: false,
            fn: this.myCheckedFunction
          }
        },
        events: {
          expanded: {
            state: true
          },
          selected: {
            state: true
          },
          checked: {
            state: true
          },
          editableName: {
            state: true,
            calledEvent: "expanded"
          }
        },
        addNode: {
          state: false,
          fn: this.addNodeFunction,
          appearOnHover: false
        },
        editNode: {
          state: false,
          fn: null,
          appearOnHover: false
        },
        deleteNode: {
          state: false,
          fn: this.deleteNodeFunction,
          appearOnHover: true
        },
        showTags: true
      };
    }
  },
  mounted: function mounted() {
    this.$refs["my-tree"].expandNode(1);
  },
  methods: {
    inviteRefer: function inviteRefer() {
      this.titleCard = "Invite";
      this.selected = 1;
    },
    statsRefer: function statsRefer() {
      this.titleCard = "stats";
      this.selected = 2;
    },
    myCheckedFunction: function myCheckedFunction(nodeId, state) {
      console.log("is ".concat(nodeId, " checked ? ").concat(state));
      console.log(this.$refs["my-tree"].getCheckedNodes("id"));
      console.log(this.$refs["my-tree"].getCheckedNodes("text"));
    },
    mySelectedFunction: function mySelectedFunction(nodeId, state) {
      console.log("is ".concat(nodeId, " selected ? ").concat(state));
      console.log(this.$refs["my-tree"].getSelectedNode());
    },
    deleteNodeFunction: function deleteNodeFunction(node) {
      var nodePath = this.$refs["my-tree"].findNodePath(node.id);
      var parentNodeId = nodePath.slice(-2, -1)[0];

      if (parentNodeId === undefined) {
        // 'root' node
        var nodeIndex = this.$refs["my-tree"].nodes.findIndex(function (x) {
          return x.id !== node.id;
        }) - 1;
        this.$refs["my-tree"].nodes.splice(nodeIndex, 1);
      } else {
        // child node
        var parentNode = this.$refs["my-tree"].findNode(parentNodeId);

        var _nodeIndex = parentNode.nodes.findIndex(function (x) {
          return x.id !== node.id;
        }) - 1;

        parentNode.nodes.splice(_nodeIndex, 1);
      }

      console.log("example: remove node", node.id);
    },
    addNodeFunction: function addNodeFunction(node) {
      var newNode = {
        text: "Grandchild 2",
        id: Math.floor(Math.random() * 100),
        checkable: false
      };
      console.log("example: add node", newNode);

      if (node.nodes === undefined) {
        // the node doesn't have childs
        // I use $set to ensure that VueJs detect the change
        this.$set(node, "nodes", [newNode]);
      } else {
        node.nodes.push(newNode);
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Register.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _components_authTooltip_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/authTooltip.vue */ "./resources/js/components/authTooltip.vue");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Register",
  data: function data() {
    return {
      form: {
        name: "",
        pass: "",
        conpass: "",
        code: ""
      },
      loading: false,
      show: true,
      isError: false,
      errorMessage: ""
    };
  },
  methods: {
    onSubmit: function onSubmit(event) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                event.preventDefault(); // Check the confirmation password

                if (!(!_this.form.conpass || _this.form.pass !== _this.form.conpass)) {
                  _context.next = 6;
                  break;
                }

                _this.loading = false;
                _this.isError = true;
                _this.errorMessage = "The password doesn't match.";
                return _context.abrupt("return");

              case 6:
                if (!(_this.form.name && _this.form.pass)) {
                  _context.next = 13;
                  break;
                }

                _this.loading = true; // Dispatch

                _context.next = 10;
                return _this.$store.dispatch("auth/REGISTER", {
                  username: _this.form.name,
                  password: _this.form.pass,
                  confirm_password: _this.form.conpass,
                  affiliate: _this.form.code == "" ? undefined : _this.form.code
                });

              case 10:
                _this.loading = false;
                _this.isError = _this.$store.state.auth.isError;
                _this.errorMessage = _this.$store.state.auth.errorMessage;

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js&":
/*!*******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js& ***!
  \*******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Forgot",
  data: function data() {
    return {
      form: {
        pass: "",
        conpass: ""
      },
      show: true,
      token: "",
      isLoading: false
    };
  },
  created: function created() {
    this.token = this.$route.params.token;
  },
  methods: {
    onSubmit: function onSubmit(event) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                event.preventDefault();
                _this.isLoading = true; // Reset the error

                _this.$store.commit("auth/SET_ERROR_RESET_PASSWORD", {
                  isError: false,
                  errorMessage: ""
                }); // Dispatch


                _context.next = 5;
                return _this.$store.dispatch("auth/RESET_PASSWORD", {
                  password: _this.form.pass,
                  confirm_password: _this.form.conpass,
                  token: _this.token
                });

              case 5:
                _this.isLoading = false;

              case 6:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    }
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=script&lang=js&":
/*!**************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Settings.vue?vue&type=script&lang=js& ***!
  \**************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var _components_userProfile_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/userProfile.vue */ "./resources/js/components/userProfile.vue");
/* harmony import */ var _components_passwordSetting_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/passwordSetting.vue */ "./resources/js/components/passwordSetting.vue");
/* harmony import */ var _components_twoFaSetting_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/twoFaSetting.vue */ "./resources/js/components/twoFaSetting.vue");
/* harmony import */ var _components_telegramSetting_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../components/telegramSetting.vue */ "./resources/js/components/telegramSetting.vue");
/* harmony import */ var _components_accountDeletionSetting_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../components/accountDeletionSetting.vue */ "./resources/js/components/accountDeletionSetting.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Transaction",
  data: function data() {
    return {
      titleCard: 'User profile',
      selected: 1
    };
  },
  methods: {
    userProfile: function userProfile() {
      this.titleCard = "User profile";
      this.selected = 1;
    },
    password: function password() {
      this.titleCard = "Password";
      this.selected = 2;
    },
    twoFA: function twoFA() {
      this.titleCard = "Two-factor authentication";
      this.selected = 3;
    },
    telegram: function telegram() {
      this.titleCard = "telegram";
      this.selected = 4;
    },
    accountDelete: function accountDelete() {
      this.titleCard = "Account deletion";
      this.selected = 5;
    }
  },
  components: {
    userProfile: _components_userProfile_vue__WEBPACK_IMPORTED_MODULE_1__["default"],
    passwordSetting: _components_passwordSetting_vue__WEBPACK_IMPORTED_MODULE_2__["default"],
    twoFaSetting: _components_twoFaSetting_vue__WEBPACK_IMPORTED_MODULE_3__["default"],
    telegramSetting: _components_telegramSetting_vue__WEBPACK_IMPORTED_MODULE_4__["default"],
    accountDeletionSetting: _components_accountDeletionSetting_vue__WEBPACK_IMPORTED_MODULE_5__["default"]
  }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Support.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'Support',
  data: function data() {
    return {
      value: ''
    };
  },
  components: {}
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=script&lang=js&":
/*!******************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Transactions.vue?vue&type=script&lang=js& ***!
  \******************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vue2-datepicker */ "./node_modules/vue2-datepicker/index.esm.js");
/* harmony import */ var vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vue2-datepicker/index.css */ "./node_modules/vue2-datepicker/index.css");
/* harmony import */ var vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(vue2_datepicker_index_css__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({
  name: "Transaction",
  data: function data() {
    return {
      statuses: ["All", "Pending", "Completed"],
      selectedStatus: 0,
      dateStart: null,
      dateFinish: null,
      titleCard: "Bets",
      selected: 1,
      columns: ["date", "transaction", "game", "desc", "amount", "wl", "status"],
      options: {
        headings: {
          date: "Date",
          transaction: "Transaction",
          game: "Game-Event",
          desc: "Description",
          amount: "Amount",
          wl: "Win/Loss",
          status: "Status"
        },
        sortable: ["date", "status"],
        filterable: false,
        perPage: 10,
        pagination: {
          show: true,
          chunk: 4,
          dropdown: false,
          nav: "fixed"
        },
        sortIcon: {
          base: "fa",
          is: "fa-sort",
          up: "fa-sort-asc",
          down: "fa-sort-desc"
        },
        showChildRowToggler: true,
        childRowTogglerFirst: true
      },
      selectedTableData: "",
      // bet / credit / deposit
      tableData: []
    };
  },
  created: function created() {
    this.$store.dispatch("transaction/GET_CREDIT", {});
  },
  methods: {
    /**
     * Triggered when user click credit/debit tab
     */
    debitCredit: function debitCredit(value) {
      var _this = this;

      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var options;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                // 1. Change the table layout for debit/credit
                options = _this.$store.state.transaction.tableOptions;
                options.headings = _this.$store.state.transaction.headingDebitCredit;
                _this.columns = _this.$store.state.transaction.columnDebitCredit;
                _this.options = options; // Reset Filter

                _this.resetFilter(); // 2. Dispatch for getting table data


                if (!(value == "debit")) {
                  _context.next = 11;
                  break;
                }

                _this.selectedTableData = "debit";
                _this.titleCard = "debits";
                _this.tableData = _this.$store.getters["transaction/debitTableData"];
                _context.next = 11;
                return _this.$store.dispatch("transaction/GET_DEBIT", {});

              case 11:
                if (!(value == "credit")) {
                  _context.next = 17;
                  break;
                }

                _this.selectedTableData = "credit";
                _this.titleCard = "credits";
                _this.tableData = _this.$store.getters["transaction/creditTableData"];
                _context.next = 17;
                return _this.$store.dispatch("transaction/GET_CREDIT", {});

              case 17:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },

    /**
     * Triggered when user click refer friend tab
     */
    referFriend: function referFriend() {
      this.titleCard = "refer-a-friend";
      this.resetFilter(); // 1 Change Table Layout

      var options = this.$store.state.transaction.tableOptions;
      options.headings = this.$store.state.transaction.headingFriends;
      this.columns = this.$store.state.transaction.columnFriends;
      this.options = options;
      /**
       * TODO: 2. Dispatch for getting table data
       */

      this.tableData = [];
    },

    /**
     * Triggered when user click bets tab
     */
    bet: function bet() {
      this.titleCard = "bets";
      this.resetFilter(); // 1 Change Table Layout

      var options = this.$store.state.transaction.tableOptions;
      options.headings = this.$store.state.transaction.headingBets;
      this.columns = this.$store.state.transaction.columnBets;
      this.options = options;
      /**
       * TODO: 2. Dispatch for getting table data
       */

      this.tableData = [];
    },
    getTableData: function getTableData() {
      if (this.selectedTableData == "credit") {
        this.tableData = this.$store.getters["transaction/creditTableData"];
      }

      if (this.selectedTableData == "debit") {
        this.tableData = this.$store.getters["transaction/debitTableData"];
      }
    },

    /**
     * When Status get Changed
     */
    statusChanged: function statusChanged(index) {
      this.selectedStatus = index;
      this.filter();
    },

    /**
     * Do the filter
     */
    filter: function filter() {
      var _this2 = this;

      this.getTableData(); // Date Start Filter

      if (this.dateStart != null) {
        var filterDate = new Date(this.dateStart);
        this.tableData = this.tableData.filter(function (item) {
          var itemDate = moment__WEBPACK_IMPORTED_MODULE_4___default()(item.date, "YYYY-MM-DD HH:mm:ss");
          return itemDate >= filterDate;
        });
      } // Date Finish Filter


      if (this.dateFinish != null) {
        var _filterDate = new Date(this.dateFinish);

        this.tableData = this.tableData.filter(function (item) {
          var itemDate = moment__WEBPACK_IMPORTED_MODULE_4___default()(item.date, "YYYY-MM-DD HH:mm:ss");
          return itemDate <= _filterDate;
        });
      } // Status Filter


      if (this.selectedStatus != 0) {
        this.tableData = this.tableData.filter(function (item) {
          return _this2.statuses[_this2.selectedStatus].toLowerCase() == item.status.toLowerCase();
        });
      }
    },

    /**
     * Reset Filter when changing the tab
     */
    resetFilter: function resetFilter() {
      this.dateStart = null;
      this.dateFinish = null;
      this.selectedStatus = 0;
    },

    /**
     * Convert/Download table data to CSV
     */
    downloadCsv: function downloadCsv() {
      var csv = this.columns.join(",") + "\n";
      this.tableData.forEach(function (row) {
        csv += Object.values(row).join(",");
        csv += "\n";
      });
      var anchor = document.createElement("a");
      anchor.href = "data:text/csv;charset=utf-8," + encodeURIComponent(csv);
      anchor.target = "_blank";
      anchor.download = "".concat(this.titleCard, "_").concat(moment__WEBPACK_IMPORTED_MODULE_4___default()().format("YYYY-MM-DD"), ".csv");
      anchor.click();
    }
  },
  mounted: function mounted() {
    while (this.tableData.length < 100) {
      this.tableData.push({
        date: "01-01-2021 12:00",
        transaction: "123456789ABC",
        game: "Snatch - Round ID",
        desc: "Bid 24",
        amount: "ALGO 88",
        wl: "ALGO 150",
        status: "Loss"
      });
    }
  },
  watch: {
    creditTableData: function creditTableData() {
      this.getTableData();
    },
    debitTableData: function debitTableData() {
      this.getTableData();
    }
  },
  computed: _objectSpread(_objectSpread({}, Object(vuex__WEBPACK_IMPORTED_MODULE_1__["mapGetters"])({
    creditTableData: "transaction/creditTableData",
    debitTableData: "transaction/debitTableData"
  })), {}, {
    selectedStatusName: function selectedStatusName() {
      return "Status: ".concat(this.statuses[this.selectedStatus]);
    }
  }),
  components: {
    DatePicker: vue2_datepicker__WEBPACK_IMPORTED_MODULE_2__["default"]
  }
});

/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "#AppContainer {\n  font-family: Open Sans;\n  -webkit-font-smoothing: antialiased;\n  -moz-osx-font-smoothing: grayscale;\n  text-align: center;\n  color: #2c3e50;\n  background: #1D1E21;\n}\n@media screen and (min-height: 958px) {\n#AppContainer {\n    min-height: 100vh;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".tfa-setting .text-head {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr {\n  margin: 20px auto;\n  width: 335px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr .row-qr {\n  margin: auto;\n  width: 226px;\n}\n.tfa-setting .cont-qr .col-qr {\n  margin-bottom: 20px;\n  position: relative;\n}\n.tfa-setting .cont-qr .col-qr .qr-img {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n}\n.tfa-setting .cont-qr .code-col-qr {\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 22px;\n  text-align: center;\n  color: #FFCD3E;\n}\n.tfa-setting .cont-qr .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.tfa-setting .cont-qr .btn-delete {\n  background: #BF3131 !important;\n  color: #FFFFFF !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".hovercard {\n  position: absolute;\n  opacity: 0;\n  z-index: 1;\n  right: -90%;\n  bottom: -70%;\n}\n.recent-link:hover .hovercard {\n  opacity: 1;\n  transition: 0.5s;\n  transition-delay: 0.1s;\n}\n.tooltiptext {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 10px;\n  line-height: 14px;\n  align-items: center;\n  color: #ffffff;\n  padding-top: 12px;\n  padding-bottom: 12px;\n  padding-left: 8px;\n  padding-right: 4px;\n  border-radius: 5px;\n  border-width: 10px;\n  border: 0.5px solid #ffcd3e;\n  box-sizing: border-box;\n  text-align: left;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".carousel-card {\n  position: relative;\n  width: 380px;\n  height: 420px;\n}\n.carousel-card .car-top {\n  position: absolute;\n  top: 0;\n  width: 380px;\n  height: 249px;\n  z-index: 100;\n}\n.carousel-card .car-bottom {\n  border-radius: 5px;\n  position: absolute;\n  bottom: 0;\n  width: 380px;\n  height: 300px;\n}\n.carousel-card .car-bottom .content-bottom {\n  padding-top: 130px;\n  margin: auto 10px auto 20px;\n}\n.carousel-card .car-bottom .content-bottom .donut-mobile {\n  padding: 5px 5px 10px;\n}\n.carousel-card .car-bottom .content-bottom .text-head {\n  margin-bottom: 10px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n}\n.carousel-card .car-bottom .content-bottom .text-cont {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  display: flex;\n  align-items: center;\n  text-transform: capitalize;\n  color: #FFFFFF;\n}\n.carousel-card .car-bottom .content-bottom .text-cont .icon-algo {\n  width: 14px;\n  height: 14px;\n  display: initial;\n  padding-bottom: 3px;\n}\n.carousel-card .car-bottom .content-bottom .donut-title {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 36px;\n  line-height: 36px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n}\n.carousel-card .car-bottom .content-bottom .donut-text {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.carousel-card .car-bottom .cont-bot {\n  margin-bottom: 20px;\n}\n.game-img {\n  padding-top: 10px;\n}\n.middle {\n  transition: 0.5s ease;\n  opacity: 0;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  -ms-transform: translate(-50%, -50%);\n  text-align: center;\n  z-index: 100;\n}\n.text-game {\n  opacity: 0;\n  position: absolute;\n  left: 5%;\n  top: 10%;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 24px;\n  color: #FFFFFF;\n  z-index: 100;\n}\n.bg-car-top {\n  background: linear-gradient(180deg, #171B1C 30.92%, rgba(23, 27, 28, 0) 100%);\n  opacity: 0;\n  position: absolute;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  border-top-left-radius: 4px;\n  border-top-right-radius: 4px;\n}\n.car-top:hover .text-game {\n  opacity: 1;\n}\n.car-top:hover .middle {\n  opacity: 1;\n}\n.car-top:hover .bg-car-top {\n  opacity: 0.5;\n}\n@media screen and (min-width: 0px) and (max-width: 768px) {\n.carousel-card {\n    position: relative;\n    width: 170px;\n    height: 283px;\n}\n.carousel-card .car-top {\n    position: absolute;\n    top: 0;\n    width: 170px;\n    height: 103px;\n    z-index: 100;\n}\n.carousel-card .car-bottom {\n    border-radius: 5px;\n    position: absolute;\n    bottom: 0;\n    width: 170px;\n    height: 229px;\n}\n.carousel-card .car-bottom .content-bottom {\n    padding-top: 55px !important;\n    margin: auto 10px auto 10px !important;\n}\n.carousel-card .car-bottom .content-bottom .donut-mobile {\n    margin: auto;\n    margin-bottom: 12px;\n}\n.carousel-card .car-bottom .content-bottom .text-head {\n    margin-bottom: 4px !important;\n    font-weight: bold !important;\n    font-size: 10px !important;\n    line-height: 14px !important;\n}\n.carousel-card .car-bottom .content-bottom .text-cont {\n    font-size: 10px !important;\n    line-height: 14px !important;\n}\n.carousel-card .car-bottom .content-bottom .text-cont .icon-algo {\n    width: 10px;\n    height: 10px;\n    padding-bottom: 0;\n    margin-top: -3px;\n}\n.carousel-card .car-bottom .cont-bot {\n    margin-bottom: 10px !important;\n}\n.game-img {\n    padding-top: 10px;\n    width: 165px;\n    height: 103px;\n}\n.play-img {\n    width: 44.42px;\n    height: 49.7px;\n}\n.text-game {\n    left: 5%;\n    top: 10%;\n    font-size: 10px !important;\n    line-height: 10px !important;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "*, *::after, *::before {\n  box-sizing: border-box;\n}\n.donut {\n  --donut-size: 152px;\n  --donut-border-width: 20px;\n  --donut-spacing: 0;\n  --donut-spacing-color: 255, 255, 255;\n  --donut-spacing-deg: calc(1deg * var(--donut-spacing));\n  border-radius: 50%;\n  height: var(--donut-size);\n  position: relative;\n  width: var(--donut-size);\n}\n.donut__label {\n  left: 50%;\n  line-height: 1.5;\n  position: absolute;\n  text-align: center;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  width: 80%;\n}\n.donut__label__heading {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 36px;\n  line-height: 36px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n}\n.donut__label__sub {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.donut__slice {\n  height: 100%;\n  position: absolute;\n  width: 100%;\n}\n.donut__slice::before,\n.donut__slice::after {\n  border: var(--donut-border-width) solid rgba(0, 0, 0, 0);\n  border-radius: 50%;\n  content: \"\";\n  height: 100%;\n  left: 0;\n  position: absolute;\n  top: 0;\n  transform: rotate(45deg);\n  width: 100%;\n}\n.donut__slice::before {\n  border-width: calc(var(--donut-border-width) + 1px);\n  box-shadow: 0 0 1px 0 rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));\n}\n.donut__slice__first {\n  --first-start: 0;\n}\n.donut__slice__first::before {\n  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));\n  transform: rotate(calc(360deg * var(--first-start) + 45deg));\n}\n.donut__slice__first::after {\n  border-top-color: #48c364;\n  border-right-color: rgba(72, 195, 100, calc(100 * (var(--first) - .25)));\n  border-bottom-color: rgba(72, 195, 100, calc(100 * (var(--first) - .5)));\n  border-left-color: rgba(72, 195, 100, calc(100 * (var(--first) - .75)));\n  transform: rotate(calc(360deg * var(--first-start) + 45deg + var(--donut-spacing-deg)));\n}\n.donut__slice__second {\n  --second-start: calc(var(--first));\n  --second-check: max(calc(var(--second-start) - .5), 0);\n  -webkit-clip-path: inset(0 calc(50% * (var(--second-check) / var(--second-check))) 0 0);\n          clip-path: inset(0 calc(50% * (var(--second-check) / var(--second-check))) 0 0);\n}\n.donut__slice__second::before {\n  border-top-color: rgba(var(--donut-spacing-color), calc(100 * var(--donut-spacing)));\n  transform: rotate(calc(360deg * var(--second-start) + 45deg));\n}\n.donut__slice__second::after {\n  border-top-color: #de0035;\n  border-right-color: rgba(222, 0, 53, calc(100 * (var(--second) - .25)));\n  border-bottom-color: rgba(222, 0, 53, calc(100 * (var(--second) - .5)));\n  border-left-color: rgba(222, 0, 53, calc(100 * (var(--second) - .75)));\n  transform: rotate(calc(360deg * var(--second-start) + 45deg + var(--donut-spacing-deg)));\n}\n@media screen and (min-width: 0px) and (max-width: 768px) {\n.donut {\n    --donut-size: 79.41px !important;\n    --donut-border-width: 10px !important;\n}\n.donut__label__heading {\n    font-size: 20px !important;\n    line-height: 19px !important;\n}\n.donut__label__sub {\n    font-size: 8px !important;\n    line-height: 9px !important;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".invite-raf {\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 12px;\n  line-height: 16px;\n}\n.invite-raf .text-head {\n  margin-top: 20px;\n  font-weight: bold;\n  color: #FFCD3E;\n}\n.invite-raf .sub-text {\n  margin-top: 20px;\n  font-weight: normal;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.invite-raf .last-text {\n  margin-bottom: 50px;\n  font-weight: normal;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.invite-raf .cont-qr {\n  margin: 20px auto;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.invite-raf .cont-qr .col-copy-1 {\n  margin-bottom: 20px;\n  padding-right: 20px;\n}\n.invite-raf .cont-qr .col-copy-2 {\n  margin-bottom: 20px;\n  padding-left: 20px;\n}\n.invite-raf .cont-qr .col-game {\n  margin-bottom: 0px;\n  padding-right: 20px;\n}\n.invite-raf .cont-qr .game-head {\n  font-weight: bold;\n  margin-bottom: 20px;\n}\n.invite-raf .cont-qr .comm-cont {\n  color: #FFCD3E;\n}\n.invite-raf .dep-input,\n.invite-raf .dep-input:active,\n.invite-raf .dep-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 13px;\n  line-height: 18px;\n  color: #FFFFFF;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF !important;\n  border-right: none !important;\n  height: 40px !important;\n  background: #1D1E21;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-left-radius: 4px;\n  border-bottom-left-radius: 4px;\n}\n.invite-raf input[readonly],\n.invite-raf input[readonly]:focus {\n  background: #1D1E21 !important;\n  color: #FFFFFF !important;\n}\n.invite-raf .dep-button,\n.invite-raf .dep-button:hover,\n.invite-raf .dep-button:focus,\n.invite-raf .dep-button:active {\n  position: relative;\n  color: #FFFFFF;\n  background-color: #1D1E21;\n  border: 1px solid #FFFFFF !important;\n  border-left: none !important;\n  height: 40px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-right-radius: 4px;\n  border-bottom-right-radius: 4px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(/*! ../../../node_modules/css-loader/lib/url/escape.js */ "./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".backdrop-a {\n  background: black !important;\n  opacity: 0.5;\n  content: \"\";\n  position: fixed;\n  top: 0;\n  left: 0;\n  right: 0;\n  bottom: 0;\n  z-index: 999;\n  width: 100%;\n  height: 100%;\n}\n.modal-deposit {\n  height: 525px;\n  width: 600px;\n  border-radius: 1px;\n  background: #1D2B36;\n  padding: 20px 16px 0 16px;\n  position: absolute;\n  top: 50%;\n  left: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n}\n.modal-deposit .text-error {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #fa4343;\n}\n.modal-deposit .moon-head {\n  position: relative;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  color: #FFCD3E;\n  border-bottom: 1px solid #b6b6b6;\n  padding-bottom: 17px;\n  margin-bottom: 18px;\n}\n.modal-deposit .deposit-body {\n  margin: auto;\n}\n.modal-deposit .deposit-body .deposit-content {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 22px;\n  color: #a7b0c6;\n  margin-top: 15px;\n}\n.modal-deposit .deposit-body .withdraw-content {\n  font-family: Open Sans;\n  margin-top: 15px;\n}\n.modal-deposit .deposit-body .withdraw-content .fees-text {\n  margin-top: 20px;\n  color: #a2a2a2;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 8px;\n  line-height: 11px;\n  /* identical to box height */\n  text-align: center;\n}\n.modal-deposit .deposit-body .aktive-content {\n  padding: 0 0 !important;\n}\n.modal-deposit .deposit-body .choosen-tab {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  align-items: center;\n  text-transform: uppercase;\n  border-radius: 0 !important;\n  border-bottom: 4px solid #FFCD3E !important;\n  margin-bottom: -2px;\n}\n.modal-deposit .deposit-body .tab-aktive {\n  font-family: Open Sans;\n  font-style: normal;\n  text-transform: uppercase;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  align-items: center;\n  color: #777777 !important;\n  padding-top: 8px !important;\n}\n.modal-deposit .deposit-body .nav-pills .nav-link.active,\n.modal-deposit .deposit-body .nav-pills .show > .nav-link {\n  background-color: transparent !important;\n  color: #777777 !important;\n}\n.modal-deposit .deposit-body .nav-item {\n  border-bottom: 2px solid #2d3e4a;\n}\n.modal-deposit .deposit-body .nav-link {\n  padding: 0 10px 8px !important;\n  color: #777777;\n  min-width: 112px;\n}\n.modal-deposit .deposit-body .col {\n  padding-right: 0;\n  padding-left: 0;\n}\n.modal-deposit .deposit-body .card-header {\n  padding: 0 10px !important;\n  border-bottom: none !important;\n  background: transparent !important;\n}\n.modal-deposit .deposit-body .nav-fill .nav-item:first-child {\n  text-align: left !important;\n}\n.modal-deposit .deposit-body .nav-fill .nav-item:last-child {\n  text-align: right !important;\n}\n.modal-deposit .cont-input {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.modal-deposit .text-prof {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  text-transform: capitalize;\n  line-height: 16px;\n  color: #FFFFFF;\n}\n.modal-deposit .dep-input,\n.modal-deposit .dep-input:active,\n.modal-deposit .dep-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 13px;\n  line-height: 18px;\n  color: #A2A2A2;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF !important;\n  border-right: none !important;\n  height: 36px !important;\n  background: #1D2B36;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-left-radius: 4px;\n  border-bottom-left-radius: 4px;\n}\n.modal-deposit input[readonly],\n.modal-deposit input[readonly]:focus {\n  background: #1D2B36 !important;\n  color: #A2A2A2 !important;\n}\n.modal-deposit .dep-button,\n.modal-deposit .dep-button:hover,\n.modal-deposit .dep-button:focus,\n.modal-deposit .dep-button:active {\n  position: relative;\n  color: #FFFFFF;\n  background-color: #1D2B36;\n  border: 1px solid #FFFFFF !important;\n  border-left: none !important;\n  height: 36px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-right-radius: 4px;\n  border-bottom-right-radius: 4px;\n}\n.modal-deposit .am-input,\n.modal-deposit .am-input:active,\n.modal-deposit .am-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 13px;\n  line-height: 16px;\n  color: #A2A2A2;\n  background-color: #1D2B36;\n  box-shadow: none !important;\n  border: 1px solid #FFFFFF;\n  height: 32px !important;\n}\n.modal-deposit .max-btn,\n.modal-deposit .max-btn:hover,\n.modal-deposit .max-btn:focus,\n.modal-deposit .max-btn:active {\n  position: relative;\n  color: #1D2B36;\n  background-color: #FFCD3E;\n  border: 1px solid #FFFFFF;\n  border-left: none !important;\n  height: 32px;\n  width: 74px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  box-shadow: none !important;\n  outline: transparent !important;\n}\n.modal-deposit .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.modal-deposit .succes-deposit .x-head .x-but,\n.modal-deposit .succes-deposit .x-head .x-but:focus,\n.modal-deposit .succes-deposit .x-head .x-but:active {\n  text-decoration: none !important;\n  background: transparent;\n  color: white !important;\n  border: none !important;\n  outline: none !important;\n}\n.modal-deposit .succes-deposit .text-succ {\n  margin: 50px 0 60px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 24px;\n  line-height: 33px;\n  color: #FFCD3E;\n}\n.choose-token .custom-select {\n  background-color: #13202C !important;\n  background-image: url(" + escape(__webpack_require__(/*! ../../img/chevron down.svg */ "./resources/img/chevron down.svg")) + ");\n  background-repeat: no-repeat;\n  background-position: right 0.8rem center;\n  background-size: 8px 12px;\n  border: transparent !important;\n  vertical-align: middle;\n  color: #ffffff !important;\n  padding: 4px 25px 4px 25px !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  text-transform: uppercase;\n  text-align: center;\n  font-size: 12px;\n  height: 40px !important;\n  width: 90px;\n}\n.choose-token .btn-secondary,\n.choose-token .btn-secondary:hover,\n.choose-token .show > .btn-secondary.dropdown-toggle,\n.choose-token .btn-secondary:focus {\n  color: #FFCD3E;\n  background-color: transparent !important;\n  border-color: transparent !important;\n  box-shadow: none !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n}\n.choose-token .nav-link {\n  display: block;\n  padding: 0 20px 0 0 !important;\n}\n.choose-token .btn-sm,\n.choose-token .btn-group-sm > .btn {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 1.5;\n  border-radius: 0.2rem;\n  padding: 0 !important;\n}\n.choose-token .dropdown-toggle::after {\n  font-size: 14px !important;\n}\n.choose-token .dropdown-menu {\n  color: #FFFFFF !important;\n  background-color: #1D2B36 !important;\n  border: 1px solid rgba(0, 0, 0, 0.15);\n  border-radius: 0.25rem;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  min-width: 90px;\n}\n.choose-token .dropdown-item {\n  color: #FFFFFF !important;\n  padding: 4px 5px !important;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n}\n.choose-token .dropdown-item span {\n  text-transform: uppercase;\n}\n.choose-token .dropdown-item:hover,\n.choose-token .dropdown-item:active,\n.choose-token .dropdown-item:focus {\n  background: #1D2B36 !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".footer-desk a, .footer-desk a:hover {\n  text-decoration: none !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".btn-register {\n  width: 86px !important;\n  border-radius: 2px;\n  background-color: #FFCD3E !important;\n  font-family: Open Sans !important;\n  font-style: normal !important;\n  font-weight: bold !important;\n  font-size: 12px !important;\n  line-height: 16px !important;\n  text-align: center !important;\n  text-transform: capitalize !important;\n  color: #1D2B36;\n}\n.btn-login {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  text-transform: capitalize;\n  color: #FFCD3E;\n}\n.custom-switch {\n  padding: 1px 0px 0 38px !important;\n}\n.custom-control-input:checked ~ .custom-control-label::before {\n  border: #FFCD3E solid 1px !important;\n  background-color: #1D2B36 !important;\n}\n.custom-switch .custom-control-input:checked ~ .custom-control-label::after {\n  background-color: #FFCD3E !important;\n}\n.custom-switch:active,\n.custom-switch:focus,\n.custom-control-input:active,\n.custom-control-input:focus,\n.custom-control-label:focus,\n.custom-control-label:active,\n.custom-control-label:focus,\n.custom-control-label:active .input-group-sm:active,\n.input-group-sm:focus {\n  box-shadow: none !important;\n  text-decoration: none !important;\n  outline: none !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".invite-raf {\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 12px;\n  line-height: 16px;\n}\n.invite-raf .cont-qr {\n  margin: 20px auto;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.invite-raf .cont-qr .title-sub {\n  color: #878787;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.invite-raf .cont-qr .field-sub {\n  font-weight: bold;\n}\n.invite-raf .cont-qr .col-left {\n  width: 265px;\n  float: left;\n  margin-bottom: 20px;\n}\n.invite-raf .cont-qr .col-center {\n  width: 265px;\n  margin: 0 auto;\n  margin-bottom: 20px;\n}\n.invite-raf .cont-qr .col-right {\n  width: 265px;\n  float: right;\n  margin-bottom: 20px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".invite-raf {\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 12px;\n  line-height: 16px;\n}\n.invite-raf .cont-qr {\n  margin: 20px auto;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.invite-raf .cont-qr .title-sub {\n  color: #878787;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.invite-raf .cont-qr .field-sub {\n  font-weight: bold;\n}\n.invite-raf .cont-qr .col-left {\n  width: 265px;\n  float: left;\n  margin-bottom: 20px;\n}\n.invite-raf .cont-qr .col-center {\n  width: 265px;\n  margin: 0 auto;\n  margin-bottom: 20px;\n}\n.invite-raf .cont-qr .col-right {\n  width: 265px;\n  float: right;\n  margin-bottom: 20px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".table-home .text-table {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 19px;\n  color: #FFCD3E;\n}\n.table-home .select-table {\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 12px;\n  border: 1px solid #BEBEBE !important;\n  border-radius: 4px;\n  vertical-align: middle;\n  color: #FFFFFF;\n  padding: 4px 25px 4px 8px !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  height: 30px !important;\n  -webkit-appearance: none;\n}\n.table-home .select-table:focus, .table-home .select-table:active {\n  outline: none !important;\n}\n.table-home .vuetable-mob {\n  display: none;\n}\n.table-home .vue-tables-custom-style {\n  padding: 0;\n}\n.table-home .vue-tables-custom-style th {\n  border: none !important;\n  background: #1D1E21 !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 14px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  padding: 10px 0 !important;\n  width: 16.6666666667%;\n  height: 40px !important;\n}\n.table-home .vue-tables-custom-style th:first-child {\n  text-align: left;\n}\n.table-home .vue-tables-custom-style th:last-child {\n  text-align: right;\n}\n.table-home .vue-tables-custom-style td {\n  border: none !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 14px;\n  color: #FFFFFF;\n  text-transform: capitalize;\n  padding: 10px 0px !important;\n  text-align: center;\n  height: 40px !important;\n}\n.table-home .vue-tables-custom-style td:first-child {\n  text-align: left;\n}\n.table-home .vue-tables-custom-style td:last-child {\n  text-align: right;\n}\n.table-home .vue-tables-custom-style tr:nth-child(odd) {\n  background: #1D1E21 !important;\n  color: #FFFFFF;\n}\n.table-home .vue-tables-custom-style tr:nth-child(even) {\n  background: #1D1E21 !important;\n  color: #FFFFFF;\n}\n.table-home .vue-tables-custom-style .form-inline label {\n  display: none !important;\n}\n.table-home .VuePagination__count {\n  padding: 0;\n  padding-top: 7px;\n  margin: 0;\n  vertical-align: middle;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  color: #42505C !important;\n}\n.table-home .VuePagination {\n  border-top: 1px solid #B6B6B6;\n  padding: 10px 0 0 0;\n  margin: 0;\n}\n.table-home .pagination {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 15px;\n  position: absolute;\n  right: -5px;\n}\n.table-home .page-item {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  padding: 0;\n}\n.table-home .page-link {\n  position: relative;\n  display: block;\n  padding: 0.5rem 0.75rem;\n  margin-left: -1px;\n  line-height: 1.25;\n  color: #FFFFFF;\n  background-color: transparent !important;\n  border: transparent !important;\n  cursor: pointer;\n}\n.table-home .page-item.active .page-link {\n  z-index: 3;\n  border-radius: 3px;\n  color: #FFFFFF !important;\n  background-color: #1D1E21 !important;\n  border-color: transparent !important;\n  padding: 4px 8px;\n  align-items: center;\n  margin-top: 4px;\n}\n.table-home .page-item.disabled .page-link {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  border-color: transparent !important;\n}\n.table-home .pull-right {\n  float: none;\n}\n.table-home .fa.pull-right {\n  margin-left: 0.6em;\n}\n.table-home .VueTables__limit {\n  display: none !important;\n}\n.table-home .val-bet {\n  color: #52FF00 !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".tfa-setting .text-head {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr {\n  margin: 20px auto;\n  width: 335px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr .row-qr {\n  margin: auto;\n  width: 226px;\n}\n.tfa-setting .cont-qr .col-qr {\n  margin-bottom: 20px;\n  position: relative;\n}\n.tfa-setting .cont-qr .col-qr .qr-img {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n}\n.tfa-setting .cont-qr .code-col-qr {\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 22px;\n  text-align: center;\n  color: #FFCD3E;\n}\n.tfa-setting .cont-qr .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.tfa-setting .cont-qr .btn-cancel {\n  background: #BEBEBE !important;\n}\n.tfa-setting .cont-form {\n  margin-top: 20px;\n  margin-bottom: 20px;\n  width: 638px;\n}\n.tfa-setting .title-input {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n}\n.tfa-setting .custom-select {\n  display: inline-block;\n  width: 100%;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1px solid #FFFFFF !important;\n}\n.tfa-setting .notify-list {\n  margin-bottom: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".tfa-setting .text-head {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr {\n  margin: 20px auto;\n  width: 335px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr .row-qr {\n  margin: auto;\n  width: 226px;\n}\n.tfa-setting .cont-qr .col-qr {\n  margin-bottom: 20px;\n  position: relative;\n}\n.tfa-setting .cont-qr .col-qr .qr-img {\n  position: absolute;\n  left: 50%;\n  top: 50%;\n  transform: translate(-50%, -50%);\n  z-index: 999;\n}\n.tfa-setting .cont-qr .code-col-qr {\n  font-weight: bold;\n  font-size: 16px;\n  line-height: 22px;\n  text-align: center;\n  color: #FFCD3E;\n}\n.tfa-setting .cont-qr .setting-input,\n.tfa-setting .cont-qr .setting-input:active,\n.tfa-setting .cont-qr .setting-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  height: 36px !important;\n  background: #1D1E21;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  max-width: 335px;\n}\n.tfa-setting .cont-qr .custom-control {\n  position: relative;\n  display: block;\n  padding-left: 0 !important;\n}\n.tfa-setting .cont-qr .checkbox-name {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 22px;\n  color: #FFFFFF;\n}\n.tfa-setting .cont-qr .custom-control-input:checked ~ .custom-control-label::before {\n  border: #FFCD3E solid 1px !important;\n  background-color: #FFCD3E !important;\n}\n.tfa-setting .cont-qr .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.tfa-setting .cont-qr .btn-cancel {\n  background: #BEBEBE !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".user-profile .form-user-profile {\n  width: 638px;\n}\n.user-profile .form-user-profile .cont-form {\n  margin-top: 20px;\n}\n.user-profile .title-input {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n}\n.user-profile .setting-input,\n.user-profile .setting-input:active,\n.user-profile .setting-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  height: 36px !important;\n  background: #1D1E21;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  max-width: 335px;\n}\n.user-profile input[readonly],\n.user-profile input[readonly]:focus {\n  background: #3e3f42 !important;\n  color: #989899 !important;\n  border: 1px solid transparent !important;\n}\n.user-profile .custom-control {\n  position: relative;\n  display: block;\n  padding-left: 0 !important;\n  margin-left: -100px;\n}\n.user-profile .checkbox-name {\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 22px;\n  color: #FFFFFF;\n}\n.user-profile .custom-control-input:checked ~ .custom-control-label::before {\n  border: #FFCD3E solid 1px !important;\n  background-color: #FFCD3E !important;\n}\n.user-profile .custom-select {\n  display: inline-block;\n  width: 100%;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1px solid #FFFFFF !important;\n}\n.user-profile .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".dashboard {\n  max-width: 1440px;\n  margin: auto;\n}\n.dashboard .games-cont {\n  padding: 30px 120px 0px;\n}\n.dashboard .table-one {\n  padding: 70px 120px 0px;\n}\n@media screen and (min-width: 0px) and (max-width: 768px) {\n.games-cont {\n    padding: 10px 20px 0px !important;\n}\n.table-one {\n    padding: 50px 20px 0px !important;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".faq {\n  max-width: 1440px;\n  margin: auto;\n}\n.faq .jumbotron {\n  padding: 2rem 1rem;\n  margin-bottom: 0;\n  background-color: #070709;\n  border-radius: 0.3rem;\n  background-image: url(\"/images/faq-jumbotron.svg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  max-width: 1440px;\n  height: 352px;\n  position: relative;\n  border-radius: 0 !important;\n}\n.faq .content-htp {\n  padding: 0 120px;\n  margin: auto !important;\n}\n.faq .content-htp .title-head {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFFFFF;\n}\n.faq .content-htp .content-main {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 14px;\n  line-height: 19px;\n  font-weight: normal;\n  color: #FFFFFF;\n  text-align: left;\n}\n.faq .content-htp .content-main span {\n  font-weight: bold;\n}\n.faq .custom-select {\n  display: inline-block;\n  width: 255px;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1px solid #FFFFFF !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".forgot-password {\n  padding-top: 20px;\n  min-height: 792px;\n}\n.forgot-password .container {\n  width: 335px;\n}\n.forgot-password .container .title-hed {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFCD3E;\n}\n.forgot-password .container .col-12 {\n  margin-bottom: 20px;\n}\n.forgot-password .container .form-group {\n  margin-bottom: 20px;\n}\n.forgot-password .container .form-control {\n  display: block;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  padding: 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #BEBEBE;\n  background-color: #1D1E21;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  border-radius: 4px;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n.forgot-password .container .form-control:active,\n.forgot-password .container .form-control:focus {\n  box-shadow: unset !important;\n}\n.forgot-password .container .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.forgot-password .container .forgot-text {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n  margin-bottom: 20px;\n}\n.forgot-password .container a {\n  color: #FFCD3E;\n  text-decoration: none;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".home {\n  max-width: 1440px;\n  margin: auto;\n}\n.home .table-one {\n  padding: 70px 120px 0px;\n}\n.home .jumbotron {\n  padding: 2rem 1rem;\n  margin-bottom: 0;\n  background-color: transparent;\n  background-image: url(\"/images/background-home.png\");\n  max-width: 1440px;\n  height: 552px;\n  position: relative;\n  border-radius: 0;\n}\n.home .jumbotron h1 {\n  font-family: Montserrat;\n  position: absolute;\n  top: 23%;\n  left: 12%;\n  font-style: normal;\n  font-weight: 1000 !important;\n  font-size: 28px;\n  line-height: 34px !important;\n  letter-spacing: 2px;\n  text-transform: uppercase;\n  color: #FFCD3E !important;\n  text-shadow: 0px 4px 4px rgba(0, 0, 0, 0.85);\n  margin: 0 !important;\n}\n.home .jumbotron .img-welcome {\n  position: absolute;\n  top: 23%;\n  left: 12%;\n}\n.home .jumbotron .display-3 {\n  -webkit-text-stroke: 1px #1D2B36;\n  font-size: 28px;\n}\n.home .jumbotron .image-jumbo {\n  position: absolute;\n  top: 28%;\n  left: 13%;\n}\n.home .cont-carousel {\n  margin-top: -290px;\n}\n.home .card-carousels {\n  margin: 0 15px;\n}\n.home .carousel-control-prev, .home .carousel-control-next {\n  width: 7%;\n  opacity: 1;\n}\n.home .carousel-inner {\n  padding: 0 5%;\n}\n.home .carousel-control-next-icon {\n  background-image: url(\"/images/carousel-next.svg\") !important;\n}\n.home .carousel-control-prev-icon {\n  background-image: url(\"/images/carousel-prev.svg\") !important;\n}\n.home .carousel-control-prev-icon, .home .carousel-control-next-icon {\n  display: inline-block;\n  width: 40px;\n  height: 40px;\n  background: 50%/100% 100% no-repeat;\n}\n.home .carousel-indicators {\n  bottom: -50px;\n}\n.home .carousel-indicators li {\n  width: 12px;\n  height: 12px;\n  border-radius: 100%;\n  background-color: #FFCD3E;\n  border-top: unset !important;\n  border-bottom: unset !important;\n  opacity: 0.1;\n}\n.home .carousel-indicators li:active, .home .carousel-indicators li:focus {\n  text-decoration: none !important;\n  outline: none !important;\n}\n.home .carousel-indicators .active {\n  opacity: 1;\n}\n.home #carousel-mob {\n  display: none;\n}\n@media screen and (min-width: 0px) and (max-width: 768px) {\n.table-one {\n    padding: 50px 20px 0px !important;\n}\n.jumbotron {\n    background-image: url(\"/images/background-home-mobile.png\") !important;\n    max-width: 375px !important;\n    height: 281px !important;\n}\n.jumbotron h1 {\n    position: absolute;\n    top: 19% !important;\n    left: 9% !important;\n    font-style: normal;\n    font-size: 11.2px !important;\n    line-height: 14px !important;\n}\n.jumbotron .display-3 {\n    -webkit-text-stroke: 0.3px #1D2B36 !important;\n    font-size: 11.2px !important;\n}\n.jumbotron .image-jumbo {\n    top: 26.5% !important;\n    left: 12% !important;\n}\n.jumbotron .image-jumbo img {\n    width: 138.8px;\n    height: 36.8px;\n}\n.cont-carousel {\n    margin-top: -95px !important;\n}\n.carousel-inner {\n    padding: 0 10px !important;\n}\n.card-carousels {\n    margin: 0 5px !important;\n}\n#carousel-desk {\n    display: none;\n}\n#carousel-mob {\n    display: block !important;\n}\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".howtoplay {\n  max-width: 1440px;\n  margin: auto;\n}\n.howtoplay .jumbotron {\n  padding: 2rem 1rem;\n  margin-bottom: 0;\n  background-color: #040D12;\n  border-radius: 0.3rem;\n  background-image: url(\"/images/howtoplay-jumbotron.svg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  max-width: 1440px;\n  height: 360px;\n  position: relative;\n  border-radius: 0 !important;\n}\n.howtoplay .content-htp {\n  padding: 0 120px;\n  margin: auto !important;\n}\n.howtoplay .content-htp .title-head {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFFFFF;\n}\n.howtoplay .content-htp .content-main {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 14px;\n  font-weight: normal;\n  line-height: 19px;\n  color: #FFFFFF;\n  text-align: left;\n  min-height: 293px;\n}\n.howtoplay .content-htp .content-main span {\n  font-weight: bold;\n}\n.howtoplay .custom-select {\n  display: inline-block;\n  width: 255px;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1px solid #FFFFFF !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login {\n  padding-top: 20px;\n  min-height: 792px;\n}\n.login .container {\n  width: 335px;\n}\n.login .container .text-error {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #fa4343;\n}\n.login .container .desc-img {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFCD3E;\n}\n.login .container .col-12 {\n  margin-bottom: 20px;\n}\n.login .container .input-icon {\n  position: relative;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  box-sizing: border-box;\n}\n.login .container .input-icon .form-control {\n  width: 100%;\n  height: 100%;\n  padding-right: 28px;\n}\n.login .container .input-icon .icon-info {\n  position: absolute;\n  right: 10px;\n  top: 10px;\n  cursor: pointer;\n}\n.login .container .form-group {\n  margin-bottom: 20px;\n}\n.login .container .form-control {\n  display: block;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  padding: 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #BEBEBE;\n  background-color: #1D1E21;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  border-radius: 4px;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n.login .container .form-control:active,\n.login .container .form-control:focus {\n  box-shadow: unset !important;\n}\n.login .container .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.login .container .have-login {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #878787;\n}\n.login .container .forgot-pw {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  color: #878787;\n  margin-bottom: 11px;\n}\n.login .container .forgot-pw a {\n  color: #878787;\n}\n.login .container a {\n  color: #FFCD3E;\n  text-decoration: none;\n}\n.login .container .message-tool {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 6px !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".login-2fa {\n  padding-top: 20px;\n  min-height: 792px;\n}\n.login-2fa .container {\n  width: 335px;\n  overflow: visible;\n}\n.login-2fa .container .text-error {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #fa4343;\n}\n.login-2fa .container .image-top {\n  position: relative;\n  float: right;\n  right: 50%;\n}\n.login-2fa .container .image-top img {\n  position: relative;\n  right: -50%;\n}\n.login-2fa .container .desc-img {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 19px;\n  color: #FFCD3E;\n}\n.login-2fa .container .col-12 {\n  margin-bottom: 20px;\n}\n.login-2fa .container .form-group {\n  margin-bottom: 22px;\n}\n.login-2fa .container .form-control {\n  display: block;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  padding: 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #BEBEBE;\n  background-color: #1D1E21;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  border-radius: 4px;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n.login-2fa .container .form-control:active,\n.login-2fa .container .form-control:focus {\n  box-shadow: unset !important;\n}\n.login-2fa .container .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.login-2fa .container a {\n  color: #FFCD3E;\n  text-decoration: none;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".provably-fair {\n  max-width: 1440px;\n  margin: auto;\n}\n.provably-fair .jumbotron {\n  padding: 2rem 1rem;\n  margin-bottom: 0;\n  background-color: #0A1318;\n  border-radius: 0.3rem;\n  background-image: url(\"/images/provably-jumbotron.svg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  max-width: 1440px;\n  height: 360px;\n  position: relative;\n  border-radius: 0 !important;\n}\n.provably-fair .content-htp {\n  padding: 0 120px;\n  margin: auto !important;\n}\n.provably-fair .content-htp .title-head {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFFFFF;\n}\n.provably-fair .content-htp .content-main {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 14px;\n  line-height: 19px;\n  font-weight: normal;\n  color: #FFFFFF;\n  text-align: left;\n}\n.provably-fair .content-htp .content-main .box-verify {\n  height: 800px;\n  border: 1px solid #FFFFFF;\n  margin-bottom: 45px;\n  box-sizing: border-box;\n  border-radius: 4px;\n}\n.provably-fair .content-htp .content-main .box-verify .wrap-verify {\n  width: 544px;\n  margin: auto;\n  margin-top: 40px;\n}\n.provably-fair .content-htp .content-main .box-verify .text-head {\n  margin: 20px 0;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 19px;\n  text-align: justify;\n  color: #FFFFFF;\n}\n.provably-fair .content-htp .content-main span {\n  font-size: 18px;\n  line-height: 25px;\n}\n.provably-fair .cont-prof {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.provably-fair .text-prof {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  text-transform: capitalize;\n  line-height: 16px;\n  color: #FFFFFF;\n}\n.provably-fair .prof-input, .provably-fair .prof-input:active, .provably-fair .prof-input:focus {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n  background-clip: padding-box;\n  border: 1.5px solid #FFFFFF !important;\n  border-right: none !important;\n  height: 36px !important;\n  background: #1D1E21;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-left-radius: 4px;\n  border-bottom-left-radius: 4px;\n}\n.provably-fair .prof-button, .provably-fair .prof-button:hover, .provably-fair .prof-button:focus, .provably-fair .prof-button:active {\n  position: relative;\n  color: #FFFFFF;\n  background-color: #1D1E21;\n  border: 1.5px solid #FFFFFF !important;\n  border-left: none !important;\n  height: 36px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-top-right-radius: 4px;\n  border-bottom-right-radius: 4px;\n}\n.provably-fair .prof-button:after {\n  position: absolute;\n  border-left: 1px solid #FFFFFF;\n  height: 14px;\n  width: 100%;\n  bottom: 9px;\n  left: 1px;\n  content: \"\";\n}\n.provably-fair input[readonly], .provably-fair input[readonly]:focus {\n  background: #1D1E21 !important;\n  color: #FFFFFF !important;\n}\n.provably-fair .custom-select {\n  display: inline-block;\n  width: 100%;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/divider.svg\"), url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat, no-repeat;\n  background-position: right 2.3rem center, right 0.75rem center;\n  background-size: 1px 14px, 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1.5px solid #FFFFFF !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container-main .main-content {\n  margin-bottom: 20px;\n}\n.container-main .network-cont {\n  margin: 20px auto;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #FFFFFF;\n}\n.container-main .network-cont .title-sub {\n  margin-bottom: 20px;\n  color: #878787;\n  font-weight: bold;\n  text-transform: uppercase;\n}\n.container-main .network-cont .field-sub {\n  margin-bottom: 20px;\n  font-weight: bold;\n  text-transform: capitalize;\n}\n.container-main .menu-side {\n  min-height: 238px !important;\n}\n.container-main .header-menu .select-time {\n  position: relative;\n}\n.container-main .header-menu .custom-select {\n  position: absolute;\n  right: 0;\n  bottom: -10px;\n  display: inline-block;\n  width: 168px;\n  height: 36px;\n  padding: 0.375rem 1.75rem 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  vertical-align: middle;\n  background-color: #1D1E21;\n  background-image: url(\"/images/chevron-big-down.svg\");\n  background-repeat: no-repeat;\n  background-position: right 0.75rem center;\n  background-size: 8px 10px;\n  box-shadow: none !important;\n  outline: transparent !important;\n  border-radius: 4px;\n  border: 1px solid #FFFFFF !important;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".register {\n  padding-top: 20px;\n  min-height: 792px;\n}\n.register .container {\n  width: 335px;\n}\n.register .container .text-error {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #fa4343;\n}\n.register .container .desc-img {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFCD3E;\n}\n.register .container .col-12 {\n  margin-bottom: 20px;\n}\n.register .container .input-icon {\n  position: relative;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  box-sizing: border-box;\n}\n.register .container .input-icon .form-control {\n  width: 100%;\n  height: 100%;\n  padding-right: 28px;\n}\n.register .container .input-icon .icon-info {\n  position: absolute;\n  right: 10px;\n  top: 10px;\n  cursor: pointer;\n}\n.register .container .form-control {\n  display: block;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  padding: 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #BEBEBE;\n  background-color: #1D1E21;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  border-radius: 4px;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n.register .container .form-control:active,\n.register .container .form-control:focus {\n  box-shadow: unset !important;\n}\n.register .container .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.register .container .have-login {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #878787;\n}\n.register .container a {\n  color: #FFCD3E;\n  text-decoration: none;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".forgot-password {\n  padding-top: 20px;\n  min-height: 792px;\n}\n.forgot-password .container {\n  width: 335px;\n}\n.forgot-password .container .text-error {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: 600;\n  font-size: 12px;\n  line-height: 16px;\n  text-align: center;\n  color: #fa4343;\n}\n.forgot-password .container .title-hed {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFCD3E;\n}\n.forgot-password .container .col-12 {\n  margin-bottom: 20px;\n}\n.forgot-password .container .form-group {\n  margin-bottom: 20px;\n}\n.forgot-password .container .form-control {\n  display: block;\n  width: 100%;\n  height: calc(1.5em + 0.75rem);\n  padding: 0.375rem 0.75rem;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 16px;\n  color: #BEBEBE;\n  background-color: #1D1E21;\n  background-clip: padding-box;\n  border: 1px solid #FFFFFF;\n  border-radius: 4px;\n  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;\n}\n.forgot-password .container .form-control:active,\n.forgot-password .container .form-control:focus {\n  box-shadow: unset !important;\n}\n.forgot-password .container .btn-create {\n  width: 100% !important;\n  height: 46px;\n  border-radius: 4px;\n  background: #FFCD3E;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #1d2b36;\n  padding: 0 !important;\n}\n.forgot-password .container a {\n  color: #FFCD3E;\n  text-decoration: none;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container-main .main-content {\n  margin-bottom: 20px;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".support {\n  max-width: 1440px;\n  margin: auto;\n}\n.support .jumbotron {\n  padding: 2rem 1rem;\n  margin-bottom: 0;\n  background-color: #040D12;\n  border-radius: 0.3rem;\n  background-image: url(\"/images/support-jumbotron.svg\");\n  background-repeat: no-repeat;\n  background-position: center;\n  max-width: 1440px;\n  height: 360px;\n  position: relative;\n  border-radius: 0 !important;\n}\n.support .content-htp {\n  padding: 0 120px;\n  margin: auto !important;\n}\n.support .content-htp .title-head {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 18px;\n  line-height: 25px;\n  color: #FFFFFF;\n}\n.support .content-htp .content-main {\n  margin-top: 20px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-size: 14px;\n  font-weight: normal;\n  line-height: 19px;\n  color: #FFFFFF;\n  text-align: left;\n  min-height: 293px;\n}\n.support .content-htp .content-main span {\n  font-weight: bold;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var escape = __webpack_require__(/*! ../../../node_modules/css-loader/lib/url/escape.js */ "./node_modules/css-loader/lib/url/escape.js");
exports = module.exports = __webpack_require__(/*! ../../../node_modules/css-loader/lib/css-base.js */ "./node_modules/css-loader/lib/css-base.js")(false);
// imports


// module
exports.push([module.i, ".container-main {\n  padding: 3px !important;\n  background-color: transparent;\n  max-width: 1210px !important;\n  margin: auto;\n  margin-top: 20px;\n  position: relative;\n}\n.container-main .menu-side {\n  max-width: 200px;\n  min-height: 360px;\n  padding: 18px 20px 0;\n  background: #1D1E21;\n  border: 1px solid #b6b6b6;\n  border-radius: 4px;\n}\n.container-main .menu-side .menu-item {\n  margin: 15px 0;\n}\n.container-main .menu-side .menu-item .activelink {\n  color: #FFCD3E !important;\n}\n.container-main .menu-side .menu-item .activelink .nav-link {\n  color: #FFCD3E !important;\n}\n.container-main .menu-side .menu-item .nav-link {\n  padding: 17px 0 !important;\n  text-align: left;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #BEBEBE;\n}\n.container-main .header-menu {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 14px;\n  line-height: 16px;\n  text-transform: uppercase;\n  color: #FFCD3E;\n  border-bottom: 1px solid #b6b6b6;\n  padding-bottom: 20px;\n}\n.container-main .content-side {\n  margin-left: 20px;\n  margin-bottom: 20px;\n  max-width: 980px;\n  min-height: 360px;\n  padding: 18px 20px 0;\n  background: #1D1E21;\n  border: 1px solid #b6b6b6;\n  border-radius: 4px;\n}\n.container-main .content-side .main-content {\n  margin-bottom: 20px;\n}\n.container-main .content-side .main-content .cont-table {\n  position: relative;\n}\n.container-main .content-side .main-content .cont-table .exp-tbl {\n  position: absolute;\n  bottom: 0;\n  right: 0;\n}\n.container-main .vue-tables-custom-style {\n  padding: 0;\n}\n.container-main .vue-tables-custom-style th {\n  border: 1px solid #1D1E21 !important;\n  background: #1D1E21 !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 14px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  padding: 10px 0 !important;\n  text-align: left;\n}\n.container-main .vue-tables-custom-style td {\n  border: 1px solid #1D1E21 !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 14px;\n  color: #FFFFFF;\n  text-transform: capitalize;\n  padding: 10px 0px !important;\n  text-align: left;\n}\n.container-main .VuePagination__count {\n  padding: 0;\n  padding-top: 7px;\n  margin: 0;\n  vertical-align: middle;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 14px;\n  color: #FFFFFF !important;\n}\n.container-main .VuePagination {\n  border-top: 1px solid #b6b6b6;\n  padding: 10px 0 0 0;\n  margin: 0;\n}\n.container-main .pagination {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 15px;\n  position: absolute;\n  right: 30px;\n}\n.container-main .page-item {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  padding: 0;\n}\n.container-main .page-link {\n  position: relative;\n  display: block;\n  padding: 0.5rem 0.75rem;\n  margin-left: -1px;\n  line-height: 1.25;\n  color: #FFFFFF;\n  background-color: transparent !important;\n  border: transparent !important;\n  cursor: pointer;\n}\n.container-main .page-item.active .page-link {\n  z-index: 3;\n  border-radius: 3px;\n  color: #1d2b36 !important;\n  background-color: #FFCD3E !important;\n  border-color: transparent !important;\n  padding: 4px 8px;\n  align-items: center;\n  margin-top: 4px;\n}\n.container-main .page-item.disabled .page-link {\n  background: transparent !important;\n  color: #FFFFFF !important;\n  border-color: transparent !important;\n}\n.container-main .pull-right {\n  float: none;\n}\n.container-main .fa.pull-right {\n  margin-left: 0.6em;\n}\n.container-main .VueTables__limit {\n  display: none !important;\n}\n.dropdown-table {\n  margin: 20px 0;\n}\n.dropdown-table .custom-select-table {\n  background-color: #1D1E21 !important;\n  background-image: url(" + escape(__webpack_require__(/*! ../../img/chevron down.svg */ "./resources/img/chevron down.svg")) + ");\n  background-repeat: no-repeat;\n  background-position: right 0.8rem center;\n  background-size: 8px 12px;\n  border: 1px solid #FFFFFF !important;\n  vertical-align: middle;\n  color: #FFFFFF !important;\n  padding: 4px 25px 4px 10px !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  text-transform: uppercase;\n  font-size: 12px;\n  height: 36px !important;\n  width: 156px;\n  border-radius: 4px;\n}\n.dropdown-table .btn-secondary,\n.dropdown-table .btn-secondary:hover,\n.dropdown-table .show > .btn-secondary.dropdown-toggle,\n.dropdown-table .btn-secondary:focus {\n  color: #FFCD3E;\n  background-color: transparent !important;\n  border-color: transparent !important;\n  box-shadow: none !important;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n}\n.dropdown-table .nav-link {\n  display: block;\n  padding: 0 20px 0 0 !important;\n}\n.dropdown-table .btn-sm,\n.dropdown-table .btn-group-sm > .btn {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: normal;\n  font-size: 12px;\n  line-height: 1.5;\n  border-radius: 0.2rem;\n  padding: 0 !important;\n}\n.dropdown-table .dropdown-toggle::after {\n  font-size: 14px !important;\n}\n.dropdown-table .dropdown-menu {\n  color: #FFFFFF !important;\n  background-color: #1D1E21 !important;\n  border: 1px solid #FFFFFF;\n  border-radius: 0.25rem;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  min-width: 156px;\n}\n.dropdown-table .dropdown-item {\n  color: #FFFFFF !important;\n  padding: 4px 5px !important;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n}\n.dropdown-table .dropdown-item span {\n  text-transform: uppercase;\n}\n.dropdown-table .dropdown-item:hover,\n.dropdown-table .dropdown-item:active,\n.dropdown-table .dropdown-item:focus {\n  background: #1D2B36 !important;\n}\n.date-cont {\n  margin: 20px 0;\n}\n.date-cont .mx-datepicker {\n  width: 106px;\n}\n.date-cont .mx-input {\n  width: 100%;\n  height: 36px !important;\n  padding: 6px 26px;\n  padding-left: 10px;\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px !important;\n  line-height: 16px;\n  color: #FFFFFF;\n  background-color: #1D1E21 !important;\n  border: 1px solid #FFFFFF !important;\n  border-radius: 4px;\n  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);\n}\n.date-cont .mx-icon-calendar,\n.date-cont .mx-icon-clear {\n  color: #FFFFFF !important;\n}\n.date-cont .cont-dates {\n  display: inline-flex;\n}\n.date-cont .date-separator {\n  font-family: Open Sans;\n  font-style: normal;\n  font-weight: bold;\n  font-size: 12px;\n  line-height: 16px;\n  text-transform: capitalize;\n  color: #FFFFFF;\n  margin: auto 10px;\n}\n.mx-datepicker-main {\n  font: 14px/1.5 Open Sans;\n  color: #FFFFFF !important;\n  background-color: #1D1E21 !important;\n  border: 1px solid #1D1E21;\n}", ""]);

// exports


/***/ }),

/***/ "./node_modules/moment/locale sync recursive ^\\.\\/.*$":
/*!**************************************************!*\
  !*** ./node_modules/moment/locale sync ^\.\/.*$ ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": "./node_modules/moment/locale/af.js",
	"./af.js": "./node_modules/moment/locale/af.js",
	"./ar": "./node_modules/moment/locale/ar.js",
	"./ar-dz": "./node_modules/moment/locale/ar-dz.js",
	"./ar-dz.js": "./node_modules/moment/locale/ar-dz.js",
	"./ar-kw": "./node_modules/moment/locale/ar-kw.js",
	"./ar-kw.js": "./node_modules/moment/locale/ar-kw.js",
	"./ar-ly": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ly.js": "./node_modules/moment/locale/ar-ly.js",
	"./ar-ma": "./node_modules/moment/locale/ar-ma.js",
	"./ar-ma.js": "./node_modules/moment/locale/ar-ma.js",
	"./ar-sa": "./node_modules/moment/locale/ar-sa.js",
	"./ar-sa.js": "./node_modules/moment/locale/ar-sa.js",
	"./ar-tn": "./node_modules/moment/locale/ar-tn.js",
	"./ar-tn.js": "./node_modules/moment/locale/ar-tn.js",
	"./ar.js": "./node_modules/moment/locale/ar.js",
	"./az": "./node_modules/moment/locale/az.js",
	"./az.js": "./node_modules/moment/locale/az.js",
	"./be": "./node_modules/moment/locale/be.js",
	"./be.js": "./node_modules/moment/locale/be.js",
	"./bg": "./node_modules/moment/locale/bg.js",
	"./bg.js": "./node_modules/moment/locale/bg.js",
	"./bm": "./node_modules/moment/locale/bm.js",
	"./bm.js": "./node_modules/moment/locale/bm.js",
	"./bn": "./node_modules/moment/locale/bn.js",
	"./bn-bd": "./node_modules/moment/locale/bn-bd.js",
	"./bn-bd.js": "./node_modules/moment/locale/bn-bd.js",
	"./bn.js": "./node_modules/moment/locale/bn.js",
	"./bo": "./node_modules/moment/locale/bo.js",
	"./bo.js": "./node_modules/moment/locale/bo.js",
	"./br": "./node_modules/moment/locale/br.js",
	"./br.js": "./node_modules/moment/locale/br.js",
	"./bs": "./node_modules/moment/locale/bs.js",
	"./bs.js": "./node_modules/moment/locale/bs.js",
	"./ca": "./node_modules/moment/locale/ca.js",
	"./ca.js": "./node_modules/moment/locale/ca.js",
	"./cs": "./node_modules/moment/locale/cs.js",
	"./cs.js": "./node_modules/moment/locale/cs.js",
	"./cv": "./node_modules/moment/locale/cv.js",
	"./cv.js": "./node_modules/moment/locale/cv.js",
	"./cy": "./node_modules/moment/locale/cy.js",
	"./cy.js": "./node_modules/moment/locale/cy.js",
	"./da": "./node_modules/moment/locale/da.js",
	"./da.js": "./node_modules/moment/locale/da.js",
	"./de": "./node_modules/moment/locale/de.js",
	"./de-at": "./node_modules/moment/locale/de-at.js",
	"./de-at.js": "./node_modules/moment/locale/de-at.js",
	"./de-ch": "./node_modules/moment/locale/de-ch.js",
	"./de-ch.js": "./node_modules/moment/locale/de-ch.js",
	"./de.js": "./node_modules/moment/locale/de.js",
	"./dv": "./node_modules/moment/locale/dv.js",
	"./dv.js": "./node_modules/moment/locale/dv.js",
	"./el": "./node_modules/moment/locale/el.js",
	"./el.js": "./node_modules/moment/locale/el.js",
	"./en-au": "./node_modules/moment/locale/en-au.js",
	"./en-au.js": "./node_modules/moment/locale/en-au.js",
	"./en-ca": "./node_modules/moment/locale/en-ca.js",
	"./en-ca.js": "./node_modules/moment/locale/en-ca.js",
	"./en-gb": "./node_modules/moment/locale/en-gb.js",
	"./en-gb.js": "./node_modules/moment/locale/en-gb.js",
	"./en-ie": "./node_modules/moment/locale/en-ie.js",
	"./en-ie.js": "./node_modules/moment/locale/en-ie.js",
	"./en-il": "./node_modules/moment/locale/en-il.js",
	"./en-il.js": "./node_modules/moment/locale/en-il.js",
	"./en-in": "./node_modules/moment/locale/en-in.js",
	"./en-in.js": "./node_modules/moment/locale/en-in.js",
	"./en-nz": "./node_modules/moment/locale/en-nz.js",
	"./en-nz.js": "./node_modules/moment/locale/en-nz.js",
	"./en-sg": "./node_modules/moment/locale/en-sg.js",
	"./en-sg.js": "./node_modules/moment/locale/en-sg.js",
	"./eo": "./node_modules/moment/locale/eo.js",
	"./eo.js": "./node_modules/moment/locale/eo.js",
	"./es": "./node_modules/moment/locale/es.js",
	"./es-do": "./node_modules/moment/locale/es-do.js",
	"./es-do.js": "./node_modules/moment/locale/es-do.js",
	"./es-mx": "./node_modules/moment/locale/es-mx.js",
	"./es-mx.js": "./node_modules/moment/locale/es-mx.js",
	"./es-us": "./node_modules/moment/locale/es-us.js",
	"./es-us.js": "./node_modules/moment/locale/es-us.js",
	"./es.js": "./node_modules/moment/locale/es.js",
	"./et": "./node_modules/moment/locale/et.js",
	"./et.js": "./node_modules/moment/locale/et.js",
	"./eu": "./node_modules/moment/locale/eu.js",
	"./eu.js": "./node_modules/moment/locale/eu.js",
	"./fa": "./node_modules/moment/locale/fa.js",
	"./fa.js": "./node_modules/moment/locale/fa.js",
	"./fi": "./node_modules/moment/locale/fi.js",
	"./fi.js": "./node_modules/moment/locale/fi.js",
	"./fil": "./node_modules/moment/locale/fil.js",
	"./fil.js": "./node_modules/moment/locale/fil.js",
	"./fo": "./node_modules/moment/locale/fo.js",
	"./fo.js": "./node_modules/moment/locale/fo.js",
	"./fr": "./node_modules/moment/locale/fr.js",
	"./fr-ca": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ca.js": "./node_modules/moment/locale/fr-ca.js",
	"./fr-ch": "./node_modules/moment/locale/fr-ch.js",
	"./fr-ch.js": "./node_modules/moment/locale/fr-ch.js",
	"./fr.js": "./node_modules/moment/locale/fr.js",
	"./fy": "./node_modules/moment/locale/fy.js",
	"./fy.js": "./node_modules/moment/locale/fy.js",
	"./ga": "./node_modules/moment/locale/ga.js",
	"./ga.js": "./node_modules/moment/locale/ga.js",
	"./gd": "./node_modules/moment/locale/gd.js",
	"./gd.js": "./node_modules/moment/locale/gd.js",
	"./gl": "./node_modules/moment/locale/gl.js",
	"./gl.js": "./node_modules/moment/locale/gl.js",
	"./gom-deva": "./node_modules/moment/locale/gom-deva.js",
	"./gom-deva.js": "./node_modules/moment/locale/gom-deva.js",
	"./gom-latn": "./node_modules/moment/locale/gom-latn.js",
	"./gom-latn.js": "./node_modules/moment/locale/gom-latn.js",
	"./gu": "./node_modules/moment/locale/gu.js",
	"./gu.js": "./node_modules/moment/locale/gu.js",
	"./he": "./node_modules/moment/locale/he.js",
	"./he.js": "./node_modules/moment/locale/he.js",
	"./hi": "./node_modules/moment/locale/hi.js",
	"./hi.js": "./node_modules/moment/locale/hi.js",
	"./hr": "./node_modules/moment/locale/hr.js",
	"./hr.js": "./node_modules/moment/locale/hr.js",
	"./hu": "./node_modules/moment/locale/hu.js",
	"./hu.js": "./node_modules/moment/locale/hu.js",
	"./hy-am": "./node_modules/moment/locale/hy-am.js",
	"./hy-am.js": "./node_modules/moment/locale/hy-am.js",
	"./id": "./node_modules/moment/locale/id.js",
	"./id.js": "./node_modules/moment/locale/id.js",
	"./is": "./node_modules/moment/locale/is.js",
	"./is.js": "./node_modules/moment/locale/is.js",
	"./it": "./node_modules/moment/locale/it.js",
	"./it-ch": "./node_modules/moment/locale/it-ch.js",
	"./it-ch.js": "./node_modules/moment/locale/it-ch.js",
	"./it.js": "./node_modules/moment/locale/it.js",
	"./ja": "./node_modules/moment/locale/ja.js",
	"./ja.js": "./node_modules/moment/locale/ja.js",
	"./jv": "./node_modules/moment/locale/jv.js",
	"./jv.js": "./node_modules/moment/locale/jv.js",
	"./ka": "./node_modules/moment/locale/ka.js",
	"./ka.js": "./node_modules/moment/locale/ka.js",
	"./kk": "./node_modules/moment/locale/kk.js",
	"./kk.js": "./node_modules/moment/locale/kk.js",
	"./km": "./node_modules/moment/locale/km.js",
	"./km.js": "./node_modules/moment/locale/km.js",
	"./kn": "./node_modules/moment/locale/kn.js",
	"./kn.js": "./node_modules/moment/locale/kn.js",
	"./ko": "./node_modules/moment/locale/ko.js",
	"./ko.js": "./node_modules/moment/locale/ko.js",
	"./ku": "./node_modules/moment/locale/ku.js",
	"./ku.js": "./node_modules/moment/locale/ku.js",
	"./ky": "./node_modules/moment/locale/ky.js",
	"./ky.js": "./node_modules/moment/locale/ky.js",
	"./lb": "./node_modules/moment/locale/lb.js",
	"./lb.js": "./node_modules/moment/locale/lb.js",
	"./lo": "./node_modules/moment/locale/lo.js",
	"./lo.js": "./node_modules/moment/locale/lo.js",
	"./lt": "./node_modules/moment/locale/lt.js",
	"./lt.js": "./node_modules/moment/locale/lt.js",
	"./lv": "./node_modules/moment/locale/lv.js",
	"./lv.js": "./node_modules/moment/locale/lv.js",
	"./me": "./node_modules/moment/locale/me.js",
	"./me.js": "./node_modules/moment/locale/me.js",
	"./mi": "./node_modules/moment/locale/mi.js",
	"./mi.js": "./node_modules/moment/locale/mi.js",
	"./mk": "./node_modules/moment/locale/mk.js",
	"./mk.js": "./node_modules/moment/locale/mk.js",
	"./ml": "./node_modules/moment/locale/ml.js",
	"./ml.js": "./node_modules/moment/locale/ml.js",
	"./mn": "./node_modules/moment/locale/mn.js",
	"./mn.js": "./node_modules/moment/locale/mn.js",
	"./mr": "./node_modules/moment/locale/mr.js",
	"./mr.js": "./node_modules/moment/locale/mr.js",
	"./ms": "./node_modules/moment/locale/ms.js",
	"./ms-my": "./node_modules/moment/locale/ms-my.js",
	"./ms-my.js": "./node_modules/moment/locale/ms-my.js",
	"./ms.js": "./node_modules/moment/locale/ms.js",
	"./mt": "./node_modules/moment/locale/mt.js",
	"./mt.js": "./node_modules/moment/locale/mt.js",
	"./my": "./node_modules/moment/locale/my.js",
	"./my.js": "./node_modules/moment/locale/my.js",
	"./nb": "./node_modules/moment/locale/nb.js",
	"./nb.js": "./node_modules/moment/locale/nb.js",
	"./ne": "./node_modules/moment/locale/ne.js",
	"./ne.js": "./node_modules/moment/locale/ne.js",
	"./nl": "./node_modules/moment/locale/nl.js",
	"./nl-be": "./node_modules/moment/locale/nl-be.js",
	"./nl-be.js": "./node_modules/moment/locale/nl-be.js",
	"./nl.js": "./node_modules/moment/locale/nl.js",
	"./nn": "./node_modules/moment/locale/nn.js",
	"./nn.js": "./node_modules/moment/locale/nn.js",
	"./oc-lnc": "./node_modules/moment/locale/oc-lnc.js",
	"./oc-lnc.js": "./node_modules/moment/locale/oc-lnc.js",
	"./pa-in": "./node_modules/moment/locale/pa-in.js",
	"./pa-in.js": "./node_modules/moment/locale/pa-in.js",
	"./pl": "./node_modules/moment/locale/pl.js",
	"./pl.js": "./node_modules/moment/locale/pl.js",
	"./pt": "./node_modules/moment/locale/pt.js",
	"./pt-br": "./node_modules/moment/locale/pt-br.js",
	"./pt-br.js": "./node_modules/moment/locale/pt-br.js",
	"./pt.js": "./node_modules/moment/locale/pt.js",
	"./ro": "./node_modules/moment/locale/ro.js",
	"./ro.js": "./node_modules/moment/locale/ro.js",
	"./ru": "./node_modules/moment/locale/ru.js",
	"./ru.js": "./node_modules/moment/locale/ru.js",
	"./sd": "./node_modules/moment/locale/sd.js",
	"./sd.js": "./node_modules/moment/locale/sd.js",
	"./se": "./node_modules/moment/locale/se.js",
	"./se.js": "./node_modules/moment/locale/se.js",
	"./si": "./node_modules/moment/locale/si.js",
	"./si.js": "./node_modules/moment/locale/si.js",
	"./sk": "./node_modules/moment/locale/sk.js",
	"./sk.js": "./node_modules/moment/locale/sk.js",
	"./sl": "./node_modules/moment/locale/sl.js",
	"./sl.js": "./node_modules/moment/locale/sl.js",
	"./sq": "./node_modules/moment/locale/sq.js",
	"./sq.js": "./node_modules/moment/locale/sq.js",
	"./sr": "./node_modules/moment/locale/sr.js",
	"./sr-cyrl": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr-cyrl.js": "./node_modules/moment/locale/sr-cyrl.js",
	"./sr.js": "./node_modules/moment/locale/sr.js",
	"./ss": "./node_modules/moment/locale/ss.js",
	"./ss.js": "./node_modules/moment/locale/ss.js",
	"./sv": "./node_modules/moment/locale/sv.js",
	"./sv.js": "./node_modules/moment/locale/sv.js",
	"./sw": "./node_modules/moment/locale/sw.js",
	"./sw.js": "./node_modules/moment/locale/sw.js",
	"./ta": "./node_modules/moment/locale/ta.js",
	"./ta.js": "./node_modules/moment/locale/ta.js",
	"./te": "./node_modules/moment/locale/te.js",
	"./te.js": "./node_modules/moment/locale/te.js",
	"./tet": "./node_modules/moment/locale/tet.js",
	"./tet.js": "./node_modules/moment/locale/tet.js",
	"./tg": "./node_modules/moment/locale/tg.js",
	"./tg.js": "./node_modules/moment/locale/tg.js",
	"./th": "./node_modules/moment/locale/th.js",
	"./th.js": "./node_modules/moment/locale/th.js",
	"./tk": "./node_modules/moment/locale/tk.js",
	"./tk.js": "./node_modules/moment/locale/tk.js",
	"./tl-ph": "./node_modules/moment/locale/tl-ph.js",
	"./tl-ph.js": "./node_modules/moment/locale/tl-ph.js",
	"./tlh": "./node_modules/moment/locale/tlh.js",
	"./tlh.js": "./node_modules/moment/locale/tlh.js",
	"./tr": "./node_modules/moment/locale/tr.js",
	"./tr.js": "./node_modules/moment/locale/tr.js",
	"./tzl": "./node_modules/moment/locale/tzl.js",
	"./tzl.js": "./node_modules/moment/locale/tzl.js",
	"./tzm": "./node_modules/moment/locale/tzm.js",
	"./tzm-latn": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm-latn.js": "./node_modules/moment/locale/tzm-latn.js",
	"./tzm.js": "./node_modules/moment/locale/tzm.js",
	"./ug-cn": "./node_modules/moment/locale/ug-cn.js",
	"./ug-cn.js": "./node_modules/moment/locale/ug-cn.js",
	"./uk": "./node_modules/moment/locale/uk.js",
	"./uk.js": "./node_modules/moment/locale/uk.js",
	"./ur": "./node_modules/moment/locale/ur.js",
	"./ur.js": "./node_modules/moment/locale/ur.js",
	"./uz": "./node_modules/moment/locale/uz.js",
	"./uz-latn": "./node_modules/moment/locale/uz-latn.js",
	"./uz-latn.js": "./node_modules/moment/locale/uz-latn.js",
	"./uz.js": "./node_modules/moment/locale/uz.js",
	"./vi": "./node_modules/moment/locale/vi.js",
	"./vi.js": "./node_modules/moment/locale/vi.js",
	"./x-pseudo": "./node_modules/moment/locale/x-pseudo.js",
	"./x-pseudo.js": "./node_modules/moment/locale/x-pseudo.js",
	"./yo": "./node_modules/moment/locale/yo.js",
	"./yo.js": "./node_modules/moment/locale/yo.js",
	"./zh-cn": "./node_modules/moment/locale/zh-cn.js",
	"./zh-cn.js": "./node_modules/moment/locale/zh-cn.js",
	"./zh-hk": "./node_modules/moment/locale/zh-hk.js",
	"./zh-hk.js": "./node_modules/moment/locale/zh-hk.js",
	"./zh-mo": "./node_modules/moment/locale/zh-mo.js",
	"./zh-mo.js": "./node_modules/moment/locale/zh-mo.js",
	"./zh-tw": "./node_modules/moment/locale/zh-tw.js",
	"./zh-tw.js": "./node_modules/moment/locale/zh-tw.js"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./node_modules/moment/locale sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./authTooltip.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./cardCarouser.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./donutChart.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./inviteRaf.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./modalPocket.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./navFooter.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./navHeader.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./passwordSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./statsAffiliates.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./statsReferrer.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./tableHome.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./telegramSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./twoFaSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./userProfile.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Faq.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&":
/*!******************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss& ***!
  \******************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./HowToPlay.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Login2fa.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ProvablyFair.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ReferFriends.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&":
/*!*****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss& ***!
  \*****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Support.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/style-loader!./node_modules/css-loader!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src??ref--7-2!./node_modules/sass-loader/dist/cjs.js??ref--7-3!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {


var content = __webpack_require__(/*! !../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Transactions.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&");

if(typeof content === 'string') content = [[module.i, content, '']];

var transform;
var insertInto;



var options = {"hmr":true}

options.transform = transform
options.insertInto = undefined;

var update = __webpack_require__(/*! ../../../node_modules/style-loader/lib/addStyles.js */ "./node_modules/style-loader/lib/addStyles.js")(content, options);

if(content.locals) module.exports = content.locals;

if(false) {}

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=template&id=332fccf4&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/App.vue?vue&type=template&id=332fccf4& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { attrs: { id: "AppContainer" } },
    [
      _c("nav-header"),
      _vm._v(" "),
      _c("router-view"),
      _vm._v(" "),
      _c("nav-footer")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640&":
/*!*************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640& ***!
  \*************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("div", { staticClass: "tfa-setting" }, [
        _c("div", { staticClass: "text-head" }, [
          _vm._v(
            "\n            You can delete your account without the possibility of recovery. This action is only available when the balance of each account is less that the minimum transfer amount of the corresponding cryptocurrency and there are no virtual currency balances. There must also be no active margins or purses connected to this account.\n        "
          )
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "cont-qr" }, [
          _c(
            "button",
            {
              staticClass: "btn btn-create btn-delete",
              attrs: { type: "submit" }
            },
            [_vm._v("\n            delete account\n            ")]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "recent-link" }, [
    _c("img", {
      staticClass: "icon-info",
      attrs: { src: __webpack_require__(/*! ../../img/info.svg */ "./resources/img/info.svg"), alt: "" }
    }),
    _vm._v(" "),
    _vm._m(0)
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("span", { staticClass: "hovercard" }, [
      _c("div", { staticClass: "tooltiptext" }, [
        _vm._v("\n      • Minimum length is 6 characters, maximum is 16"),
        _c("br"),
        _vm._v(
          "\n      • Can only contain letters and numbers, caps sensitive"
        ),
        _c("br"),
        _vm._v("\n      • Cannot contain symbols or spaces"),
        _c("br"),
        _vm._v("\n      • Once created, cannot be changed or deleted"),
        _c("br")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "carousel-card" }, [
    _c("div", { staticClass: "car-top" }, [
      _c("img", {
        staticClass: "game-img",
        attrs: { src: __webpack_require__("./resources/img sync recursive ^\\.\\/.*$")("./" + _vm.imageUrl), alt: "" }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "text-game" }, [
        _vm._v("\n            " + _vm._s(_vm.titles) + "\n        ")
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "middle" }, [
        _c("img", {
          staticClass: "play-img",
          attrs: { src: __webpack_require__(/*! ../../img/play-game.svg */ "./resources/img/play-game.svg"), alt: "" }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "bg-car-top" })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "car-bottom", style: _vm.bgStyles }, [
      _c(
        "div",
        { staticClass: "content-bottom" },
        [
          _c(
            "b-row",
            { attrs: { "no-gutters": "" } },
            [
              _c(
                "b-col",
                { staticClass: "text-center", attrs: { cols: "12", md: "6" } },
                [
                  _c("donut-chart", {
                    staticClass: "donut-mobile",
                    attrs: { rateDay: _vm.rateWin }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-col",
                {
                  staticStyle: { margin: "auto" },
                  attrs: { cols: "12", md: "6" }
                },
                [
                  _c(
                    "div",
                    { staticClass: "cont-bot" },
                    [
                      _c("div", { staticClass: "text-head text-left" }, [
                        _vm._v(
                          "\n                          Biggest Win\n                      "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "b-row",
                        {
                          staticClass: "text-cont",
                          attrs: { "no-gutters": "" }
                        },
                        [
                          _c("b-col", { staticClass: "text-left" }, [
                            _vm._v(
                              "\n                              User1245\n                          "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-col",
                            {
                              staticClass: "text-left",
                              attrs: { cols: "6", md: "7" }
                            },
                            [
                              _vm._v(
                                "\n                              1.28x\n                          "
                              )
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    [
                      _c("div", { staticClass: "text-head text-left" }, [
                        _vm._v(
                          "\n                          Recent Win\n                      "
                        )
                      ]),
                      _vm._v(" "),
                      _c(
                        "b-row",
                        {
                          staticClass: "text-cont",
                          attrs: { "no-gutters": "" }
                        },
                        [
                          _c("b-col", { staticClass: "text-left" }, [
                            _vm._v(
                              "\n                              User1245\n                          "
                            )
                          ]),
                          _vm._v(" "),
                          _c(
                            "b-col",
                            {
                              staticClass: "text-left",
                              attrs: { cols: "6", md: "7" }
                            },
                            [
                              _vm._v(
                                "\n                              88.28000000 "
                              ),
                              _c("img", {
                                staticClass: "icon-algo",
                                attrs: {
                                  src: __webpack_require__(/*! ../../img/tokenAlgo.svg */ "./resources/img/tokenAlgo.svg"),
                                  alt: ""
                                }
                              })
                            ]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ]
              )
            ],
            1
          )
        ],
        1
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067&":
/*!*************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067& ***!
  \*************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "donut",
      style: { "--first": this.rate1, "--second": this.rate2 }
    },
    [
      _c("div", { staticClass: "donut__slice donut__slice__first" }),
      _vm._v(" "),
      _c("div", { staticClass: "donut__slice donut__slice__second" }),
      _vm._v(" "),
      _c("div", { staticClass: "donut__label" }, [
        _c("div", { staticClass: "donut__label__heading" }, [
          _vm._v("\n        " + _vm._s(_vm.rateDay) + "\n        ")
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "donut__label__sub" }, [
          _vm._v("\n        24H Win %\n        ")
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "invite-raf" }, [
    _c("div", { staticClass: "text-head text-left" }, [
      _vm._v("\n        Spread The Word, Earn Money\n    ")
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "sub-text" }, [
      _vm._v(
        "\n      Refer your friends and earn money! You will receive a referral commission for every user that registers through your links and plays. Unlike traditional affilate programs, you will earn commission for all bets placed which means that regardless if they win or lose, you still make the same commission!  \n    "
      )
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c(
          "b-row",
          { attrs: { "no-gutters": "" } },
          [
            _c(
              "b-col",
              { staticClass: "col-copy-1 text-center", attrs: { cols: "6" } },
              [
                _c(
                  "b-row",
                  {
                    staticStyle: { "margin-bottom": "4px" },
                    attrs: { "no-gutters": "" }
                  },
                  [
                    _c("b-col", { staticClass: "text-left text-prof" }, [
                      _vm._v(
                        "\n                    Your Affiliate Code\n                  "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-input-group",
                  { staticStyle: { height: "36px !important" } },
                  [
                    _c("b-form-input", {
                      staticClass: "dep-input",
                      attrs: { id: "affiliate-code", readonly: "" },
                      model: {
                        value: _vm.affCode,
                        callback: function($$v) {
                          _vm.affCode = $$v
                        },
                        expression: "affCode"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "b-input-group-append",
                      [
                        _c("b-button", { staticClass: "dep-button" }, [
                          _c("img", {
                            staticStyle: { height: "20px", width: "20px" },
                            attrs: {
                              src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                              alt: ""
                            }
                          })
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { staticClass: "col-copy-2 text-center", attrs: { cols: "6" } },
              [
                _c(
                  "b-row",
                  {
                    staticStyle: { "margin-bottom": "4px" },
                    attrs: { "no-gutters": "" }
                  },
                  [
                    _c("b-col", { staticClass: "text-left text-prof" }, [
                      _vm._v(
                        "\n                    Your Invitation Link\n                  "
                      )
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _c(
                  "b-input-group",
                  { staticStyle: { height: "36px !important" } },
                  [
                    _c("b-form-input", {
                      staticClass: "dep-input",
                      attrs: { id: "invite-link", readonly: "" },
                      model: {
                        value: _vm.inviteLink,
                        callback: function($$v) {
                          _vm.inviteLink = $$v
                        },
                        expression: "inviteLink"
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "b-input-group-append",
                      [
                        _c("b-button", { staticClass: "dep-button" }, [
                          _c("img", {
                            staticStyle: { height: "20px", width: "20px" },
                            attrs: {
                              src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                              alt: ""
                            }
                          })
                        ])
                      ],
                      1
                    )
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c(
          "b-row",
          { attrs: { "no-gutters": "" } },
          [
            _c(
              "b-col",
              { staticClass: "col-game text-center", attrs: { cols: "6" } },
              [
                _c(
                  "b-row",
                  { staticClass: "game-head", attrs: { "no-gutters": "" } },
                  [
                    _c(
                      "b-col",
                      { staticClass: "text-left", attrs: { cols: "6" } },
                      [
                        _vm._v(
                          "\n                        Games\n                    "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "b-col",
                      { staticClass: "text-right", attrs: { cols: "6" } },
                      [
                        _vm._v(
                          "\n                        Commission\n                    "
                        )
                      ]
                    )
                  ],
                  1
                ),
                _vm._v(" "),
                _vm._l(_vm.games, function(game, index) {
                  return _c(
                    "b-row",
                    {
                      key: index,
                      staticClass: "mb-3",
                      attrs: { "no-gutters": "" }
                    },
                    [
                      _c(
                        "b-col",
                        { staticClass: "text-left", attrs: { cols: "6" } },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(game.name) +
                              "\n                    "
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "b-col",
                        {
                          staticClass: "text-right comm-cont",
                          attrs: { cols: "6" }
                        },
                        [
                          _vm._v(
                            "\n                        " +
                              _vm._s(game.commision) +
                              "%\n                    "
                          )
                        ]
                      )
                    ],
                    1
                  )
                })
              ],
              2
            )
          ],
          1
        )
      ],
      1
    ),
    _vm._v(" "),
    _c("div", { staticClass: "last-text" }, [
      _vm._v(
        "\n      If you are an affiliate under special circumstance or with outstanding reach, contact our affiliate team at hello@huatbet.com. Upon speaking to your affiliate manager, we may be able to negotiate a customized program. This could include higher commission. Please note that only appropriate affiliates will be considered.\n    "
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "jumbotron" },
    [
      _c(
        "b-jumbotron",
        {
          staticClass: "mx-auto",
          scopedSlots: _vm._u([
            {
              key: "header",
              fn: function() {
                return [_vm._v("Welcome to")]
              },
              proxy: true
            }
          ])
        },
        [
          _vm._v(" "),
          _c("div", { staticClass: "image-jumbo" }, [
            _c("img", {
              attrs: { src: __webpack_require__(/*! ../../img/Huatbet-02.svg */ "./resources/img/Huatbet-02.svg"), alt: "" }
            })
          ])
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _vm.modalVal
      ? _c("div", {
          staticClass: "backdrop-a",
          on: {
            click: function($event) {
              return _vm.modHide()
            }
          }
        })
      : _vm._e(),
    _vm._v(" "),
    _vm.modalVal
      ? _c(
          "div",
          { staticClass: "modal-deposit mx-auto" },
          [
            !_vm.succesDraw
              ? _c(
                  "b-row",
                  { attrs: { "no-gutters": "" } },
                  [
                    _c(
                      "b-col",
                      {
                        staticClass: "text-left moon-head",
                        attrs: { cols: "12" }
                      },
                      [
                        _c("img", {
                          staticStyle: {
                            "padding-bottom": "4px",
                            height: "14px: weight:14px"
                          },
                          attrs: { src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"), alt: "" }
                        }),
                        _vm._v(
                          "\n        " +
                            _vm._s(_vm.headerNames[_vm.selectedTab]) +
                            "\n      "
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "b-col",
                      { staticClass: "deposit-body", attrs: { cols: "12" } },
                      [
                        _c(
                          "b-card",
                          {
                            staticStyle: {
                              background: "transparent !important",
                              border: "none !important"
                            },
                            attrs: { "no-body": "" }
                          },
                          [
                            _c(
                              "b-tabs",
                              {
                                attrs: {
                                  pills: "",
                                  card: "",
                                  "active-nav-item-class": "choosen-tab",
                                  "nav-class": "tab-aktive",
                                  "active-tab-class": "aktive-content"
                                },
                                on: { "activate-tab": _vm.tabChanged }
                              },
                              [
                                _c(
                                  "b-tab",
                                  {
                                    attrs: { active: "" },
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "title",
                                          fn: function() {
                                            return [_vm._v(" Deposit ")]
                                          },
                                          proxy: true
                                        }
                                      ],
                                      null,
                                      false,
                                      3176120621
                                    )
                                  },
                                  [
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "deposit-content text-center"
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "choose-token" },
                                          [
                                            _c(
                                              "b-dropdown",
                                              {
                                                attrs: {
                                                  size: "sm",
                                                  "toggle-class":
                                                    "text-decoration-none",
                                                  center: "",
                                                  "no-caret": ""
                                                },
                                                scopedSlots: _vm._u(
                                                  [
                                                    {
                                                      key: "button-content",
                                                      fn: function() {
                                                        return [
                                                          _c("input", {
                                                            directives: [
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  _vm.tokenName,
                                                                expression:
                                                                  "tokenName"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "custom-select",
                                                            attrs: {
                                                              type: "text",
                                                              disabled: ""
                                                            },
                                                            domProps: {
                                                              value:
                                                                _vm.tokenName
                                                            },
                                                            on: {
                                                              input: function(
                                                                $event
                                                              ) {
                                                                if (
                                                                  $event.target
                                                                    .composing
                                                                ) {
                                                                  return
                                                                }
                                                                _vm.tokenName =
                                                                  $event.target.value
                                                              }
                                                            }
                                                          }),
                                                          _vm._v(" "),
                                                          _c("img", {
                                                            staticStyle: {
                                                              position:
                                                                "absolute",
                                                              left: "17%",
                                                              top: "50%",
                                                              transform:
                                                                "translate(-50%, -50%)"
                                                            },
                                                            attrs: {
                                                              src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                                              alt: ""
                                                            }
                                                          })
                                                        ]
                                                      },
                                                      proxy: true
                                                    }
                                                  ],
                                                  null,
                                                  false,
                                                  1016038855
                                                )
                                              },
                                              [
                                                _vm._v(" "),
                                                _vm._l(_vm.tokenList, function(
                                                  token,
                                                  index
                                                ) {
                                                  return _c(
                                                    "b-dropdown-item",
                                                    {
                                                      key: index,
                                                      attrs: { selected: "" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.changeDepositToken(
                                                            index
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticStyle: {
                                                            "margin-left":
                                                              "8px",
                                                            "padding-bottom":
                                                              "3px",
                                                            display: "inline"
                                                          }
                                                        },
                                                        [
                                                          _c("img", {
                                                            staticStyle: {
                                                              "padding-bottom":
                                                                "3px",
                                                              height:
                                                                "14px: weight:14px"
                                                            },
                                                            attrs: {
                                                              src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                                              alt: ""
                                                            }
                                                          })
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticStyle: {
                                                            "margin-left": "6px"
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(token.name)
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                })
                                              ],
                                              2
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "cont-input mt-3" },
                                          [
                                            _c(
                                              "b-row",
                                              {
                                                staticStyle: {
                                                  "margin-bottom": "4px"
                                                },
                                                attrs: { "no-gutters": "" }
                                              },
                                              [
                                                _c(
                                                  "b-col",
                                                  {
                                                    staticClass:
                                                      "text-left text-prof"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                      Your " +
                                                        _vm._s(_vm.tokenName) +
                                                        " Deposit Address\n                    "
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "b-input-group",
                                              {
                                                staticStyle: {
                                                  height: "36px !important"
                                                }
                                              },
                                              [
                                                _c("b-form-input", {
                                                  staticClass: "dep-input",
                                                  attrs: {
                                                    id: "deposit-address",
                                                    readonly: ""
                                                  },
                                                  model: {
                                                    value: _vm.depValue,
                                                    callback: function($$v) {
                                                      _vm.depValue = $$v
                                                    },
                                                    expression: "depValue"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "b-input-group-append",
                                                  [
                                                    _c(
                                                      "b-button",
                                                      {
                                                        staticClass:
                                                          "dep-button"
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticStyle: {
                                                            height: "20px",
                                                            width: "20px"
                                                          },
                                                          attrs: {
                                                            src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                                            alt: ""
                                                          },
                                                          on: {
                                                            click: _vm.copy
                                                          }
                                                        })
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        )
                                      ]
                                    )
                                  ]
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-tab",
                                  {
                                    scopedSlots: _vm._u(
                                      [
                                        {
                                          key: "title",
                                          fn: function() {
                                            return [_vm._v(" Withdraw ")]
                                          },
                                          proxy: true
                                        }
                                      ],
                                      null,
                                      false,
                                      2929857183
                                    )
                                  },
                                  [
                                    _vm._v(" "),
                                    _c(
                                      "div",
                                      {
                                        staticClass:
                                          "withdraw-content text-center"
                                      },
                                      [
                                        _c(
                                          "div",
                                          { staticClass: "choose-token" },
                                          [
                                            _c(
                                              "b-dropdown",
                                              {
                                                attrs: {
                                                  size: "sm",
                                                  "toggle-class":
                                                    "text-decoration-none",
                                                  center: "",
                                                  "no-caret": ""
                                                },
                                                scopedSlots: _vm._u(
                                                  [
                                                    {
                                                      key: "button-content",
                                                      fn: function() {
                                                        return [
                                                          _c("input", {
                                                            directives: [
                                                              {
                                                                name: "model",
                                                                rawName:
                                                                  "v-model",
                                                                value:
                                                                  _vm.tokenName,
                                                                expression:
                                                                  "tokenName"
                                                              }
                                                            ],
                                                            staticClass:
                                                              "custom-select",
                                                            attrs: {
                                                              type: "text",
                                                              disabled: ""
                                                            },
                                                            domProps: {
                                                              value:
                                                                _vm.tokenName
                                                            },
                                                            on: {
                                                              input: function(
                                                                $event
                                                              ) {
                                                                if (
                                                                  $event.target
                                                                    .composing
                                                                ) {
                                                                  return
                                                                }
                                                                _vm.tokenName =
                                                                  $event.target.value
                                                              }
                                                            }
                                                          }),
                                                          _vm._v(" "),
                                                          _c("img", {
                                                            staticStyle: {
                                                              position:
                                                                "absolute",
                                                              left: "17%",
                                                              top: "50%",
                                                              transform:
                                                                "translate(-50%, -50%)"
                                                            },
                                                            attrs: {
                                                              src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                                              alt: ""
                                                            }
                                                          })
                                                        ]
                                                      },
                                                      proxy: true
                                                    }
                                                  ],
                                                  null,
                                                  false,
                                                  1016038855
                                                )
                                              },
                                              [
                                                _vm._v(" "),
                                                _vm._l(_vm.tokenList, function(
                                                  token,
                                                  index
                                                ) {
                                                  return _c(
                                                    "b-dropdown-item",
                                                    {
                                                      key: index,
                                                      attrs: { selected: "" },
                                                      on: {
                                                        click: function(
                                                          $event
                                                        ) {
                                                          return _vm.changeDepositToken(
                                                            index
                                                          )
                                                        }
                                                      }
                                                    },
                                                    [
                                                      _c(
                                                        "div",
                                                        {
                                                          staticStyle: {
                                                            "margin-left":
                                                              "8px",
                                                            "padding-bottom":
                                                              "3px",
                                                            display: "inline"
                                                          }
                                                        },
                                                        [
                                                          _c("img", {
                                                            staticStyle: {
                                                              "padding-bottom":
                                                                "3px",
                                                              height:
                                                                "14px: weight:14px"
                                                            },
                                                            attrs: {
                                                              src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                                              alt: ""
                                                            }
                                                          })
                                                        ]
                                                      ),
                                                      _vm._v(" "),
                                                      _c(
                                                        "span",
                                                        {
                                                          staticStyle: {
                                                            "margin-left": "6px"
                                                          }
                                                        },
                                                        [
                                                          _vm._v(
                                                            _vm._s(token.name)
                                                          )
                                                        ]
                                                      )
                                                    ]
                                                  )
                                                })
                                              ],
                                              2
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "cont-input mt-3" },
                                          [
                                            _c(
                                              "b-row",
                                              {
                                                staticStyle: {
                                                  "margin-bottom": "4px"
                                                },
                                                attrs: { "no-gutters": "" }
                                              },
                                              [
                                                _c(
                                                  "b-col",
                                                  {
                                                    staticClass:
                                                      "text-left text-prof"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                      Your " +
                                                        _vm._s(_vm.tokenName) +
                                                        " Deposit Address\n                    "
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "b-input-group",
                                              {
                                                staticStyle: {
                                                  height: "36px !important"
                                                }
                                              },
                                              [
                                                _c("b-form-input", {
                                                  staticClass: "dep-input",
                                                  model: {
                                                    value: _vm.withdrawAddress,
                                                    callback: function($$v) {
                                                      _vm.withdrawAddress = $$v
                                                    },
                                                    expression:
                                                      "withdrawAddress"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "b-input-group-append",
                                                  [
                                                    _c(
                                                      "b-button",
                                                      {
                                                        staticClass:
                                                          "dep-button"
                                                      },
                                                      [
                                                        _c("img", {
                                                          staticStyle: {
                                                            height: "20px",
                                                            width: "20px"
                                                          },
                                                          attrs: {
                                                            src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                                            alt: ""
                                                          }
                                                        })
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "div",
                                          { staticClass: "cont-input" },
                                          [
                                            _c(
                                              "b-row",
                                              {
                                                staticStyle: {
                                                  "margin-bottom": "4px"
                                                },
                                                attrs: { "no-gutters": "" }
                                              },
                                              [
                                                _c(
                                                  "b-col",
                                                  {
                                                    staticClass:
                                                      "text-left text-prof"
                                                  },
                                                  [
                                                    _vm._v(
                                                      "\n                      Amount (Min.\n                      " +
                                                        _vm._s(
                                                          this.$store.state
                                                            .wallet
                                                            .minimal_withdraw
                                                        ) +
                                                        ")\n                    "
                                                    )
                                                  ]
                                                )
                                              ],
                                              1
                                            ),
                                            _vm._v(" "),
                                            _c(
                                              "b-input-group",
                                              {
                                                staticStyle: {
                                                  height: "36px !important"
                                                }
                                              },
                                              [
                                                _c("b-form-input", {
                                                  staticClass: "am-input",
                                                  model: {
                                                    value: _vm.withdrawAmount,
                                                    callback: function($$v) {
                                                      _vm.withdrawAmount = $$v
                                                    },
                                                    expression: "withdrawAmount"
                                                  }
                                                }),
                                                _vm._v(" "),
                                                _c(
                                                  "b-input-group-append",
                                                  [
                                                    _c(
                                                      "b-button",
                                                      {
                                                        staticClass: "max-btn",
                                                        on: {
                                                          click: _vm.maxAmount
                                                        }
                                                      },
                                                      [
                                                        _vm._v(
                                                          "\n                        MAX\n                      "
                                                        )
                                                      ]
                                                    )
                                                  ],
                                                  1
                                                )
                                              ],
                                              1
                                            )
                                          ],
                                          1
                                        ),
                                        _vm._v(" "),
                                        _vm.isError
                                          ? _c(
                                              "b-col",
                                              {
                                                staticStyle: {
                                                  "padding-bottom": "10px"
                                                },
                                                attrs: { cols: "12" }
                                              },
                                              [
                                                _c(
                                                  "div",
                                                  { staticClass: "text-error" },
                                                  [
                                                    _vm._v(
                                                      "\n                    " +
                                                        _vm._s(
                                                          _vm.errorMessage
                                                        ) +
                                                        "\n                  "
                                                    )
                                                  ]
                                                )
                                              ]
                                            )
                                          : _vm._e(),
                                        _vm._v(" "),
                                        _c(
                                          "button",
                                          {
                                            staticClass: "btn btn-create",
                                            attrs: {
                                              type: "submit",
                                              disabled: _vm.withdrawalLoading
                                            },
                                            on: {
                                              click: function($event) {
                                                return _vm.withdraw()
                                              }
                                            }
                                          },
                                          [
                                            _vm._v(
                                              "\n                  WITHDRAW\n                "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _vm.$store.state.wallet
                                          .withdrawalFees != null
                                          ? _c(
                                              "div",
                                              { staticClass: "fees-text" },
                                              [
                                                _vm._v(
                                                  "\n                  There will be a fee of\n                  " +
                                                    _vm._s(
                                                      _vm.$store.state.wallet
                                                        .withdrawalFees
                                                    ) +
                                                    " " +
                                                    _vm._s(_vm.tokenName) +
                                                    " for\n                  external transactions.\n                "
                                                )
                                              ]
                                            )
                                          : _vm._e()
                                      ],
                                      1
                                    )
                                  ]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      1
                    )
                  ],
                  1
                )
              : _c(
                  "b-row",
                  {
                    staticClass: "succes-deposit",
                    attrs: { "no-gutters": "" }
                  },
                  [
                    _c(
                      "b-col",
                      { staticClass: "text-right", attrs: { cols: "12" } },
                      [
                        _c(
                          "div",
                          {
                            staticClass: "x-head",
                            on: {
                              click: function($event) {
                                return _vm.modHide()
                              }
                            }
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "x-but",
                                attrs: { type: "button" }
                              },
                              [
                                _c("b-icon", {
                                  attrs: { icon: "x", "font-scale": "1.5" }
                                })
                              ],
                              1
                            )
                          ]
                        )
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "b-col",
                      { staticClass: "text-center", attrs: { cols: "12" } },
                      [
                        _c("div", { staticClass: "text-succ" }, [
                          _vm._v("TRANSACTION SUCCESSFUL!")
                        ])
                      ]
                    ),
                    _vm._v(" "),
                    _c(
                      "b-col",
                      { staticClass: "text-center", attrs: { cols: "12" } },
                      [
                        _c("img", {
                          attrs: {
                            src: __webpack_require__(/*! ../../img/succes-info.svg */ "./resources/img/succes-info.svg"),
                            alt: ""
                          }
                        })
                      ]
                    )
                  ],
                  1
                )
          ],
          1
        )
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "footer-nav" },
    [
      _c(
        "b-navbar",
        { staticClass: "navbar mx-auto hide-nav" },
        [
          _c(
            "b-navbar-nav",
            [
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/" } },
                    [
                      _c("b-nav-text", { staticClass: "footer-brand" }, [
                        _vm._v(
                          "\n                © Huatbet 2020\n                "
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/howto" } },
                    [
                      _c("b-nav-text", { staticClass: "footer-menu" }, [
                        _vm._v(
                          "\n                How To Play\n                "
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/faq" } },
                    [
                      _c("b-nav-text", { staticClass: "footer-menu" }, [
                        _vm._v("\n                FAQ\n                ")
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/provablyfair" } },
                    [
                      _c("b-nav-text", { staticClass: "footer-menu" }, [
                        _vm._v(
                          "\n                Provably Fair\n                "
                        )
                      ])
                    ],
                    1
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c(
                    "router-link",
                    { attrs: { to: "/support" } },
                    [
                      _c("b-nav-text", { staticClass: "footer-menu" }, [
                        _vm._v("\n                Support\n                ")
                      ])
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-navbar-nav",
            { staticClass: "ml-auto" },
            [
              _c(
                "b-nav-item",
                { staticClass: "nav-item-foot" },
                [
                  _c("b-nav-text", { staticClass: "footer-brand" }, [
                    _vm._v(
                      "\n                hello@huatbet.com\n                "
                    )
                  ])
                ],
                1
              ),
              _vm._v(" "),
              _c("b-nav-item", { staticClass: "nav-item-foot" }, [
                _c("img", {
                  staticClass: "footer-icon",
                  attrs: { src: __webpack_require__(/*! ../../img/telegram.svg */ "./resources/img/telegram.svg"), alt: "" }
                })
              ]),
              _vm._v(" "),
              _c("b-nav-item", { staticClass: "nav-item-foot" }, [
                _c("img", {
                  staticClass: "footer-icon",
                  attrs: { src: __webpack_require__(/*! ../../img/twitter.svg */ "./resources/img/twitter.svg"), alt: "" }
                })
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "header-nav" },
    [
      _c(
        "b-navbar",
        { staticClass: "navbar navbar-container-first mx-auto" },
        [
          _c(
            "b-navbar-brand",
            { staticClass: "brand-one d-inline-block" },
            [
              _c("router-link", { attrs: { to: "/" } }, [
                _c("div", { staticClass: "img-brand" })
              ])
            ],
            1
          ),
          _vm._v(" "),
          _vm.$store.state.isAuth
            ? _c("b-nav-form", { staticClass: "select-top" }, [
                _c(
                  "div",
                  { staticClass: "input-group input-group-sm mb-3" },
                  [
                    _c(
                      "b-dropdown",
                      {
                        attrs: {
                          size: "sm",
                          "toggle-class": "text-decoration-none",
                          center: "",
                          "no-caret": ""
                        },
                        scopedSlots: _vm._u(
                          [
                            {
                              key: "button-content",
                              fn: function() {
                                return [
                                  _c("input", {
                                    directives: [
                                      {
                                        name: "model",
                                        rawName: "v-model",
                                        value: _vm.getTokenbalance,
                                        expression: "getTokenbalance"
                                      }
                                    ],
                                    staticClass: "custom-select",
                                    staticStyle: { width: "130px" },
                                    attrs: {
                                      type: "text",
                                      maxlength: "18",
                                      disabled: ""
                                    },
                                    domProps: { value: _vm.getTokenbalance },
                                    on: {
                                      input: function($event) {
                                        if ($event.target.composing) {
                                          return
                                        }
                                        _vm.getTokenbalance =
                                          $event.target.value
                                      }
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c("img", {
                                    staticStyle: {
                                      position: "absolute",
                                      right: "11%",
                                      top: "50%",
                                      transform: "translate(-50%, -50%)"
                                    },
                                    attrs: {
                                      src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                      alt: ""
                                    }
                                  })
                                ]
                              },
                              proxy: true
                            }
                          ],
                          null,
                          false,
                          1401054452
                        )
                      },
                      [
                        _vm._v(" "),
                        _vm._l(_vm.tokenList, function(token, index) {
                          return _c(
                            "b-dropdown-item",
                            {
                              key: index,
                              on: {
                                click: function($event) {
                                  return _vm.clickToken(index)
                                }
                              }
                            },
                            [
                              _vm._v(
                                "\n            " +
                                  _vm._s(_vm.isViewedInUsd ? "$ " : "") +
                                  " " +
                                  _vm._s(token.value) +
                                  "\n            "
                              ),
                              _c(
                                "div",
                                {
                                  staticStyle: {
                                    "margin-left": "8px",
                                    "padding-bottom": "3px",
                                    display: "inline"
                                  }
                                },
                                [
                                  _c("img", {
                                    staticStyle: {
                                      "padding-bottom": "3px",
                                      height: "14px: weight:14px"
                                    },
                                    attrs: {
                                      src: __webpack_require__(/*! ../../img/algo.svg */ "./resources/img/algo.svg"),
                                      alt: ""
                                    }
                                  })
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "span",
                                { staticStyle: { "margin-left": "6px" } },
                                [_vm._v(_vm._s(token.name))]
                              )
                            ]
                          )
                        }),
                        _vm._v(" "),
                        _c(
                          "b-row",
                          {
                            staticClass: "text-center",
                            staticStyle: {
                              "margin-top": "15px",
                              padding: "0 20px"
                            },
                            attrs: { "no-gutters": "" }
                          },
                          [
                            _c(
                              "b-col",
                              { staticClass: "my-auto", attrs: { cols: "8" } },
                              [_vm._v(" View in USD ")]
                            ),
                            _vm._v(" "),
                            _c(
                              "b-col",
                              { staticStyle: { position: "relative" } },
                              [
                                _c("b-form-checkbox", {
                                  attrs: { name: "check-button", switch: "" },
                                  model: {
                                    value: _vm.isViewedInUsd,
                                    callback: function($$v) {
                                      _vm.isViewedInUsd = $$v
                                    },
                                    expression: "isViewedInUsd"
                                  }
                                })
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ],
                      2
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "input-group-append" }, [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-top-center",
                          staticStyle: { padding: "0 8px", margin: "auto" },
                          attrs: { type: "button" },
                          on: {
                            click: function($event) {
                              return _vm.openDeposit()
                            }
                          }
                        },
                        [
                          _c("img", {
                            attrs: {
                              src: __webpack_require__(/*! ../../img/pocket.svg */ "./resources/img/pocket.svg"),
                              alt: ""
                            }
                          })
                        ]
                      )
                    ])
                  ],
                  1
                )
              ])
            : _vm._e(),
          _vm._v(" "),
          _c(
            "b-navbar-nav",
            { staticClass: "ml-auto d-inline-block align-top" },
            [
              _c(
                "b-navbar-nav",
                { staticClass: "nav-head-right" },
                [
                  _vm.$store.state.isAuth
                    ? [
                        _c(
                          "b-nav-item",
                          { staticClass: "nav-item-1", attrs: { href: "#" } },
                          [
                            _c("img", {
                              attrs: {
                                src: __webpack_require__(/*! ../../img/bell.svg */ "./resources/img/bell.svg"),
                                alt: ""
                              }
                            })
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "b-nav-item",
                          { staticClass: "nav-item-2" },
                          [
                            _c(
                              "b-dropdown",
                              {
                                attrs: {
                                  size: "sm",
                                  "toggle-class": "text-decoration-none",
                                  right: ""
                                },
                                scopedSlots: _vm._u(
                                  [
                                    {
                                      key: "button-content",
                                      fn: function() {
                                        return [
                                          _c(
                                            "svg",
                                            {
                                              attrs: {
                                                width: "19",
                                                height: "19",
                                                viewBox: "0 0 19 19",
                                                fill: "none",
                                                xmlns:
                                                  "http://www.w3.org/2000/svg"
                                              }
                                            },
                                            [
                                              _c("path", {
                                                attrs: {
                                                  d:
                                                    "M17.4652 11.8444C16.8676 11.3205 16.1469 10.867 15.3137 10.4979C14.9586 10.3397 14.5437 10.5014 14.3855 10.8565C14.2273 11.2115 14.3891 11.6264 14.7441 11.7846C15.4438 12.0975 16.0484 12.4701 16.5371 12.9026C17.1418 13.4334 17.4863 14.1998 17.4863 15.0084V16.3092C17.4863 16.6959 17.1699 17.0123 16.7832 17.0123H3.00195C2.61523 17.0123 2.29883 16.6959 2.29883 16.3092V15.0084C2.29883 14.1998 2.64336 13.4299 3.24805 12.9026C3.9582 12.2768 6.02891 10.8248 9.89258 10.8248C12.7613 10.8248 15.0957 8.49045 15.0957 5.6217C15.0957 2.75295 12.7613 0.418579 9.89258 0.418579C7.02383 0.418579 4.68945 2.75295 4.68945 5.6217C4.68945 7.29866 5.4875 8.7928 6.725 9.74553C4.46094 10.2412 3.05469 11.1975 2.31992 11.8444C1.41289 12.6424 0.892578 13.7955 0.892578 15.0084V16.3092C0.892578 17.4729 1.83828 18.4186 3.00195 18.4186H16.7832C17.9469 18.4186 18.8926 17.4729 18.8926 16.3092V15.0084C18.8926 13.7955 18.3723 12.6424 17.4652 11.8444ZM6.0957 5.6217C6.0957 3.52639 7.79727 1.82483 9.89258 1.82483C11.9879 1.82483 13.6895 3.52639 13.6895 5.6217C13.6895 7.71702 11.9879 9.41858 9.89258 9.41858C7.79727 9.41858 6.0957 7.71702 6.0957 5.6217Z"
                                                }
                                              })
                                            ]
                                          )
                                        ]
                                      },
                                      proxy: true
                                    }
                                  ],
                                  null,
                                  false,
                                  1591870199
                                )
                              },
                              [
                                _vm._v(" "),
                                _c(
                                  "b-dropdown-item",
                                  { on: { click: _vm.transaction } },
                                  [_vm._v("Transactions")]
                                ),
                                _vm._v(" "),
                                _c("b-dropdown-item", [_vm._v("VIP")]),
                                _vm._v(" "),
                                _c(
                                  "b-dropdown-item",
                                  { on: { click: _vm.referFriend } },
                                  [_vm._v("Refer-A-Friend")]
                                ),
                                _vm._v(" "),
                                _c(
                                  "b-dropdown-item",
                                  { on: { click: _vm.settings } },
                                  [_vm._v("Settings")]
                                ),
                                _vm._v(" "),
                                _c("b-dropdown-item", [_vm._v("FAQ")]),
                                _vm._v(" "),
                                _c("b-dropdown-item", [_vm._v("Support")]),
                                _vm._v(" "),
                                _c(
                                  "b-dropdown-item",
                                  {
                                    on: {
                                      click: function($event) {
                                        return _vm.logout()
                                      }
                                    }
                                  },
                                  [_vm._v("Logout")]
                                )
                              ],
                              1
                            )
                          ],
                          1
                        )
                      ]
                    : [
                        _c(
                          "b-nav-item",
                          {
                            staticClass: "nav-item-login",
                            attrs: { to: "/login" }
                          },
                          [
                            _c("div", { staticClass: "btn-login my-auto" }, [
                              _vm._v("login")
                            ])
                          ]
                        ),
                        _vm._v(" "),
                        _c(
                          "b-nav-item",
                          {
                            staticClass: "nav-item-regis",
                            attrs: { to: "/register" }
                          },
                          [
                            _c(
                              "button",
                              {
                                staticClass: "btn btn-register",
                                attrs: { type: "button" }
                              },
                              [_vm._v("Register")]
                            )
                          ]
                        )
                      ],
                  _vm._v(" "),
                  _c(
                    "b-nav-item",
                    { staticClass: "nav-item-lang" },
                    [
                      _c(
                        "b-dropdown",
                        { attrs: { size: "sm", text: "EN", right: "" } },
                        [
                          _c("b-dropdown-item-button", [_vm._v("SG")]),
                          _vm._v(" "),
                          _c("b-dropdown-item-button", [_vm._v("IN")]),
                          _vm._v(" "),
                          _c("b-dropdown-item-button", [_vm._v("RS")])
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  !_vm.$store.state.isAuth
                    ? _c("b-nav-item", { staticClass: "nav-item-burger" }, [
                        _c("img", {
                          attrs: {
                            src: __webpack_require__(/*! ../../img/burger-menu.svg */ "./resources/img/burger-menu.svg"),
                            alt: ""
                          }
                        })
                      ])
                    : _vm._e()
                ],
                2
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _vm.$store.state.isAuth
        ? _c("modal-deposit", { ref: "showdep" })
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "user-profile" }, [
    _c(
      "div",
      { staticClass: "form-user-profile" },
      [
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [
                _vm._v(
                  "\n                    Current Password\n                    "
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-input", {
                  staticClass: "setting-input",
                  attrs: { type: "password" },
                  model: {
                    value: _vm.passNow,
                    callback: function($$v) {
                      _vm.passNow = $$v
                    },
                    expression: "passNow"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [
                _vm._v(
                  "\n                    New Password\n                    "
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-input", {
                  staticClass: "setting-input",
                  attrs: { type: "password" },
                  model: {
                    value: _vm.passNew,
                    callback: function($$v) {
                      _vm.passNew = $$v
                    },
                    expression: "passNew"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [
                _vm._v(
                  "\n                    Confirm New Password\n                    "
                )
              ])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-input", {
                  staticClass: "setting-input",
                  attrs: { type: "password" },
                  model: {
                    value: _vm.passConfirm,
                    callback: function($$v) {
                      _vm.passConfirm = $$v
                    },
                    expression: "passConfirm"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }),
            _vm._v(" "),
            _c("b-col", [
              _c(
                "button",
                { staticClass: "btn btn-create", attrs: { type: "submit" } },
                [_vm._v("\n                    SAVE\n                    ")]
              )
            ])
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "invite-raf" }, [
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c("chart-stats", {
          attrs: { labelChart: _vm.chartLabels, dataChart: _vm.chartData }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c(
          "b-row",
          { attrs: { "no-gutters": "" } },
          [
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-left", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              active Friends (This Year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              9,999\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-center", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Turnover (this year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-right", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Referral Commission (this year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-left", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              active Friends (Current Month)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              9,999\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-center", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Turnover (Current Month)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-right", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v("\n              status\n            ")
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              Referrer\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba&":
/*!****************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba& ***!
  \****************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "invite-raf" }, [
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c("chart-stats", {
          attrs: { labelChart: _vm.chartLabels, dataChart: _vm.chartData }
        })
      ],
      1
    ),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "cont-qr" },
      [
        _c(
          "b-row",
          { attrs: { "no-gutters": "" } },
          [
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-left", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              active Friends (This Year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              9,999\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-center", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Turnover (this year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-right", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Referral Commission (this year)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-left", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              active Friends (Current Month)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              9,999\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-center", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v(
                        "\n              Turnover (Current Month)\n            "
                      )
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              USD 1,000,000.00\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-col",
              { attrs: { md: "4", sm: "4", cols: "12" } },
              [
                _c(
                  "b-row",
                  { staticClass: "col-right", attrs: { "no-gutters": "" } },
                  [
                    _c("b-col", { staticClass: "title-sub text-left" }, [
                      _vm._v("\n              status\n            ")
                    ]),
                    _vm._v(" "),
                    _c("b-col", { staticClass: "field-sub text-right" }, [
                      _vm._v("\n              Referrer\n            ")
                    ])
                  ],
                  1
                )
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=template&id=7031d992&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/tableHome.vue?vue&type=template&id=7031d992& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "table-home" },
    [
      _c(
        "b-row",
        { attrs: { "no-gutters": "" } },
        [
          _c("b-col", { staticClass: "text-left" }, [
            _c("div", { staticClass: "text-table" }, [
              _vm._v("\n          Activity\n      ")
            ])
          ]),
          _vm._v(" "),
          _c("b-col", { staticClass: "text-right limit-table" }, [
            _c(
              "select",
              {
                staticClass: "select-table",
                on: {
                  change: function($event) {
                    return _vm.$refs.tabledesk.setLimit($event.target.value)
                  }
                }
              },
              [
                _c("option", { attrs: { value: "10" } }, [_vm._v("10")]),
                _vm._v(" "),
                _c("option", { attrs: { value: "20" } }, [_vm._v("20")]),
                _vm._v(" "),
                _c("option", { attrs: { value: "30" } }, [_vm._v("30")]),
                _vm._v(" "),
                _c("option", { attrs: { value: "50" } }, [_vm._v("50")])
              ]
            )
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c("v-client-table", {
        ref: "tabledesk",
        staticClass: "vue-tables-custom-style vuetable-desk",
        attrs: {
          data: _vm.tableData,
          columns: _vm.columns,
          options: _vm.options
        },
        scopedSlots: _vm._u([
          {
            key: "bet",
            fn: function(props) {
              return [
                _vm._v("\n      " + _vm._s(props.row.bet) + "\n      "),
                _c("img", {
                  staticStyle: {
                    "margin-left": "3px",
                    "padding-bottom": "3px"
                  },
                  attrs: { src: __webpack_require__(/*! ../../img/tokenAlgo.svg */ "./resources/img/tokenAlgo.svg"), alt: "" }
                })
              ]
            }
          },
          {
            key: "wl",
            fn: function(props) {
              return [
                _c("div", { class: { "val-bet": props.row.wl >= 0 } }, [
                  _vm._v("\n      " + _vm._s(props.row.wl) + "\n      "),
                  _c("img", {
                    staticStyle: {
                      "margin-left": "3px",
                      "padding-bottom": "3px"
                    },
                    attrs: { src: __webpack_require__(/*! ../../img/tokenAlgo.svg */ "./resources/img/tokenAlgo.svg"), alt: "" }
                  })
                ])
              ]
            }
          }
        ])
      }),
      _vm._v(" "),
      _c("v-client-table", {
        ref: "table",
        staticClass: "vue-tables-custom-style vuetable-mob",
        attrs: {
          data: _vm.tableData,
          columns: _vm.columnsMob,
          options: _vm.optionsMob
        },
        scopedSlots: _vm._u([
          {
            key: "wl",
            fn: function(props) {
              return [
                _c("div", { class: { "val-bet": props.row.wl >= 0 } }, [
                  _vm._v("\n      " + _vm._s(props.row.wl) + "\n      "),
                  _c("img", {
                    staticStyle: {
                      "margin-left": "3px",
                      "padding-bottom": "3px"
                    },
                    attrs: { src: __webpack_require__(/*! ../../img/tokenAlgo.svg */ "./resources/img/tokenAlgo.svg"), alt: "" }
                  })
                ])
              ]
            }
          }
        ])
      })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8&":
/*!******************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8& ***!
  \******************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    !_vm.activeTele
      ? _c("div", { staticClass: "tfa-setting" }, [
          _c("div", { staticClass: "text-head" }, [
            _vm._v(
              "\n            You can link your Telegram account to your HuatBet account to receive notifications. Please note if you do not link your telegram account to receive notifications, you will not be able to reset your password. "
            ),
            _c("br"),
            _c("br"),
            _vm._v(
              "\n            Linking a Telegram account allows you to receive notifications about deposits to a wallet, trade and margin transactions, and other updates. The notification settings is below."
            ),
            _c("br"),
            _c("br"),
            _vm._v(
              "\n            In order to link your Telegram account, open the link " +
                _vm._s(_vm.linkTele) +
                " or scan it with your smartphone:\n        "
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "cont-qr" },
            [
              _c(
                "b-row",
                { staticClass: "row-qr", attrs: { "no-gutters": "" } },
                [
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("img", {
                        attrs: {
                          src: __webpack_require__(/*! ../../img/join-qa.svg */ "./resources/img/join-qa.svg"),
                          alt: ""
                        }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "qr-img" }, [
                        _c("img", {
                          attrs: {
                            src: __webpack_require__(/*! ../../img/qr-example.svg */ "./resources/img/qr-example.svg"),
                            alt: ""
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("div", {}, [
                        _vm._v(
                          "\n                    Or run the Telegram-bot @HuatBet_bot and specify the identifier:\n                    "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c(
                        "div",
                        {
                          staticClass: "code-col-qr",
                          on: { click: _vm.actifedTele }
                        },
                        [
                          _vm._v(
                            "\n                    " +
                              _vm._s(_vm.codeqr) +
                              "\n                    "
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      : _c(
          "div",
          { staticClass: "tfa-setting" },
          [
            _c("div", { staticClass: "text-head" }, [
              _vm._v(
                "\n            You can link your Telegram account to your HuatBet account to receive notifications. Please note if you do not link your telegram account to receive notifications, you will not be able to reset your password.\n        "
              )
            ]),
            _vm._v(" "),
            _c(
              "b-row",
              { staticClass: "cont-form", attrs: { "no-gutters": "" } },
              [
                _c("b-col", { staticClass: "title-input text-left my-auto" }, [
                  _c("div", [
                    _vm._v(
                      "\n                Preferred Language\n                "
                    )
                  ])
                ]),
                _vm._v(" "),
                _c(
                  "b-col",
                  [
                    _c("b-form-select", {
                      attrs: { options: _vm.optionsLang },
                      model: {
                        value: _vm.selectLang,
                        callback: function($$v) {
                          _vm.selectLang = $$v
                        },
                        expression: "selectLang"
                      }
                    })
                  ],
                  1
                )
              ],
              1
            ),
            _vm._v(" "),
            _c(
              "b-row",
              {
                staticClass: "text-left notify-list",
                attrs: { "no-gutters": "" }
              },
              [
                _c("b-col", { staticClass: "mb-3", attrs: { cols: "12" } }, [
                  _vm._v(
                    "\n              You will receive notifications after linking your Telegram Account:  \n            "
                  )
                ]),
                _vm._v(" "),
                _c("b-col", { staticClass: "mb-1", attrs: { cols: "12" } }, [
                  _vm._v(
                    "\n                Notify of wallet transactions\n            "
                  )
                ]),
                _vm._v(" "),
                _c("b-col", { staticClass: "mb-1", attrs: { cols: "12" } }, [
                  _vm._v(
                    "\n                Notify of affiliate transactions\n            "
                  )
                ]),
                _vm._v(" "),
                _c("b-col", { staticClass: "mb-1", attrs: { cols: "12" } }, [
                  _vm._v(
                    "\n                Notify of marketing & promotions\n            "
                  )
                ]),
                _vm._v(" "),
                _c("b-col", { staticClass: "mb-1", attrs: { cols: "12" } }, [
                  _vm._v(
                    "\n                Notify of password guessing attempts of 2FA/password\n            "
                  )
                ])
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "cont-qr" }, [
              _c(
                "button",
                {
                  staticClass: "btn btn-create btn-cancel",
                  attrs: { type: "submit" },
                  on: { click: _vm.cancelTele }
                },
                [_vm._v("\n            disable link to telegram\n            ")]
              )
            ])
          ],
          1
        )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774&":
/*!***************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774& ***!
  \***************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    !_vm.activeTfa
      ? _c("div", { staticClass: "tfa-setting" }, [
          _c("div", { staticClass: "text-head" }, [
            _vm._v(
              "\n            We provide two-factor authentication (2FA) using FreeOTP Authenticator. One time password (OTP) authentication requires a mobile phone with the FreeOTP Authenticator Application. The application generates a new password every few seconds and this password will be required for authentication. Press the button Use OTP to activate 2FA.\n        "
            )
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "cont-qr" },
            [
              _c(
                "b-row",
                { staticClass: "row-qr", attrs: { "no-gutters": "" } },
                [
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("div", {}, [
                        _vm._v(
                          "\n                    Open up FreeOTP Authenticator app and scan the QR barcode\n                    "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("img", {
                        attrs: {
                          src: __webpack_require__(/*! ../../img/join-qa.svg */ "./resources/img/join-qa.svg"),
                          alt: ""
                        }
                      }),
                      _vm._v(" "),
                      _c("div", { staticClass: "qr-img" }, [
                        _c("img", {
                          attrs: {
                            src: __webpack_require__(/*! ../../img/qr-example.svg */ "./resources/img/qr-example.svg"),
                            alt: ""
                          }
                        })
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("div", {}, [
                        _vm._v(
                          "\n                    For recovery, please save the following alpha-numeric code in a secure location:\n                    "
                        )
                      ])
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    {
                      staticClass: "col-qr text-center",
                      attrs: { cols: "12" }
                    },
                    [
                      _c("div", { staticClass: "code-col-qr" }, [
                        _vm._v(
                          "\n                    " +
                            _vm._s(_vm.codeqr) +
                            "\n                    "
                        )
                      ])
                    ]
                  )
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-row",
                { attrs: { "no-gutters": "" } },
                [
                  _c(
                    "b-col",
                    {
                      staticClass: "text-left",
                      staticStyle: { "margin-bottom": "8px" },
                      attrs: { cols: "12" }
                    },
                    [
                      _vm._v(
                        "\n                    Enter OTP and validate it to enable 2FA\n                "
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { staticClass: "col-qr", attrs: { cols: "12" } },
                    [
                      _c("b-form-input", {
                        staticClass: "setting-input",
                        attrs: { placeholder: "Enter One-Time Password" },
                        model: {
                          value: _vm.otpCode,
                          callback: function($$v) {
                            _vm.otpCode = $$v
                          },
                          expression: "otpCode"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { staticClass: "col-qr", attrs: { cols: "12" } },
                    [
                      _c(
                        "b-form-checkbox",
                        {
                          staticClass: "checkbox-name",
                          attrs: {
                            id: "checkbox-name",
                            name: "checkbox-1",
                            value: "accepted",
                            "unchecked-value": "not_accepted"
                          },
                          model: {
                            value: _vm.confirmCode,
                            callback: function($$v) {
                              _vm.confirmCode = $$v
                            },
                            expression: "confirmCode"
                          }
                        },
                        [
                          _vm._v(
                            "\n                        I confirm that i have saved the alpha numeric code.\n                    "
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { staticClass: "col-qr", attrs: { cols: "12" } },
                    [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-create",
                          attrs: { type: "submit" },
                          on: { click: _vm.actifedTfa }
                        },
                        [
                          _vm._v(
                            "\n                    validate and enable 2fa\n                    "
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "b-col",
                    { staticClass: "col-qr", attrs: { cols: "12" } },
                    [
                      _c(
                        "button",
                        {
                          staticClass: "btn btn-create btn-cancel",
                          attrs: { type: "submit" }
                        },
                        [
                          _vm._v(
                            "\n                    cancel 2fa activation\n                    "
                          )
                        ]
                      )
                    ]
                  )
                ],
                1
              )
            ],
            1
          )
        ])
      : _c("div", { staticClass: "tfa-setting" }, [
          _c("div", { staticClass: "text-head" }, [
            _vm._v(
              "\n            We provide two-factor authentication (2FA) using FreeOTP Authenticator. One time password (OTP) authentication requires a mobile phone with the FreeOTP Authenticator Application. The application generates a new password every few seconds and this password will be required for authentication. Press the button Use OTP to activate 2FA.\n        "
            )
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "cont-qr" }, [
            _c(
              "button",
              {
                staticClass: "btn btn-create btn-cancel",
                attrs: { type: "submit" },
                on: { click: _vm.cancelTfa }
              },
              [_vm._v("\n            disable 2fa authentication\n            ")]
            )
          ])
        ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a&":
/*!**************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a& ***!
  \**************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "user-profile" }, [
    _c(
      "div",
      { staticClass: "form-user-profile" },
      [
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [_vm._v("Username (UN)")])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-input", {
                  staticClass: "setting-input",
                  attrs: { readonly: "" },
                  model: {
                    value: _vm.userName,
                    callback: function($$v) {
                      _vm.userName = $$v
                    },
                    expression: "userName"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [_vm._v("Display Name")])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-input", {
                  staticClass: "setting-input",
                  model: {
                    value: _vm.displayName,
                    callback: function($$v) {
                      _vm.displayName = $$v
                    },
                    expression: "displayName"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [_vm._v("Privacy")])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c(
                  "b-form-checkbox",
                  {
                    staticClass: "checkbox-name",
                    attrs: {
                      id: "checkbox-name",
                      name: "checkbox-1",
                      value: "1",
                      "unchecked-value": "0"
                    },
                    model: {
                      value: _vm.status,
                      callback: function($$v) {
                        _vm.status = $$v
                      },
                      expression: "status"
                    }
                  },
                  [_vm._v("\n          I accept the terms and use\n        ")]
                )
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }, [
              _c("div", [_vm._v("Time Zone")])
            ]),
            _vm._v(" "),
            _c(
              "b-col",
              [
                _c("b-form-select", {
                  attrs: { options: _vm.optionsTime },
                  model: {
                    value: _vm.selectTime,
                    callback: function($$v) {
                      _vm.selectTime = $$v
                    },
                    expression: "selectTime"
                  }
                })
              ],
              1
            )
          ],
          1
        ),
        _vm._v(" "),
        _vm.$store.state.profile.error.isError
          ? _c("div", [
              _vm._v(
                "\n      " +
                  _vm._s(_vm.$store.state.profile.error.errorMsg) +
                  "\n    "
              )
            ])
          : _vm._e(),
        _vm._v(" "),
        _c(
          "b-row",
          { staticClass: "cont-form", attrs: { "no-gutters": "" } },
          [
            _c("b-col", { staticClass: "title-input text-left my-auto" }),
            _vm._v(" "),
            _c("b-col", [
              _c(
                "button",
                {
                  staticClass: "btn btn-create",
                  attrs: {
                    type: "submit",
                    disabled: _vm.$store.state.profile.isLoading
                  },
                  on: { click: _vm.update }
                },
                [_vm._v("\n          SAVE\n        ")]
              )
            ])
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "dashboard" },
    [
      _c(
        "div",
        { staticClass: "games-cont" },
        [
          _c(
            "b-row",
            { attrs: { "no-gutters": "" } },
            [
              _c(
                "b-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("card-carousel", {
                    staticClass: "card-carousels",
                    attrs: {
                      titles: "Dice",
                      imageUrl: "3d-dice.svg",
                      bgColor: "#706749",
                      rateWin: 30
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("card-carousel", {
                    staticClass: "card-carousels",
                    attrs: {
                      titles: "Si Ki Pi",
                      imageUrl: "3d-sikipi.svg",
                      bgColor: "#4D1B5A",
                      rateWin: 50
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("card-carousel", {
                    staticClass: "card-carousels",
                    attrs: {
                      titles: "Mines",
                      imageUrl: "3d-mines.svg",
                      bgColor: "#3A0E16",
                      rateWin: 60
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("card-carousel", {
                    staticClass: "card-carousels",
                    attrs: {
                      titles: "Moonshot",
                      imageUrl: "3d-moonshot.svg",
                      bgColor: "#585C65",
                      rateWin: 80
                    }
                  })
                ],
                1
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { attrs: { cols: "6", md: "4" } },
                [
                  _c("card-carousel", {
                    staticClass: "card-carousels",
                    attrs: {
                      titles: "Gor Ki Pi",
                      imageUrl: "3D-GORKIPI.svg",
                      bgColor: "#084C5A",
                      rateWin: 70
                    }
                  })
                ],
                1
              )
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("table-home", { staticClass: "table-one" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6&":
/*!*************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6& ***!
  \*************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "faq" },
    [
      _c("b-jumbotron", { staticClass: "mx-auto" }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content-htp" },
        [
          _c(
            "b-row",
            {
              staticStyle: { "margin-top": "20px" },
              attrs: { "no-gutters": "" }
            },
            [
              _c(
                "b-col",
                {
                  staticClass: "text-left",
                  staticStyle: { margin: "auto !important" },
                  attrs: { cols: "3" }
                },
                [
                  _c("div", { staticClass: "title-head" }, [
                    _vm._v(
                      "\n                  Frequently Asked Questions\n              "
                    )
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { staticClass: "text-left" },
                [
                  _c(
                    "b-col",
                    { staticClass: "text-left" },
                    [
                      _c("b-form-select", {
                        attrs: { options: _vm.options, calss: "mb-3" },
                        model: {
                          value: _vm.selected,
                          callback: function($$v) {
                            _vm.selected = $$v
                          },
                          expression: "selected"
                        }
                      })
                    ],
                    1
                  )
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm._m(0)
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "content-main" }, [
      _c("div", { staticClass: "body-main" }, [
        _c("span", [_vm._v(" What is the fee structure?")]),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              The fee structure is either a 5% commission or the net win/loss per game. Depending on the game, you can share in that commission or pot by being an early participant or by playing the side of the ‘house’ and taking on liquidity risk."
        ),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _c("span", [_vm._v("Who are we?")]),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              We are a team of 3 developers and game theorists who love Algorand and wanted to do something exciting, and fun instead of living under the oppression of Ethereum smurf land and numb the pain of failed hopes and dreams of Algo financial freedom."
        ),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _c("span", [_vm._v("New Development")]),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              If things go well, you’ll find that we are developing old and new crytpo provenly fair games. We hope to make a new game every month and keep adding on to it."
        ),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _c("span", [_vm._v("Algofair.io Research Grant and Jackpot Fund")]),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              We love playing around tokenomics and game theory. For any revenue gained, 75% will go to the team, but 25% will go to an AlgoFair Research Grant and Jackpot Fund."
        ),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              So if you have a great idea about a game that can be made on Algofair; and all you need to do is share it with us and we will pay you for the idea or give you a grant to build it yourself. We’ll put it on our platform and you’ll get 20% revenue share from it as long as it people play it."
        ),
        _c("br"),
        _c("br"),
        _vm._v(" "),
        _c("span", [_vm._v("Algofair Governance")]),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              If we can get big enough, we will give out an algofair governance token, which you can only gain from playing the games on Algofair. But when we make it, we’ll do it ala uniswap style and any wallet that has interacted with our games we’ll do an airdrop."
        ),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              So, we’ll make a community voting portal, if you want to make jackpot proposal, then we can use funds from the Jackpot Fund to fund the proposal i.e. to make a direct contribution from the Jackpot pool. So any game on the site that has a jackpot, you can make a proposal to add, 1%-100% of the Jackpot to the existing winnings."
        ),
        _c("br"),
        _c("br"),
        _vm._v(
          "\n              Ok big dreams, but we’re inspired by how uniswap started, and by Algorand’s continued tech and the need for us to do something useful with our Algos!"
        ),
        _c("br"),
        _c("br"),
        _vm._v("\n              Thanks for coming out!"),
        _c("br"),
        _c("br")
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7&":
/*!************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7& ***!
  \************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "forgot-password" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("div", { staticClass: "title-hed" }, [
                _vm._v("Forgot Your Password?")
              ])
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c(
              "b-col",
              { attrs: { cols: "12" } },
              [
                _vm.show
                  ? _c(
                      "b-form",
                      { on: { submit: _vm.onSubmit } },
                      [
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-1" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "input-1",
                                placeholder: "Username",
                                required: ""
                              },
                              model: {
                                value: _vm.form.name,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "name", $$v)
                                },
                                expression: "form.name"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c("div", { staticClass: "forgot-text text-centre" }, [
                          _vm._v(
                            "\n            Enter your username to receive a notification to reset your\n            password. Please note if you have not linked your telegram account\n            to receive notifications, you will not be able to reset your\n            password.\n          "
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-create",
                            attrs: { type: "submit", disabled: _vm.isLoading }
                          },
                          [_vm._v("\n            reset password\n          ")]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30&":
/*!**************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30& ***!
  \**************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "home" },
    [
      _c(
        "b-jumbotron",
        {
          staticClass: "mx-auto",
          scopedSlots: _vm._u([
            {
              key: "header",
              fn: function() {
                return [_vm._v("Welcome to")]
              },
              proxy: true
            }
          ])
        },
        [
          _vm._v(" "),
          _c("div", { staticClass: "image-jumbo" }, [
            _c("img", {
              attrs: { src: __webpack_require__(/*! ../../img/Huatbet-02.svg */ "./resources/img/Huatbet-02.svg"), alt: "" }
            })
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "cont-carousel" },
        [
          _c(
            "b-carousel",
            {
              staticStyle: { "text-shadow": "1px 1px 2px #333" },
              attrs: {
                id: "carousel-desk",
                interval: 0,
                controls: "",
                indicators: "",
                "img-height": "420",
                "img-width": "380",
                background: "transparent"
              },
              on: {
                "sliding-start": _vm.onSlideStart,
                "sliding-end": _vm.onSlideEnd
              },
              model: {
                value: _vm.slide,
                callback: function($$v) {
                  _vm.slide = $$v
                },
                expression: "slide"
              }
            },
            [
              _c("b-carousel-slide", {
                attrs: { "img-blank": "" },
                scopedSlots: _vm._u([
                  {
                    key: "img",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticStyle: { display: "flex" } },
                          [
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Dice",
                                imageUrl: "3d-dice.svg",
                                bgColor: "#706749",
                                rateWin: 30
                              }
                            }),
                            _vm._v(" "),
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Si Ki Pi",
                                imageUrl: "3d-sikipi.svg",
                                bgColor: "#4D1B5A",
                                rateWin: 50
                              }
                            }),
                            _vm._v(" "),
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Mines",
                                imageUrl: "3d-mines.svg",
                                bgColor: "#3A0E16",
                                rateWin: 60
                              }
                            })
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  }
                ])
              }),
              _vm._v(" "),
              _c("b-carousel-slide", {
                attrs: { "img-blank": "" },
                scopedSlots: _vm._u([
                  {
                    key: "img",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticStyle: { display: "flex" } },
                          [
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Moonshot",
                                imageUrl: "3d-moonshot.svg",
                                bgColor: "#585C65",
                                rateWin: 80
                              }
                            }),
                            _vm._v(" "),
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Gor Ki Pi",
                                imageUrl: "3D-GORKIPI.svg",
                                bgColor: "#084C5A",
                                rateWin: 70
                              }
                            })
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  }
                ])
              })
            ],
            1
          ),
          _vm._v(" "),
          _c(
            "b-carousel",
            {
              staticStyle: { "text-shadow": "1px 1px 2px #333" },
              attrs: {
                id: "carousel-mob",
                indicators: "",
                "img-height": "283",
                "img-width": "170",
                background: "transparent"
              }
            },
            [
              _c("b-carousel-slide", {
                attrs: { "img-blank": "" },
                scopedSlots: _vm._u([
                  {
                    key: "img",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticStyle: { display: "flex" } },
                          [
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Dice",
                                imageUrl: "3d-dice.svg",
                                bgColor: "#706749",
                                rateWin: 30
                              }
                            }),
                            _vm._v(" "),
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Si Ki Pi",
                                imageUrl: "3d-sikipi.svg",
                                bgColor: "#4D1B5A",
                                rateWin: 50
                              }
                            })
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  }
                ])
              }),
              _vm._v(" "),
              _c("b-carousel-slide", {
                attrs: { "img-blank": "" },
                scopedSlots: _vm._u([
                  {
                    key: "img",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticStyle: { display: "flex" } },
                          [
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Mines",
                                imageUrl: "3d-mines.svg",
                                bgColor: "#3A0E16",
                                rateWin: 60
                              }
                            }),
                            _vm._v(" "),
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Moonshot",
                                imageUrl: "3d-moonshot.svg",
                                bgColor: "#585C65",
                                rateWin: 80
                              }
                            })
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  }
                ])
              }),
              _vm._v(" "),
              _c("b-carousel-slide", {
                attrs: { "img-blank": "" },
                scopedSlots: _vm._u([
                  {
                    key: "img",
                    fn: function() {
                      return [
                        _c(
                          "div",
                          { staticStyle: { display: "flex" } },
                          [
                            _c("card-carousel", {
                              staticClass: "card-carousels",
                              attrs: {
                                titles: "Gor Ki Pi",
                                imageUrl: "3D-GORKIPI.svg",
                                bgColor: "#084C5A",
                                rateWin: 70
                              }
                            })
                          ],
                          1
                        )
                      ]
                    },
                    proxy: true
                  }
                ])
              })
            ],
            1
          )
        ],
        1
      ),
      _vm._v(" "),
      _c("table-home", { staticClass: "table-one" })
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06&":
/*!*******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06& ***!
  \*******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "howtoplay" },
    [
      _c("b-jumbotron", { staticClass: "mx-auto" }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content-htp" },
        [
          _c(
            "b-row",
            {
              staticStyle: { "margin-top": "20px" },
              attrs: { "no-gutters": "" }
            },
            [
              _c(
                "b-col",
                {
                  staticClass: "text-left",
                  staticStyle: { margin: "auto !important" },
                  attrs: { cols: "2" }
                },
                [
                  _c("div", { staticClass: "title-head" }, [
                    _vm._v("\n                  How To Play\n              ")
                  ])
                ]
              ),
              _vm._v(" "),
              _c(
                "b-col",
                { staticClass: "text-left" },
                [
                  _c("b-form-select", {
                    attrs: { options: _vm.options, calss: "mb-3" },
                    model: {
                      value: _vm.selected,
                      callback: function($$v) {
                        _vm.selected = $$v
                      },
                      expression: "selected"
                    }
                  })
                ],
                1
              )
            ],
            1
          ),
          _vm._v(" "),
          _vm._m(0)
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "content-main" }, [
      _c("span", [_vm._v(" Title Here ")]),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n              "
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n      "
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30&":
/*!***************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30& ***!
  \***************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "login" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("img", {
                attrs: { src: __webpack_require__(/*! ../../img/log-in.svg */ "./resources/img/log-in.svg"), alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("div", { staticClass: "desc-img" }, [_vm._v("Welcome Back")])
            ]),
            _vm._v(" "),
            _vm.isError
              ? _c("b-col", { attrs: { cols: "12" } }, [
                  _c("div", { staticClass: "text-error" }, [
                    _vm._v(
                      "\n          " + _vm._s(_vm.errorMessage) + "\n        "
                    )
                  ])
                ])
              : _vm._e()
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c(
              "b-col",
              { attrs: { cols: "12" } },
              [
                _vm.show
                  ? _c(
                      "b-form",
                      { on: { submit: _vm.onSubmit } },
                      [
                        _c("b-form-group", { attrs: { id: "input-group-1" } }, [
                          _c(
                            "div",
                            { staticClass: "input-icon" },
                            [
                              _c("b-form-input", {
                                attrs: {
                                  id: "input-1",
                                  placeholder: "Username",
                                  required: ""
                                },
                                model: {
                                  value: _vm.form.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "name", $$v)
                                  },
                                  expression: "form.name"
                                }
                              }),
                              _vm._v(" "),
                              _c("img", {
                                staticClass: "icon-info",
                                attrs: {
                                  id: "tooltip-target-1",
                                  src: __webpack_require__(/*! ../../img/info.svg */ "./resources/img/info.svg"),
                                  alt: ""
                                }
                              }),
                              _vm._v(" "),
                              _c("auth-tooltip")
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "b-form-group",
                          {
                            staticStyle: { "margin-bottom": "12px" },
                            attrs: { id: "input-group-2" }
                          },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-2",
                                "aria-describedby": "password-help-block",
                                placeholder: "Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.pass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "pass", $$v)
                                },
                                expression: "form.pass"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "forgot-pw text-right" },
                          [
                            _c(
                              "router-link",
                              { attrs: { to: "/forgotpassword" } },
                              [_vm._v(" Forgot Password ")]
                            )
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-create",
                            attrs: { disabled: _vm.loading, type: "submit" }
                          },
                          [_vm._v("\n            Login\n          ")]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            ),
            _vm._v(" "),
            _c("b-col", { attrs: { cols: "12" } }, [
              _c(
                "div",
                { staticClass: "have-login" },
                [
                  _vm._v(
                    "\n          Don’t have an account? Sign up\n          "
                  ),
                  _c("router-link", { attrs: { to: "/register" } }, [
                    _vm._v("Here")
                  ]),
                  _vm._v("!\n        ")
                ],
                1
              )
            ])
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "login-2fa" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c("b-col", { staticClass: "text-centre", attrs: { cols: "12" } }, [
              _c("div", { staticClass: "image-top" }, [
                _c("img", {
                  attrs: { src: __webpack_require__(/*! ../../img/2fa.svg */ "./resources/img/2fa.svg"), alt: "" }
                })
              ])
            ]),
            _vm._v(" "),
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("div", { staticClass: "desc-img" }, [
                _vm._v("One-Time Password")
              ])
            ]),
            _vm._v(" "),
            _vm.isError
              ? _c("b-col", { attrs: { cols: "12" } }, [
                  _c("div", { staticClass: "text-error" }, [
                    _vm._v(
                      "\n          " + _vm._s(_vm.errorMessage) + "\n        "
                    )
                  ])
                ])
              : _vm._e()
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c(
              "b-col",
              { attrs: { cols: "12" } },
              [
                _vm.show
                  ? _c(
                      "b-form",
                      { on: { submit: _vm.onSubmit } },
                      [
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-1" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-1",
                                "aria-describedby": "password-help-block",
                                placeholder: "Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.pass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "pass", $$v)
                                },
                                expression: "form.pass"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-create",
                            attrs: { type: "submit" }
                          },
                          [_vm._v("validate")]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "provably-fair" },
    [
      _c("b-jumbotron", { staticClass: "mx-auto" }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content-htp" },
        [
          _c(
            "b-row",
            {
              staticStyle: { "margin-top": "20px" },
              attrs: { "no-gutters": "" }
            },
            [
              _c("b-col", { staticClass: "text-left", attrs: { cols: "3" } }, [
                _c("div", { staticClass: "title-head" }, [
                  _vm._v("\n                  Provably Fair\n              ")
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _c("div", { staticClass: "content-main" }, [
            _c("div", { staticClass: "box-verify text-center" }, [
              _c(
                "div",
                { staticClass: "text-center wrap-verify" },
                [
                  _c("img", {
                    staticStyle: { height: "360px", width: "360px" },
                    attrs: { src: __webpack_require__(/*! ../../img/pf-crash.svg */ "./resources/img/pf-crash.svg"), alt: "" }
                  }),
                  _vm._v(" "),
                  _c("div", { staticClass: "text-head" }, [
                    _vm._v("\n                      Verify\n                  ")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "cont-prof" },
                    [
                      _c(
                        "b-row",
                        {
                          staticStyle: { "margin-bottom": "4px" },
                          attrs: { "no-gutters": "" }
                        },
                        [
                          _c("b-col", { staticClass: "text-left text-prof" }, [
                            _vm._v(
                              "\n                            Game\n                        "
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c("b-form-select", {
                        attrs: { options: _vm.options, calss: "mb-3" },
                        model: {
                          value: _vm.selected,
                          callback: function($$v) {
                            _vm.selected = $$v
                          },
                          expression: "selected"
                        }
                      })
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "cont-prof" },
                    [
                      _c(
                        "b-row",
                        {
                          staticStyle: { "margin-bottom": "4px" },
                          attrs: { "no-gutters": "" }
                        },
                        [
                          _c("b-col", { staticClass: "text-left text-prof" }, [
                            _vm._v(
                              "\n                            Server Seed\n                        "
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-input-group",
                        { staticStyle: { height: "36px !important" } },
                        [
                          _c("b-form-input", {
                            staticClass: "prof-input",
                            attrs: { readonly: "" },
                            model: {
                              value: _vm.serverSeed,
                              callback: function($$v) {
                                _vm.serverSeed = $$v
                              },
                              expression: "serverSeed"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "b-input-group-append",
                            [
                              _c("b-button", { staticClass: "prof-button" }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                    alt: ""
                                  }
                                })
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "cont-prof" },
                    [
                      _c(
                        "b-row",
                        {
                          staticStyle: { "margin-bottom": "4px" },
                          attrs: { "no-gutters": "" }
                        },
                        [
                          _c("b-col", { staticClass: "text-left text-prof" }, [
                            _vm._v(
                              "\n                            Server Seed (Hashed)\n                        "
                            )
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "b-input-group",
                        { staticStyle: { height: "36px !important" } },
                        [
                          _c("b-form-input", {
                            staticClass: "prof-input",
                            attrs: { readonly: "" },
                            model: {
                              value: _vm.serverHash,
                              callback: function($$v) {
                                _vm.serverHash = $$v
                              },
                              expression: "serverHash"
                            }
                          }),
                          _vm._v(" "),
                          _c(
                            "b-input-group-append",
                            [
                              _c("b-button", { staticClass: "prof-button" }, [
                                _c("img", {
                                  attrs: {
                                    src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                    alt: ""
                                  }
                                })
                              ])
                            ],
                            1
                          )
                        ],
                        1
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _c(
                    "b-row",
                    { attrs: { "no-gutters": "" } },
                    [
                      _c(
                        "b-col",
                        {
                          staticStyle: { "padding-right": "5px" },
                          attrs: { cols: "8" }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "cont-prof" },
                            [
                              _c(
                                "b-row",
                                {
                                  staticStyle: { "margin-bottom": "4px" },
                                  attrs: { "no-gutters": "" }
                                },
                                [
                                  _c(
                                    "b-col",
                                    { staticClass: "text-left text-prof" },
                                    [
                                      _vm._v(
                                        "\n                              Client Seed\n                          "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "b-input-group",
                                { staticStyle: { height: "36px !important" } },
                                [
                                  _c("b-form-input", {
                                    staticClass: "prof-input",
                                    attrs: { readonly: "" },
                                    model: {
                                      value: _vm.clientSeed,
                                      callback: function($$v) {
                                        _vm.clientSeed = $$v
                                      },
                                      expression: "clientSeed"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "b-input-group-append",
                                    [
                                      _c(
                                        "b-button",
                                        { staticClass: "prof-button" },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                              alt: ""
                                            }
                                          })
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      ),
                      _vm._v(" "),
                      _c(
                        "b-col",
                        {
                          staticStyle: { "padding-left": "5px" },
                          attrs: { cols: "4" }
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "cont-prof" },
                            [
                              _c(
                                "b-row",
                                {
                                  staticStyle: { "margin-bottom": "4px" },
                                  attrs: { "no-gutters": "" }
                                },
                                [
                                  _c(
                                    "b-col",
                                    { staticClass: "text-left text-prof" },
                                    [
                                      _vm._v(
                                        "\n                              Nonce\n                          "
                                      )
                                    ]
                                  )
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c(
                                "b-input-group",
                                { staticStyle: { height: "36px !important" } },
                                [
                                  _c("b-form-input", {
                                    staticClass: "prof-input",
                                    attrs: { readonly: "" },
                                    model: {
                                      value: _vm.nonce,
                                      callback: function($$v) {
                                        _vm.nonce = $$v
                                      },
                                      expression: "nonce"
                                    }
                                  }),
                                  _vm._v(" "),
                                  _c(
                                    "b-input-group-append",
                                    [
                                      _c(
                                        "b-button",
                                        { staticClass: "prof-button" },
                                        [
                                          _c("img", {
                                            attrs: {
                                              src: __webpack_require__(/*! ../../img/copy.svg */ "./resources/img/copy.svg"),
                                              alt: ""
                                            }
                                          })
                                        ]
                                      )
                                    ],
                                    1
                                  )
                                ],
                                1
                              )
                            ],
                            1
                          )
                        ]
                      )
                    ],
                    1
                  )
                ],
                1
              )
            ]),
            _vm._v(" "),
            _vm._m(0)
          ])
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "body-main" }, [
      _c("span", [_vm._v("Overview")]),
      _c("br"),
      _c("br"),
      _vm._v("\n              Solving the Trust Issue with Online Gaming"),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The underlying concept of provable fairness is that players have the ability to prove and verify that their results are fair and unmanipulated. This is achieved through the use of a commitment scheme, along with cryptographic hashing."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The commitment scheme is used to ensure that the player has an influence on all results generated. Cryptographic hashing is used to ensure that the operator also remains honest to this commitment scheme. Both concepts combined creates a trust-less environment when gaming online."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              This is simplified in the following representation:"
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              fair result = operators input (hashed) + players input "
      ),
      _c("br"),
      _c("br"),
      _c("br"),
      _vm._v(" "),
      _c("span", [_vm._v("Implementation")]),
      _c("br"),
      _c("br"),
      _vm._v("\n              Random Number Generation"),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              For each verifiable bet, a client seed, a server seed, a nonce and a cursor are used as the input parameters for the random number generation function. This function utilises the cryptographic hash function HMAC_SHA256 to generate bytes which are then used as the foundation for how we generate provably fair random outcomes on our platform."
      ),
      _c("br"),
      _c("br"),
      _c("br"),
      _vm._v("\n              Server Seed"),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The server seed is generated by our system as a random 64-character hex string. You are then provided with an encrypted hash of that generated server seed before you place any bets. The reason we provide you with the encrypted form of the server seed is to ensure that the un-hashed server seed cannot be changed by the operator, and that the player cannot calculate the results beforehand."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              To reveal the server seed from its hashed version, the seed must be rotated by the player, which triggers the replacement with a newly generated one."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              From this point you are able to verify that the hashed server seed matches that of the un-hashed server seed. This process can be verified via our un-hashed server seed function found in the menu above."
      ),
      _c("br"),
      _c("br"),
      _c("br"),
      _vm._v("\n              Client Seed"),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The client seed belongs to the player and is used to ensure the player also has influence on the randomness of the outcomes generated. Without this component of the algorithm, the server seed alone would have complete leverage over the outcome of each bet."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              All players are free to edit and change their client seed regularly to create a new chain of random upcoming outcomes. This ensures the player has absolute control over the generation of the result, similar to cutting a deck of cards."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              During registration, a client seed is created for you by your browser, to ensure your initial experience with the site goes uninterrupted. Whilst this randomly generated client seed is considered suitable, we highly recommend that you choose your own, so that your influence is included in the randomness."
      ),
      _c("br"),
      _c("br"),
      _c("br"),
      _vm._v("\n              Nonce"),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The nonce is simply a number that increments as every new bet is made. Due to the nature of the SHA256 cryptographic function, this creates a completely new result each time, without having to generate a new client seed and server seed."
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              The implementation of nonce, ensures we remain committed to your client seed and server seed pair, whilst generating new results for each bet placed."
      ),
      _c("br"),
      _c("br")
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "setting" },
    [
      _c(
        "b-container",
        { staticClass: "container-main", attrs: { fluid: "" } },
        [
          _c(
            "b-row",
            { attrs: { "no-gutters": "" } },
            [
              _c("b-col", { attrs: { cols: "2" } }, [
                _c("div", { staticClass: "menu-side" }, [
                  _c("div", { staticClass: "header-menu text-left" }, [
                    _vm._v("Refer-a-friend")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "menu-item" },
                    [
                      _c(
                        "b-nav",
                        { attrs: { vertical: "" } },
                        [
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 1 },
                              on: {
                                click: function($event) {
                                  return _vm.inviteRefer()
                                }
                              }
                            },
                            [_vm._v("Invite")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 2 },
                              on: {
                                click: function($event) {
                                  return _vm.statsRefer()
                                }
                              }
                            },
                            [_vm._v("stats")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("b-col", [
                _c(
                  "div",
                  { staticClass: "content-side" },
                  [
                    _c(
                      "b-row",
                      {
                        staticClass: "header-menu",
                        attrs: { "no-gutters": "" }
                      },
                      [
                        _c(
                          "b-col",
                          {
                            staticClass: "text-left my-auto",
                            attrs: { cols: "6" }
                          },
                          [
                            _vm._v(
                              "\n                  " +
                                _vm._s(_vm.titleCard) +
                                "\n              "
                            )
                          ]
                        ),
                        _vm._v(" "),
                        _vm.selected == 2
                          ? _c(
                              "b-col",
                              {
                                staticClass: "text-right select-time",
                                attrs: { cols: "6" }
                              },
                              [
                                _c("b-form-select", {
                                  attrs: { options: _vm.optionsTime },
                                  model: {
                                    value: _vm.selectTime,
                                    callback: function($$v) {
                                      _vm.selectTime = $$v
                                    },
                                    expression: "selectTime"
                                  }
                                })
                              ],
                              1
                            )
                          : _vm._e()
                      ],
                      1
                    ),
                    _vm._v(" "),
                    _c("div", { staticClass: "main-content" }, [
                      _vm.selected == 1
                        ? _c(
                            "div",
                            { staticClass: "cont-table" },
                            [_c("invite-raf")],
                            1
                          )
                        : _vm.selected == 2
                        ? _c(
                            "div",
                            { staticClass: "cont-table" },
                            [
                              _vm.referrer
                                ? _c("stats-referrer")
                                : _c("stats-affiliates")
                            ],
                            1
                          )
                        : _vm._e()
                    ])
                  ],
                  1
                ),
                _vm._v(" "),
                _vm.selected == 2
                  ? _c(
                      "div",
                      { staticClass: "content-side" },
                      [
                        _c(
                          "b-row",
                          {
                            staticClass: "header-menu",
                            attrs: { "no-gutters": "" }
                          },
                          [
                            _c(
                              "b-col",
                              {
                                staticClass: "text-left my-auto",
                                attrs: { cols: "6" }
                              },
                              [
                                _vm._v(
                                  "\n                  Network\n              "
                                )
                              ]
                            ),
                            _vm._v(" "),
                            !_vm.referrer
                              ? _c(
                                  "b-col",
                                  {
                                    staticClass: "text-right select-time",
                                    attrs: { cols: "6" }
                                  },
                                  [
                                    _c("b-form-select", {
                                      attrs: { options: _vm.optionsTime2 },
                                      model: {
                                        value: _vm.selectTime2,
                                        callback: function($$v) {
                                          _vm.selectTime2 = $$v
                                        },
                                        expression: "selectTime2"
                                      }
                                    })
                                  ],
                                  1
                                )
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _vm.referrer
                          ? _c("div", { staticClass: "main-content" }, [
                              _c(
                                "div",
                                { staticClass: "network-cont" },
                                [
                                  _c(
                                    "b-row",
                                    {
                                      staticClass: "title-sub",
                                      attrs: { "no-gutters": "" }
                                    },
                                    [
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-left",
                                          attrs: { cols: "6" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          name\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-center",
                                          attrs: { cols: "3" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          date joined\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-right",
                                          attrs: { cols: "3" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          Active this month\n                      "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.netJoin, function(join, index) {
                                    return _c(
                                      "b-row",
                                      {
                                        key: index,
                                        staticClass: "field-sub",
                                        attrs: { "no-gutters": "" }
                                      },
                                      [
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-left",
                                            attrs: { cols: "6" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(join.name) +
                                                "\n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-center",
                                            attrs: { cols: "3" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(join.date) +
                                                "\n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-right",
                                            attrs: { cols: "3" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(join.active) +
                                                "\n                      "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  })
                                ],
                                2
                              )
                            ])
                          : _c("div", { staticClass: "main-content" }, [
                              _c(
                                "div",
                                { staticClass: "network-cont text-left" },
                                [
                                  _vm._v(
                                    "\n                  See who is in your affiliate network in the chart below. When your affiliates invite other friends, they become part of your network too! In total you get up to 3 tiers of revenue sharing!\n              "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "network-cont" },
                                [
                                  _c(
                                    "b-row",
                                    {
                                      staticClass: "title-sub",
                                      attrs: { "no-gutters": "" }
                                    },
                                    [
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-left",
                                          attrs: { cols: "4" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          name\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-center",
                                          attrs: { cols: "2" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          Turnover\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-center",
                                          attrs: { cols: "2" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          NET GAMING REVENUE\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-center",
                                          attrs: { cols: "2" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          COMMISSION\n                      "
                                          )
                                        ]
                                      ),
                                      _vm._v(" "),
                                      _c(
                                        "b-col",
                                        {
                                          staticClass: "text-right",
                                          attrs: { cols: "2" }
                                        },
                                        [
                                          _vm._v(
                                            "\n                          Date joined\n                      "
                                          )
                                        ]
                                      )
                                    ],
                                    1
                                  ),
                                  _vm._v(" "),
                                  _vm._l(_vm.netAffiliates, function(
                                    aff,
                                    index
                                  ) {
                                    return _c(
                                      "b-row",
                                      {
                                        key: index,
                                        staticClass: "field-sub",
                                        attrs: { "no-gutters": "" }
                                      },
                                      [
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-left",
                                            attrs: { cols: "4" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(aff.name) +
                                                "                            \n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-center",
                                            attrs: { cols: "2" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          USD " +
                                                _vm._s(aff.turn) +
                                                "\n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-center",
                                            attrs: { cols: "2" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          USD " +
                                                _vm._s(aff.net) +
                                                "\n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-center",
                                            attrs: { cols: "2" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          USD " +
                                                _vm._s(aff.commision) +
                                                "\n                      "
                                            )
                                          ]
                                        ),
                                        _vm._v(" "),
                                        _c(
                                          "b-col",
                                          {
                                            staticClass: "text-right",
                                            attrs: { cols: "2" }
                                          },
                                          [
                                            _vm._v(
                                              "\n                          " +
                                                _vm._s(aff.date) +
                                                "\n                      "
                                            )
                                          ]
                                        )
                                      ],
                                      1
                                    )
                                  })
                                ],
                                2
                              )
                            ]),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "network-cont" },
                          [
                            _c("Tree", {
                              ref: "my-tree",
                              attrs: {
                                id: "my-tree-id",
                                "custom-options": _vm.myCustomOptions,
                                "custom-styles": _vm.myCustomStyles,
                                nodes: _vm.treeDisplayData
                              }
                            })
                          ],
                          1
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=template&id=364a2fac&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Register.vue?vue&type=template&id=364a2fac& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "register" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("img", {
                attrs: { src: __webpack_require__(/*! ../../img/sign-up.svg */ "./resources/img/sign-up.svg"), alt: "" }
              })
            ]),
            _vm._v(" "),
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("div", { staticClass: "desc-img" }, [
                _vm._v("Sign Up For Free!")
              ])
            ]),
            _vm._v(" "),
            _vm.isError
              ? _c("b-col", { attrs: { cols: "12" } }, [
                  _c("div", { staticClass: "text-error" }, [
                    _vm._v(
                      "\n          " + _vm._s(_vm.errorMessage) + "\n        "
                    )
                  ])
                ])
              : _vm._e()
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c(
              "b-col",
              { attrs: { cols: "12" } },
              [
                _vm.show
                  ? _c(
                      "b-form",
                      { on: { submit: _vm.onSubmit } },
                      [
                        _c("b-form-group", { attrs: { id: "input-group-1" } }, [
                          _c(
                            "div",
                            { staticClass: "input-icon" },
                            [
                              _c("b-form-input", {
                                attrs: {
                                  id: "input-1",
                                  placeholder: "Username",
                                  required: ""
                                },
                                model: {
                                  value: _vm.form.name,
                                  callback: function($$v) {
                                    _vm.$set(_vm.form, "name", $$v)
                                  },
                                  expression: "form.name"
                                }
                              }),
                              _vm._v(" "),
                              _c("auth-tooltip")
                            ],
                            1
                          )
                        ]),
                        _vm._v(" "),
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-2" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-2",
                                "aria-describedby": "password-help-block",
                                placeholder: "Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.pass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "pass", $$v)
                                },
                                expression: "form.pass"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-3" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-3",
                                "aria-describedby": "password-help-block",
                                placeholder: "Confirm Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.conpass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "conpass", $$v)
                                },
                                expression: "form.conpass"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-4" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                id: "input-4",
                                placeholder: "Affiliate Code"
                              },
                              model: {
                                value: _vm.form.code,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "code", $$v)
                                },
                                expression: "form.code"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-create",
                            attrs: { type: "submit", disabled: _vm.loading }
                          },
                          [
                            _vm._v(
                              "\n            Create New Account\n          "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            ),
            _vm._v(" "),
            _c("b-col", { attrs: { cols: "12" } }, [
              _c(
                "div",
                { staticClass: "have-login" },
                [
                  _vm._v("\n          Already registered? Login\n          "),
                  _c("router-link", { attrs: { to: "/login" } }, [
                    _vm._v("Here")
                  ]),
                  _vm._v("!\n        ")
                ],
                1
              )
            ])
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e&":
/*!***********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e& ***!
  \***********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "forgot-password" }, [
    _c(
      "div",
      { staticClass: "container" },
      [
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c("b-col", { attrs: { cols: "12" } }, [
              _c("div", { staticClass: "title-hed" }, [
                _vm._v("Reset Password")
              ])
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c(
          "b-row",
          {
            staticClass: "justify-content-center",
            attrs: { "no-gutters": "" }
          },
          [
            _c(
              "b-col",
              { attrs: { cols: "12" } },
              [
                _vm.show
                  ? _c(
                      "b-form",
                      { on: { submit: _vm.onSubmit } },
                      [
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-1" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-2",
                                "aria-describedby": "password-help-block",
                                placeholder: "New Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.pass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "pass", $$v)
                                },
                                expression: "form.pass"
                              }
                            })
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "b-form-group",
                          { attrs: { id: "input-group-2" } },
                          [
                            _c("b-form-input", {
                              attrs: {
                                type: "password",
                                id: "input-3",
                                "aria-describedby": "password-help-block",
                                placeholder: "Confirm Password",
                                required: ""
                              },
                              model: {
                                value: _vm.form.conpass,
                                callback: function($$v) {
                                  _vm.$set(_vm.form, "conpass", $$v)
                                },
                                expression: "form.conpass"
                              }
                            }),
                            _vm._v(" "),
                            this.$store.state.auth.resetPassword.isError
                              ? _c(
                                  "div",
                                  {
                                    staticClass: "text-error",
                                    staticStyle: { "margin-top": "15px" }
                                  },
                                  [
                                    _vm._v(
                                      "\n              " +
                                        _vm._s(
                                          this.$store.state.auth.resetPassword
                                            .errorMessage
                                        ) +
                                        "\n            "
                                    )
                                  ]
                                )
                              : _vm._e()
                          ],
                          1
                        ),
                        _vm._v(" "),
                        _c(
                          "button",
                          {
                            staticClass: "btn btn-create",
                            attrs: { type: "submit", disabled: _vm.isLoading }
                          },
                          [_vm._v("\n            reset password\n          ")]
                        )
                      ],
                      1
                    )
                  : _vm._e()
              ],
              1
            )
          ],
          1
        )
      ],
      1
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=template&id=882405a8&":
/*!******************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Settings.vue?vue&type=template&id=882405a8& ***!
  \******************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "setting" },
    [
      _c(
        "b-container",
        { staticClass: "container-main", attrs: { fluid: "" } },
        [
          _c(
            "b-row",
            { attrs: { "no-gutters": "" } },
            [
              _c("b-col", { attrs: { cols: "2" } }, [
                _c("div", { staticClass: "menu-side" }, [
                  _c("div", { staticClass: "header-menu text-left" }, [
                    _vm._v("Settings")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "menu-item" },
                    [
                      _c(
                        "b-nav",
                        { attrs: { vertical: "" } },
                        [
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 1 },
                              on: {
                                click: function($event) {
                                  return _vm.userProfile()
                                }
                              }
                            },
                            [_vm._v("User profile")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 2 },
                              on: {
                                click: function($event) {
                                  return _vm.password()
                                }
                              }
                            },
                            [_vm._v("Password")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 3 },
                              on: {
                                click: function($event) {
                                  return _vm.twoFA()
                                }
                              }
                            },
                            [_vm._v("Two-factor authentication")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 4 },
                              on: {
                                click: function($event) {
                                  return _vm.telegram()
                                }
                              }
                            },
                            [_vm._v("Telegram")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 5 },
                              on: {
                                click: function($event) {
                                  return _vm.accountDelete()
                                }
                              }
                            },
                            [_vm._v("Account deletion")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("b-col", [
                _c("div", { staticClass: "content-side" }, [
                  _c("div", { staticClass: "header-menu text-left" }, [
                    _vm._v(_vm._s(_vm.titleCard))
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "main-content" }, [
                    _vm.selected == 1
                      ? _c(
                          "div",
                          { staticClass: "cont-table" },
                          [_c("user-profile")],
                          1
                        )
                      : _vm.selected == 2
                      ? _c(
                          "div",
                          { staticClass: "cont-table" },
                          [_c("password-setting")],
                          1
                        )
                      : _vm.selected == 3
                      ? _c(
                          "div",
                          { staticClass: "cont-table" },
                          [_c("two-fa-setting")],
                          1
                        )
                      : _vm.selected == 4
                      ? _c(
                          "div",
                          { staticClass: "cont-table" },
                          [_c("telegram-setting")],
                          1
                        )
                      : _vm.selected == 5
                      ? _c(
                          "div",
                          { staticClass: "cont-table" },
                          [_c("account-deletion-setting")],
                          1
                        )
                      : _vm._e()
                  ])
                ])
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=template&id=e343ce54&":
/*!*****************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Support.vue?vue&type=template&id=e343ce54& ***!
  \*****************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "support" },
    [
      _c("b-jumbotron", { staticClass: "mx-auto" }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "content-htp" },
        [
          _c(
            "b-row",
            {
              staticStyle: { "margin-top": "20px" },
              attrs: { "no-gutters": "" }
            },
            [
              _c("b-col", { staticClass: "text-left", attrs: { cols: "2" } }, [
                _c("div", { staticClass: "title-head" }, [
                  _vm._v("\n                  Support\n              ")
                ])
              ])
            ],
            1
          ),
          _vm._v(" "),
          _vm._m(0)
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "content-main" }, [
      _c("span", [_vm._v(" Title Here ")]),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n              "
      ),
      _c("br"),
      _c("br"),
      _vm._v(
        "\n              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.\n      "
      )
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44&":
/*!**********************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44& ***!
  \**********************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "transaction" },
    [
      _c(
        "b-container",
        { staticClass: "container-main", attrs: { fluid: "" } },
        [
          _c(
            "b-row",
            { attrs: { "no-gutters": "" } },
            [
              _c("b-col", { attrs: { cols: "2" } }, [
                _c("div", { staticClass: "menu-side" }, [
                  _c("div", { staticClass: "header-menu text-left" }, [
                    _vm._v("Transactions")
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "menu-item" },
                    [
                      _c(
                        "b-nav",
                        { attrs: { vertical: "" } },
                        [
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 1 },
                              on: {
                                click: function($event) {
                                  _vm.bet()
                                  _vm.selected = 1
                                }
                              }
                            },
                            [_vm._v("Bets")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 2 },
                              on: {
                                click: function($event) {
                                  _vm.debitCredit("credit")
                                  _vm.selected = 2
                                }
                              }
                            },
                            [_vm._v("Credits")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 3 },
                              on: {
                                click: function($event) {
                                  _vm.debitCredit("debit")
                                  _vm.selected = 3
                                }
                              }
                            },
                            [_vm._v("Debits")]
                          ),
                          _vm._v(" "),
                          _c(
                            "b-nav-item",
                            {
                              class: { activelink: _vm.selected == 4 },
                              on: {
                                click: function($event) {
                                  _vm.referFriend()
                                  _vm.selected = 4
                                }
                              }
                            },
                            [_vm._v("refer-a-friend")]
                          )
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ]),
              _vm._v(" "),
              _c("b-col", [
                _c("div", { staticClass: "content-side" }, [
                  _c("div", { staticClass: "header-menu text-left" }, [
                    _vm._v(_vm._s(_vm.titleCard))
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "main-content" },
                    [
                      _c(
                        "b-row",
                        { attrs: { "no-gutters": "" } },
                        [
                          _c(
                            "b-col",
                            { staticClass: "dropdown-table text-left" },
                            [
                              _c(
                                "b-dropdown",
                                {
                                  attrs: {
                                    size: "sm",
                                    "toggle-class": "text-decoration-none",
                                    center: "",
                                    "no-caret": ""
                                  },
                                  scopedSlots: _vm._u([
                                    {
                                      key: "button-content",
                                      fn: function() {
                                        return [
                                          _c("input", {
                                            directives: [
                                              {
                                                name: "model",
                                                rawName: "v-model",
                                                value: _vm.selectedStatusName,
                                                expression: "selectedStatusName"
                                              }
                                            ],
                                            staticClass: "custom-select-table",
                                            attrs: {
                                              type: "text",
                                              disabled: ""
                                            },
                                            domProps: {
                                              value: _vm.selectedStatusName
                                            },
                                            on: {
                                              input: function($event) {
                                                if ($event.target.composing) {
                                                  return
                                                }
                                                _vm.selectedStatusName =
                                                  $event.target.value
                                              }
                                            }
                                          })
                                        ]
                                      },
                                      proxy: true
                                    }
                                  ])
                                },
                                [
                                  _vm._v(" "),
                                  _vm._l(_vm.statuses, function(status, index) {
                                    return _c(
                                      "b-dropdown-item",
                                      {
                                        key: status,
                                        on: {
                                          click: function($event) {
                                            return _vm.statusChanged(index)
                                          }
                                        }
                                      },
                                      [
                                        _vm._v(
                                          "\n                    " +
                                            _vm._s(status) +
                                            "\n                  "
                                        )
                                      ]
                                    )
                                  })
                                ],
                                2
                              )
                            ],
                            1
                          ),
                          _vm._v(" "),
                          _c("b-col", { staticClass: "date-cont text-right" }, [
                            _c("div", { staticClass: "cont-dates" }, [
                              _c(
                                "div",
                                { staticClass: "date1" },
                                [
                                  _c("date-picker", {
                                    attrs: { placeholder: "Start Date" },
                                    on: { change: _vm.filter },
                                    model: {
                                      value: _vm.dateStart,
                                      callback: function($$v) {
                                        _vm.dateStart = $$v
                                      },
                                      expression: "dateStart"
                                    }
                                  })
                                ],
                                1
                              ),
                              _vm._v(" "),
                              _c("div", { staticClass: "date-separator" }, [
                                _vm._v("TO")
                              ]),
                              _vm._v(" "),
                              _c(
                                "div",
                                { staticClass: "date1" },
                                [
                                  _c("date-picker", {
                                    attrs: { placeholder: "End Date" },
                                    on: { change: _vm.filter },
                                    model: {
                                      value: _vm.dateFinish,
                                      callback: function($$v) {
                                        _vm.dateFinish = $$v
                                      },
                                      expression: "dateFinish"
                                    }
                                  })
                                ],
                                1
                              )
                            ])
                          ])
                        ],
                        1
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "cont-table" },
                        [
                          _c("v-client-table", {
                            ref: "table",
                            staticClass: "vue-tables-custom-style",
                            attrs: {
                              data: _vm.tableData,
                              columns: _vm.columns,
                              options: _vm.options
                            }
                          }),
                          _vm._v(" "),
                          _vm.tableData.length != 0
                            ? _c(
                                "div",
                                {
                                  staticClass: "exp-tbl",
                                  on: { click: _vm.downloadCsv }
                                },
                                [
                                  _c("img", {
                                    attrs: {
                                      src: __webpack_require__(/*! ../../img/export-csv.svg */ "./resources/img/export-csv.svg"),
                                      alt: ""
                                    }
                                  })
                                ]
                              )
                            : _vm._e()
                        ],
                        1
                      )
                    ],
                    1
                  )
                ])
              ])
            ],
            1
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./resources/img sync recursive ^\\.\\/.*$":
/*!*************************************!*\
  !*** ./resources/img sync ^\.\/.*$ ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./2fa.svg": "./resources/img/2fa.svg",
	"./3D-GORKIPI.svg": "./resources/img/3D-GORKIPI.svg",
	"./3d-dice.svg": "./resources/img/3d-dice.svg",
	"./3d-mines.svg": "./resources/img/3d-mines.svg",
	"./3d-moonshot.svg": "./resources/img/3d-moonshot.svg",
	"./3d-sikipi.svg": "./resources/img/3d-sikipi.svg",
	"./Huatbet logo-02.svg": "./resources/img/Huatbet logo-02.svg",
	"./Huatbet logo-05.svg": "./resources/img/Huatbet logo-05.svg",
	"./Huatbet-02.svg": "./resources/img/Huatbet-02.svg",
	"./Welcome-to.svg": "./resources/img/Welcome-to.svg",
	"./algo.svg": "./resources/img/algo.svg",
	"./background-home-mobile.png": "./resources/img/background-home-mobile.png",
	"./background-home.png": "./resources/img/background-home.png",
	"./bell.svg": "./resources/img/bell.svg",
	"./burger-menu.svg": "./resources/img/burger-menu.svg",
	"./carousel-next.svg": "./resources/img/carousel-next.svg",
	"./carousel-prev.svg": "./resources/img/carousel-prev.svg",
	"./chevron down.svg": "./resources/img/chevron down.svg",
	"./chevron-big-down.svg": "./resources/img/chevron-big-down.svg",
	"./contoh.svg": "./resources/img/contoh.svg",
	"./copy.svg": "./resources/img/copy.svg",
	"./divider.svg": "./resources/img/divider.svg",
	"./export-csv.svg": "./resources/img/export-csv.svg",
	"./faq-jumbotron.svg": "./resources/img/faq-jumbotron.svg",
	"./howtoplay-jumbotron.svg": "./resources/img/howtoplay-jumbotron.svg",
	"./info.svg": "./resources/img/info.svg",
	"./join-qa.svg": "./resources/img/join-qa.svg",
	"./log-in.svg": "./resources/img/log-in.svg",
	"./logo.png": "./resources/img/logo.png",
	"./pf-crash.svg": "./resources/img/pf-crash.svg",
	"./play-game.svg": "./resources/img/play-game.svg",
	"./pocket.svg": "./resources/img/pocket.svg",
	"./provably-jumbotron.svg": "./resources/img/provably-jumbotron.svg",
	"./qr-example.svg": "./resources/img/qr-example.svg",
	"./sign-up.svg": "./resources/img/sign-up.svg",
	"./succes-info.svg": "./resources/img/succes-info.svg",
	"./support-jumbotron.svg": "./resources/img/support-jumbotron.svg",
	"./telegram.svg": "./resources/img/telegram.svg",
	"./tokenAlgo.svg": "./resources/img/tokenAlgo.svg",
	"./twitter.svg": "./resources/img/twitter.svg"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/img sync recursive ^\\.\\/.*$";

/***/ }),

/***/ "./resources/img/2fa.svg":
/*!*******************************!*\
  !*** ./resources/img/2fa.svg ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/2fa.svg?145a80135c30b4f0cd70b7b9682368f7";

/***/ }),

/***/ "./resources/img/3D-GORKIPI.svg":
/*!**************************************!*\
  !*** ./resources/img/3D-GORKIPI.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/3D-GORKIPI.svg?ccb19864b5fb5fea1e5d060a86648e73";

/***/ }),

/***/ "./resources/img/3d-dice.svg":
/*!***********************************!*\
  !*** ./resources/img/3d-dice.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/3d-dice.svg?15073aa4290680720c1d48e1307d2177";

/***/ }),

/***/ "./resources/img/3d-mines.svg":
/*!************************************!*\
  !*** ./resources/img/3d-mines.svg ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/3d-mines.svg?2648b593342fe65bfe53de11eb3847fe";

/***/ }),

/***/ "./resources/img/3d-moonshot.svg":
/*!***************************************!*\
  !*** ./resources/img/3d-moonshot.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/3d-moonshot.svg?49185ffb33b4ad3cd9d6d9f4234a9592";

/***/ }),

/***/ "./resources/img/3d-sikipi.svg":
/*!*************************************!*\
  !*** ./resources/img/3d-sikipi.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/3d-sikipi.svg?6ac00b57fb2eb4a9de1e49920dfa1890";

/***/ }),

/***/ "./resources/img/Huatbet logo-02.svg":
/*!*******************************************!*\
  !*** ./resources/img/Huatbet logo-02.svg ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/Huatbet logo-02.svg?8a4816036a3e9499165419539beee5ce";

/***/ }),

/***/ "./resources/img/Huatbet logo-05.svg":
/*!*******************************************!*\
  !*** ./resources/img/Huatbet logo-05.svg ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/Huatbet logo-05.svg?2b674c8be939fd8945fc1c4c77818947";

/***/ }),

/***/ "./resources/img/Huatbet-02.svg":
/*!**************************************!*\
  !*** ./resources/img/Huatbet-02.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/Huatbet-02.svg?03a7381ab06b1ea41123e224832f7a38";

/***/ }),

/***/ "./resources/img/Welcome-to.svg":
/*!**************************************!*\
  !*** ./resources/img/Welcome-to.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/Welcome-to.svg?9dd761e3638282563ead4e102d091f18";

/***/ }),

/***/ "./resources/img/algo.svg":
/*!********************************!*\
  !*** ./resources/img/algo.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/algo.svg?6fe0f46b13ac578ca7d67d2300a30fe5";

/***/ }),

/***/ "./resources/img/background-home-mobile.png":
/*!**************************************************!*\
  !*** ./resources/img/background-home-mobile.png ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/background-home-mobile.png?79340cf6b5a8233f6a5e702af2447a8e";

/***/ }),

/***/ "./resources/img/background-home.png":
/*!*******************************************!*\
  !*** ./resources/img/background-home.png ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/background-home.png?37c959bb18f13b37f752a2ce0db88726";

/***/ }),

/***/ "./resources/img/bell.svg":
/*!********************************!*\
  !*** ./resources/img/bell.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/bell.svg?60695183f45313f73241474bdac6fd3a";

/***/ }),

/***/ "./resources/img/burger-menu.svg":
/*!***************************************!*\
  !*** ./resources/img/burger-menu.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/burger-menu.svg?488d7ae3e464c48947f68fb799e817f1";

/***/ }),

/***/ "./resources/img/carousel-next.svg":
/*!*****************************************!*\
  !*** ./resources/img/carousel-next.svg ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/carousel-next.svg?ee6fb0c6dc94320beb721e3246ca0976";

/***/ }),

/***/ "./resources/img/carousel-prev.svg":
/*!*****************************************!*\
  !*** ./resources/img/carousel-prev.svg ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/carousel-prev.svg?0bd7b55d7ed396f39c54246f7d710592";

/***/ }),

/***/ "./resources/img/chevron down.svg":
/*!****************************************!*\
  !*** ./resources/img/chevron down.svg ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/chevron down.svg?2fced5f79468c3891802d65d6d5a687b";

/***/ }),

/***/ "./resources/img/chevron-big-down.svg":
/*!********************************************!*\
  !*** ./resources/img/chevron-big-down.svg ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/chevron-big-down.svg?e1351f560f96e69d457e62c1087cd2b9";

/***/ }),

/***/ "./resources/img/contoh.svg":
/*!**********************************!*\
  !*** ./resources/img/contoh.svg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/contoh.svg?85b8867c2f955e07740802d13a51b670";

/***/ }),

/***/ "./resources/img/copy.svg":
/*!********************************!*\
  !*** ./resources/img/copy.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/copy.svg?3115bc374482ec183b99bfd7de1a0afa";

/***/ }),

/***/ "./resources/img/divider.svg":
/*!***********************************!*\
  !*** ./resources/img/divider.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/divider.svg?15706e8c2720d29d7d3cdb388cf06960";

/***/ }),

/***/ "./resources/img/export-csv.svg":
/*!**************************************!*\
  !*** ./resources/img/export-csv.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/export-csv.svg?135601c5cd0c37837f3bca89e03c3077";

/***/ }),

/***/ "./resources/img/faq-jumbotron.svg":
/*!*****************************************!*\
  !*** ./resources/img/faq-jumbotron.svg ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/faq-jumbotron.svg?9af605335651ac192a2538c2e0655505";

/***/ }),

/***/ "./resources/img/howtoplay-jumbotron.svg":
/*!***********************************************!*\
  !*** ./resources/img/howtoplay-jumbotron.svg ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/howtoplay-jumbotron.svg?23aeb2487aa2af20667d237d1475c67e";

/***/ }),

/***/ "./resources/img/info.svg":
/*!********************************!*\
  !*** ./resources/img/info.svg ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/info.svg?a9d9773829746d4c5e04987e3a82fbc1";

/***/ }),

/***/ "./resources/img/join-qa.svg":
/*!***********************************!*\
  !*** ./resources/img/join-qa.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/join-qa.svg?f9a6e1facb049d464c947e3b4cce43ac";

/***/ }),

/***/ "./resources/img/log-in.svg":
/*!**********************************!*\
  !*** ./resources/img/log-in.svg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/log-in.svg?21a1a1c8016603c3cafa863a15895ae7";

/***/ }),

/***/ "./resources/img/logo.png":
/*!********************************!*\
  !*** ./resources/img/logo.png ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/logo.png?82b9c7a5a3f405032b1db71a25f67021";

/***/ }),

/***/ "./resources/img/pf-crash.svg":
/*!************************************!*\
  !*** ./resources/img/pf-crash.svg ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/pf-crash.svg?794fd05e2502b27e03d1014dd9bde6b0";

/***/ }),

/***/ "./resources/img/play-game.svg":
/*!*************************************!*\
  !*** ./resources/img/play-game.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/play-game.svg?9a4b5b225f28cfc156d4f5231324ea24";

/***/ }),

/***/ "./resources/img/pocket.svg":
/*!**********************************!*\
  !*** ./resources/img/pocket.svg ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/pocket.svg?f673f78972333ab37558feb45c995d36";

/***/ }),

/***/ "./resources/img/provably-jumbotron.svg":
/*!**********************************************!*\
  !*** ./resources/img/provably-jumbotron.svg ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/provably-jumbotron.svg?1ce910ef5e37f0305646206b6b3b8588";

/***/ }),

/***/ "./resources/img/qr-example.svg":
/*!**************************************!*\
  !*** ./resources/img/qr-example.svg ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/qr-example.svg?8d932f6c26b29466945ab82a443b7ba3";

/***/ }),

/***/ "./resources/img/sign-up.svg":
/*!***********************************!*\
  !*** ./resources/img/sign-up.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/sign-up.svg?66ac43c325dadf17cda41ea5f380c206";

/***/ }),

/***/ "./resources/img/succes-info.svg":
/*!***************************************!*\
  !*** ./resources/img/succes-info.svg ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/succes-info.svg?a463b434a8609a943df58166e2897a69";

/***/ }),

/***/ "./resources/img/support-jumbotron.svg":
/*!*********************************************!*\
  !*** ./resources/img/support-jumbotron.svg ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/support-jumbotron.svg?320df5693882f3f38b6912e3cfcd6194";

/***/ }),

/***/ "./resources/img/telegram.svg":
/*!************************************!*\
  !*** ./resources/img/telegram.svg ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/telegram.svg?5a33fe3dd05e48e4360416c61aa805e5";

/***/ }),

/***/ "./resources/img/tokenAlgo.svg":
/*!*************************************!*\
  !*** ./resources/img/tokenAlgo.svg ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/tokenAlgo.svg?0f21f6dc6e20d6168605bba9b6866948";

/***/ }),

/***/ "./resources/img/twitter.svg":
/*!***********************************!*\
  !*** ./resources/img/twitter.svg ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/images/twitter.svg?1551b1cea4afb05e1fd04555ea8bc2d3";

/***/ }),

/***/ "./resources/js sync recursive \\.vue$/":
/*!***********************************!*\
  !*** ./resources/js sync \.vue$/ ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./components/App.vue": "./resources/js/components/App.vue",
	"./components/accountDeletionSetting.vue": "./resources/js/components/accountDeletionSetting.vue",
	"./components/authTooltip.vue": "./resources/js/components/authTooltip.vue",
	"./components/cardCarouser.vue": "./resources/js/components/cardCarouser.vue",
	"./components/donutChart.vue": "./resources/js/components/donutChart.vue",
	"./components/inviteRaf.vue": "./resources/js/components/inviteRaf.vue",
	"./components/jumbotronCard.vue": "./resources/js/components/jumbotronCard.vue",
	"./components/modalPocket.vue": "./resources/js/components/modalPocket.vue",
	"./components/navFooter.vue": "./resources/js/components/navFooter.vue",
	"./components/navHeader.vue": "./resources/js/components/navHeader.vue",
	"./components/passwordSetting.vue": "./resources/js/components/passwordSetting.vue",
	"./components/statsAffiliates.vue": "./resources/js/components/statsAffiliates.vue",
	"./components/statsReferrer.vue": "./resources/js/components/statsReferrer.vue",
	"./components/tableHome.vue": "./resources/js/components/tableHome.vue",
	"./components/telegramSetting.vue": "./resources/js/components/telegramSetting.vue",
	"./components/twoFaSetting.vue": "./resources/js/components/twoFaSetting.vue",
	"./components/userProfile.vue": "./resources/js/components/userProfile.vue",
	"./pages/Dashboard.vue": "./resources/js/pages/Dashboard.vue",
	"./pages/Faq.vue": "./resources/js/pages/Faq.vue",
	"./pages/ForgotPassword.vue": "./resources/js/pages/ForgotPassword.vue",
	"./pages/Home.vue": "./resources/js/pages/Home.vue",
	"./pages/HowToPlay.vue": "./resources/js/pages/HowToPlay.vue",
	"./pages/Login.vue": "./resources/js/pages/Login.vue",
	"./pages/Login2fa.vue": "./resources/js/pages/Login2fa.vue",
	"./pages/ProvablyFair.vue": "./resources/js/pages/ProvablyFair.vue",
	"./pages/ReferFriends.vue": "./resources/js/pages/ReferFriends.vue",
	"./pages/Register.vue": "./resources/js/pages/Register.vue",
	"./pages/ResetPassword.vue": "./resources/js/pages/ResetPassword.vue",
	"./pages/Settings.vue": "./resources/js/pages/Settings.vue",
	"./pages/Support.vue": "./resources/js/pages/Support.vue",
	"./pages/Transactions.vue": "./resources/js/pages/Transactions.vue"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./resources/js sync recursive \\.vue$/";

/***/ }),

/***/ "./resources/js/app.js":
/*!*****************************!*\
  !*** ./resources/js/app.js ***!
  \*****************************/
/*! exports provided: router */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "router", function() { return router; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue-router */ "./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var _routes__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./routes */ "./resources/js/routes.js");
/* harmony import */ var _store__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./store */ "./resources/js/store/index.js");
/* harmony import */ var _components_App__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/App */ "./resources/js/components/App.vue");
/* harmony import */ var bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! bootstrap-vue */ "./node_modules/bootstrap-vue/esm/index.js");
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! bootstrap/dist/css/bootstrap.css */ "./node_modules/bootstrap/dist/css/bootstrap.css");
/* harmony import */ var bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(bootstrap_dist_css_bootstrap_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var bootstrap_vue_dist_bootstrap_vue_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap-vue/dist/bootstrap-vue.css */ "./node_modules/bootstrap-vue/dist/bootstrap-vue.css");
/* harmony import */ var bootstrap_vue_dist_bootstrap_vue_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(bootstrap_vue_dist_bootstrap_vue_css__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var vue_tables_2__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! vue-tables-2 */ "./node_modules/vue-tables-2/compiled/index.js");
/* harmony import */ var vue_tables_2__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(vue_tables_2__WEBPACK_IMPORTED_MODULE_8__);
__webpack_require__(/*! ./bootstrap */ "./resources/js/bootstrap.js");



vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_router__WEBPACK_IMPORTED_MODULE_1__["default"]);



/*
*
* BOOTSTRAP VUE
*
 */


vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["CollapsePlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["ButtonPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["NavPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["NavbarPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["ModalPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["CardPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["DropdownPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["ListGroupPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["InputGroupPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["FormInputPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["TabsPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["BadgePlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["TooltipPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["FormCheckboxPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["FormSelectPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["JumbotronPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["CarouselPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["LayoutPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["IconsPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["FormPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["PopoverPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["FormGroupPlugin"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(bootstrap_vue__WEBPACK_IMPORTED_MODULE_5__["AlertPlugin"]); // Optionally install the BootstrapVue icon components plugin




vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_tables_2__WEBPACK_IMPORTED_MODULE_8__["ClientTable"]);
vue__WEBPACK_IMPORTED_MODULE_0___default.a.use(vue_tables_2__WEBPACK_IMPORTED_MODULE_8__["ServerTable"]);
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

var files = __webpack_require__("./resources/js sync recursive \\.vue$/");

files.keys().map(function (key) {
  return vue__WEBPACK_IMPORTED_MODULE_0___default.a.component(key.split('/').pop().split('.')[0], files(key)["default"]);
});
var router = new vue_router__WEBPACK_IMPORTED_MODULE_1__["default"]({
  mode: 'history',
  routes: _routes__WEBPACK_IMPORTED_MODULE_2__["default"]
});
router.beforeEach(function (to, from, next) {
  next();
});
window.BASE_API = '/api/';
var app = new vue__WEBPACK_IMPORTED_MODULE_0___default.a({
  el: '#app',
  router: router,
  store: _store__WEBPACK_IMPORTED_MODULE_3__["default"],
  render: function render(h) {
    return h(_components_App__WEBPACK_IMPORTED_MODULE_4__["default"]);
  }
});

/***/ }),

/***/ "./resources/js/bootstrap.js":
/*!***********************************!*\
  !*** ./resources/js/bootstrap.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

window._ = __webpack_require__(/*! lodash */ "./node_modules/lodash/lodash.js");
/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
var token = localStorage.getItem('token');

if (token) {
  window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + localStorage.getItem('token');
}

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest'; // Add a 401 response interceptor

window.axios.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  if (401 === error.response.status && window.location.pathname != '/') {
    localStorage.removeItem('token');
    window.location = '/';
  } else {
    return Promise.reject(error);
  }
});
/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */
// import Echo from 'laravel-echo';
// window.Pusher = require('pusher-js');
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

/***/ }),

/***/ "./resources/js/components/App.vue":
/*!*****************************************!*\
  !*** ./resources/js/components/App.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./App.vue?vue&type=template&id=332fccf4& */ "./resources/js/components/App.vue?vue&type=template&id=332fccf4&");
/* harmony import */ var _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./App.vue?vue&type=script&lang=js& */ "./resources/js/components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./App.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["render"],
  _App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/App.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/App.vue?vue&type=template&id=332fccf4&":
/*!************************************************************************!*\
  !*** ./resources/js/components/App.vue?vue&type=template&id=332fccf4& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./App.vue?vue&type=template&id=332fccf4& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/App.vue?vue&type=template&id=332fccf4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_App_vue_vue_type_template_id_332fccf4___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/accountDeletionSetting.vue":
/*!************************************************************!*\
  !*** ./resources/js/components/accountDeletionSetting.vue ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./accountDeletionSetting.vue?vue&type=template&id=65dd9640& */ "./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640&");
/* harmony import */ var _accountDeletionSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./accountDeletionSetting.vue?vue&type=script&lang=js& */ "./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _accountDeletionSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__["render"],
  _accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/accountDeletionSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./accountDeletionSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************************!*\
  !*** ./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./accountDeletionSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640&":
/*!*******************************************************************************************!*\
  !*** ./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640& ***!
  \*******************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./accountDeletionSetting.vue?vue&type=template&id=65dd9640& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/accountDeletionSetting.vue?vue&type=template&id=65dd9640&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_accountDeletionSetting_vue_vue_type_template_id_65dd9640___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/authTooltip.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/authTooltip.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./authTooltip.vue?vue&type=template&id=35728ac0& */ "./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0&");
/* harmony import */ var _authTooltip_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./authTooltip.vue?vue&type=script&lang=js& */ "./resources/js/components/authTooltip.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./authTooltip.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _authTooltip_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/authTooltip.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/authTooltip.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/authTooltip.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./authTooltip.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./authTooltip.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./authTooltip.vue?vue&type=template&id=35728ac0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/authTooltip.vue?vue&type=template&id=35728ac0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_authTooltip_vue_vue_type_template_id_35728ac0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/cardCarouser.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/cardCarouser.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cardCarouser.vue?vue&type=template&id=152a2561& */ "./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561&");
/* harmony import */ var _cardCarouser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./cardCarouser.vue?vue&type=script&lang=js& */ "./resources/js/components/cardCarouser.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./cardCarouser.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _cardCarouser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__["render"],
  _cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/cardCarouser.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/cardCarouser.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/cardCarouser.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./cardCarouser.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./cardCarouser.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./cardCarouser.vue?vue&type=template&id=152a2561& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cardCarouser.vue?vue&type=template&id=152a2561&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_cardCarouser_vue_vue_type_template_id_152a2561___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/chartStats.js":
/*!***********************************************!*\
  !*** ./resources/js/components/chartStats.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue_chartjs__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! vue-chartjs */ "./node_modules/vue-chartjs/es/index.js");
 // var plugin = function(chart) {
//     var ctx = chart.chart.ctx;
//     var gradient = ctx.createLinearGradient(0, 0, 0, 510);
//     gradient.addColorStop(0, 'rgba(250,174,50,1)');   
//     gradient.addColorStop(1, 'rgba(250,174,50,0)');
//     chart.data.datasets.backgroundColor = gradient;
//     //this.chartdata.datasets.backgroundColor= gradient;
// }

/* harmony default export */ __webpack_exports__["default"] = ({
  "extends": vue_chartjs__WEBPACK_IMPORTED_MODULE_0__["Line"],
  data: function data() {
    return {
      gradient: null,
      chartdata: {
        labels: this.labelChart,
        datasets: [{
          backgroundColor: this.gradient,
          //'#f87979'
          data: this.dataChart,
          borderColor: '#FFCD3E'
        }]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        elements: {
          point: {
            radius: 0
          }
        },
        tooltips: {
          callbacks: {
            label: function label(tooltipItem) {
              return tooltipItem.yLabel;
            }
          }
        },
        // scaleShowLabels:false,
        // scaleFontSize:0,
        scales: {
          xAxes: [{
            time: {
              unit: 'Areas'
            },
            gridLines: {
              display: false,
              drawBorder: false,
              color: '#1D1E21'
            },
            ticks: {
              maxTicksLimit: 7,
              display: true,
              autoSkip: true,
              fontFamily: 'Open Sans',
              fontSize: 12,
              fontColor: '#797979',
              paddingLeft: 20
            } //'dataset.maxBarThickness': 5,

          }],
          yAxes: [{
            //   afterTickToLabelConversion: function(scaleInstance) {               
            //       scaleInstance.ticks[0] = null;
            //       scaleInstance.ticks[scaleInstance.ticks.length - 1] = null;
            //       scaleInstance.ticksAsNumbers[0] = null;
            //       scaleInstance.ticksAsNumbers[scaleInstance.ticksAsNumbers.length - 1] = null;
            //     },
            time: {
              unit: 'Areas'
            },
            gridLines: {
              display: true,
              drawBorder: false,
              color: '#EEEEEE',
              zeroLineWidth: 2,
              zeroLineColor: '#DDDDDD'
            },
            ticks: {
              //precision: 5,
              //stepSize: 40,
              // sampleSize: 1,
              // min: -100,
              // max: 100,
              display: true,
              beginAtZero: true,
              maxTicksLimit: 8,
              autoSkip: true,
              fontFamily: 'Open Sans',
              fontSize: 12,
              fontColor: '#797979',
              padding: 10
            },
            minor: {
              fontColor: 'yellow'
            } //'dataset.maxBarThickness': 5,

          }]
        }
      }
    };
  },
  props: {
    labelChart: Array,
    dataChart: Array
  },
  mounted: function mounted() {
    // this.addPlugin({
    //     id: 'my-plugin',
    //     beforeDraw: plugin,
    //   })
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
    this.gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
    this.gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
    this.gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');
    this.renderChart(this.chartdata, this.options);
  }
});

/***/ }),

/***/ "./resources/js/components/donutChart.vue":
/*!************************************************!*\
  !*** ./resources/js/components/donutChart.vue ***!
  \************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./donutChart.vue?vue&type=template&id=4ea25067& */ "./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067&");
/* harmony import */ var _donutChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./donutChart.vue?vue&type=script&lang=js& */ "./resources/js/components/donutChart.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./donutChart.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _donutChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__["render"],
  _donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/donutChart.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/donutChart.vue?vue&type=script&lang=js&":
/*!*************************************************************************!*\
  !*** ./resources/js/components/donutChart.vue?vue&type=script&lang=js& ***!
  \*************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./donutChart.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./donutChart.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067&":
/*!*******************************************************************************!*\
  !*** ./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067& ***!
  \*******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./donutChart.vue?vue&type=template&id=4ea25067& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/donutChart.vue?vue&type=template&id=4ea25067&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_donutChart_vue_vue_type_template_id_4ea25067___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/inviteRaf.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/inviteRaf.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./inviteRaf.vue?vue&type=template&id=88c928da& */ "./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da&");
/* harmony import */ var _inviteRaf_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./inviteRaf.vue?vue&type=script&lang=js& */ "./resources/js/components/inviteRaf.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./inviteRaf.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _inviteRaf_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__["render"],
  _inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/inviteRaf.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/inviteRaf.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/inviteRaf.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./inviteRaf.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./inviteRaf.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./inviteRaf.vue?vue&type=template&id=88c928da& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/inviteRaf.vue?vue&type=template&id=88c928da&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_inviteRaf_vue_vue_type_template_id_88c928da___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/jumbotronCard.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/jumbotronCard.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./jumbotronCard.vue?vue&type=template&id=16fc8ca1& */ "./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1&");
/* harmony import */ var _jumbotronCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./jumbotronCard.vue?vue&type=script&lang=js& */ "./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _jumbotronCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__["render"],
  _jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/jumbotronCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_jumbotronCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./jumbotronCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/jumbotronCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_jumbotronCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./jumbotronCard.vue?vue&type=template&id=16fc8ca1& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/jumbotronCard.vue?vue&type=template&id=16fc8ca1&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_jumbotronCard_vue_vue_type_template_id_16fc8ca1___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/modalPocket.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/modalPocket.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./modalPocket.vue?vue&type=template&id=4d297fb0& */ "./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0&");
/* harmony import */ var _modalPocket_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modalPocket.vue?vue&type=script&lang=js& */ "./resources/js/components/modalPocket.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./modalPocket.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _modalPocket_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/modalPocket.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/modalPocket.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/modalPocket.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./modalPocket.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./modalPocket.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./modalPocket.vue?vue&type=template&id=4d297fb0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/modalPocket.vue?vue&type=template&id=4d297fb0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_modalPocket_vue_vue_type_template_id_4d297fb0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/navFooter.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/navFooter.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navFooter.vue?vue&type=template&id=5cbec03a& */ "./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a&");
/* harmony import */ var _navFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navFooter.vue?vue&type=script&lang=js& */ "./resources/js/components/navFooter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./navFooter.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _navFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/navFooter.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/navFooter.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/navFooter.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./navFooter.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./navFooter.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./navFooter.vue?vue&type=template&id=5cbec03a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navFooter.vue?vue&type=template&id=5cbec03a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navFooter_vue_vue_type_template_id_5cbec03a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/navHeader.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/navHeader.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./navHeader.vue?vue&type=template&id=1a59f856& */ "./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856&");
/* harmony import */ var _navHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./navHeader.vue?vue&type=script&lang=js& */ "./resources/js/components/navHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./navHeader.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _navHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__["render"],
  _navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/navHeader.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/navHeader.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/navHeader.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./navHeader.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./navHeader.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./navHeader.vue?vue&type=template&id=1a59f856& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/navHeader.vue?vue&type=template&id=1a59f856&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_navHeader_vue_vue_type_template_id_1a59f856___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/passwordSetting.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/passwordSetting.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./passwordSetting.vue?vue&type=template&id=99230d8c& */ "./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c&");
/* harmony import */ var _passwordSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./passwordSetting.vue?vue&type=script&lang=js& */ "./resources/js/components/passwordSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./passwordSetting.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _passwordSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__["render"],
  _passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/passwordSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/passwordSetting.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/passwordSetting.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./passwordSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./passwordSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./passwordSetting.vue?vue&type=template&id=99230d8c& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/passwordSetting.vue?vue&type=template&id=99230d8c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_passwordSetting_vue_vue_type_template_id_99230d8c___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/statsAffiliates.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/statsAffiliates.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./statsAffiliates.vue?vue&type=template&id=690d24ca& */ "./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca&");
/* harmony import */ var _statsAffiliates_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./statsAffiliates.vue?vue&type=script&lang=js& */ "./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./statsAffiliates.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _statsAffiliates_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__["render"],
  _statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/statsAffiliates.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./statsAffiliates.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./statsAffiliates.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./statsAffiliates.vue?vue&type=template&id=690d24ca& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsAffiliates.vue?vue&type=template&id=690d24ca&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsAffiliates_vue_vue_type_template_id_690d24ca___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/statsReferrer.vue":
/*!***************************************************!*\
  !*** ./resources/js/components/statsReferrer.vue ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./statsReferrer.vue?vue&type=template&id=e2775aba& */ "./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba&");
/* harmony import */ var _statsReferrer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./statsReferrer.vue?vue&type=script&lang=js& */ "./resources/js/components/statsReferrer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./statsReferrer.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _statsReferrer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__["render"],
  _statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/statsReferrer.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/statsReferrer.vue?vue&type=script&lang=js&":
/*!****************************************************************************!*\
  !*** ./resources/js/components/statsReferrer.vue?vue&type=script&lang=js& ***!
  \****************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./statsReferrer.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&":
/*!*************************************************************************************!*\
  !*** ./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss& ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./statsReferrer.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba&":
/*!**********************************************************************************!*\
  !*** ./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba& ***!
  \**********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./statsReferrer.vue?vue&type=template&id=e2775aba& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/statsReferrer.vue?vue&type=template&id=e2775aba&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_statsReferrer_vue_vue_type_template_id_e2775aba___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/tableHome.vue":
/*!***********************************************!*\
  !*** ./resources/js/components/tableHome.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tableHome.vue?vue&type=template&id=7031d992& */ "./resources/js/components/tableHome.vue?vue&type=template&id=7031d992&");
/* harmony import */ var _tableHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tableHome.vue?vue&type=script&lang=js& */ "./resources/js/components/tableHome.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tableHome.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _tableHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__["render"],
  _tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/tableHome.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/tableHome.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/components/tableHome.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./tableHome.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./tableHome.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/tableHome.vue?vue&type=template&id=7031d992&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/tableHome.vue?vue&type=template&id=7031d992& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./tableHome.vue?vue&type=template&id=7031d992& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/tableHome.vue?vue&type=template&id=7031d992&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_tableHome_vue_vue_type_template_id_7031d992___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/telegramSetting.vue":
/*!*****************************************************!*\
  !*** ./resources/js/components/telegramSetting.vue ***!
  \*****************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./telegramSetting.vue?vue&type=template&id=2fa321d8& */ "./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8&");
/* harmony import */ var _telegramSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./telegramSetting.vue?vue&type=script&lang=js& */ "./resources/js/components/telegramSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./telegramSetting.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _telegramSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/telegramSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/telegramSetting.vue?vue&type=script&lang=js&":
/*!******************************************************************************!*\
  !*** ./resources/js/components/telegramSetting.vue?vue&type=script&lang=js& ***!
  \******************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./telegramSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************************!*\
  !*** ./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./telegramSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8& ***!
  \************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./telegramSetting.vue?vue&type=template&id=2fa321d8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/telegramSetting.vue?vue&type=template&id=2fa321d8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_telegramSetting_vue_vue_type_template_id_2fa321d8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/twoFaSetting.vue":
/*!**************************************************!*\
  !*** ./resources/js/components/twoFaSetting.vue ***!
  \**************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./twoFaSetting.vue?vue&type=template&id=058d0774& */ "./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774&");
/* harmony import */ var _twoFaSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./twoFaSetting.vue?vue&type=script&lang=js& */ "./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./twoFaSetting.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _twoFaSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__["render"],
  _twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/twoFaSetting.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js&":
/*!***************************************************************************!*\
  !*** ./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js& ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./twoFaSetting.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************************!*\
  !*** ./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./twoFaSetting.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774&":
/*!*********************************************************************************!*\
  !*** ./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774& ***!
  \*********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./twoFaSetting.vue?vue&type=template&id=058d0774& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/twoFaSetting.vue?vue&type=template&id=058d0774&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_twoFaSetting_vue_vue_type_template_id_058d0774___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/components/userProfile.vue":
/*!*************************************************!*\
  !*** ./resources/js/components/userProfile.vue ***!
  \*************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./userProfile.vue?vue&type=template&id=ba80613a& */ "./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a&");
/* harmony import */ var _userProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./userProfile.vue?vue&type=script&lang=js& */ "./resources/js/components/userProfile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./userProfile.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _userProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/userProfile.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/userProfile.vue?vue&type=script&lang=js&":
/*!**************************************************************************!*\
  !*** ./resources/js/components/userProfile.vue?vue&type=script&lang=js& ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./userProfile.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************************!*\
  !*** ./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./userProfile.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a& ***!
  \********************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./userProfile.vue?vue&type=template&id=ba80613a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/userProfile.vue?vue&type=template&id=ba80613a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_userProfile_vue_vue_type_template_id_ba80613a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Dashboard.vue":
/*!******************************************!*\
  !*** ./resources/js/pages/Dashboard.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=template&id=82704d4a& */ "./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a&");
/* harmony import */ var _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=script&lang=js& */ "./resources/js/pages/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Dashboard.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Dashboard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Dashboard.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/Dashboard.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************!*\
  !*** ./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a&":
/*!*************************************************************************!*\
  !*** ./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Dashboard.vue?vue&type=template&id=82704d4a& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Dashboard.vue?vue&type=template&id=82704d4a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Dashboard_vue_vue_type_template_id_82704d4a___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Faq.vue":
/*!************************************!*\
  !*** ./resources/js/pages/Faq.vue ***!
  \************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Faq.vue?vue&type=template&id=63c30fc6& */ "./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6&");
/* harmony import */ var _Faq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Faq.vue?vue&type=script&lang=js& */ "./resources/js/pages/Faq.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Faq.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Faq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Faq.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Faq.vue?vue&type=script&lang=js&":
/*!*************************************************************!*\
  !*** ./resources/js/pages/Faq.vue?vue&type=script&lang=js& ***!
  \*************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Faq.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&":
/*!**********************************************************************!*\
  !*** ./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss& ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Faq.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6& ***!
  \*******************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Faq.vue?vue&type=template&id=63c30fc6& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Faq.vue?vue&type=template&id=63c30fc6&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Faq_vue_vue_type_template_id_63c30fc6___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/ForgotPassword.vue":
/*!***********************************************!*\
  !*** ./resources/js/pages/ForgotPassword.vue ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ForgotPassword.vue?vue&type=template&id=4b5a21a7& */ "./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7&");
/* harmony import */ var _ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ForgotPassword.vue?vue&type=script&lang=js& */ "./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ForgotPassword.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/ForgotPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js& ***!
  \************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&":
/*!*********************************************************************************!*\
  !*** ./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7&":
/*!******************************************************************************!*\
  !*** ./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ForgotPassword.vue?vue&type=template&id=4b5a21a7& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ForgotPassword.vue?vue&type=template&id=4b5a21a7&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ForgotPassword_vue_vue_type_template_id_4b5a21a7___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Home.vue":
/*!*************************************!*\
  !*** ./resources/js/pages/Home.vue ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Home.vue?vue&type=template&id=b3c5cf30& */ "./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30&");
/* harmony import */ var _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Home.vue?vue&type=script&lang=js& */ "./resources/js/pages/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Home.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Home.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Home.vue?vue&type=script&lang=js&":
/*!**************************************************************!*\
  !*** ./resources/js/pages/Home.vue?vue&type=script&lang=js& ***!
  \**************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&":
/*!***********************************************************************!*\
  !*** ./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss& ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30&":
/*!********************************************************************!*\
  !*** ./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30& ***!
  \********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Home.vue?vue&type=template&id=b3c5cf30& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Home.vue?vue&type=template&id=b3c5cf30&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Home_vue_vue_type_template_id_b3c5cf30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/HowToPlay.vue":
/*!******************************************!*\
  !*** ./resources/js/pages/HowToPlay.vue ***!
  \******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./HowToPlay.vue?vue&type=template&id=3fa03e06& */ "./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06&");
/* harmony import */ var _HowToPlay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./HowToPlay.vue?vue&type=script&lang=js& */ "./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./HowToPlay.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _HowToPlay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__["render"],
  _HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/HowToPlay.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js&":
/*!*******************************************************************!*\
  !*** ./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js& ***!
  \*******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./HowToPlay.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&":
/*!****************************************************************************!*\
  !*** ./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss& ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./HowToPlay.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06&":
/*!*************************************************************************!*\
  !*** ./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06& ***!
  \*************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./HowToPlay.vue?vue&type=template&id=3fa03e06& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/HowToPlay.vue?vue&type=template&id=3fa03e06&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_HowToPlay_vue_vue_type_template_id_3fa03e06___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Login.vue":
/*!**************************************!*\
  !*** ./resources/js/pages/Login.vue ***!
  \**************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login.vue?vue&type=template&id=3b6adb30& */ "./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30&");
/* harmony import */ var _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login.vue?vue&type=script&lang=js& */ "./resources/js/pages/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Login.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Login.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Login.vue?vue&type=script&lang=js&":
/*!***************************************************************!*\
  !*** ./resources/js/pages/Login.vue?vue&type=script&lang=js& ***!
  \***************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss& ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30&":
/*!*********************************************************************!*\
  !*** ./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30& ***!
  \*********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Login.vue?vue&type=template&id=3b6adb30& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login.vue?vue&type=template&id=3b6adb30&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login_vue_vue_type_template_id_3b6adb30___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Login2fa.vue":
/*!*****************************************!*\
  !*** ./resources/js/pages/Login2fa.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Login2fa.vue?vue&type=template&id=2f136ced& */ "./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced&");
/* harmony import */ var _Login2fa_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Login2fa.vue?vue&type=script&lang=js& */ "./resources/js/pages/Login2fa.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Login2fa.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Login2fa_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Login2fa.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Login2fa.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/pages/Login2fa.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Login2fa.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************!*\
  !*** ./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Login2fa.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Login2fa.vue?vue&type=template&id=2f136ced& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Login2fa.vue?vue&type=template&id=2f136ced&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Login2fa_vue_vue_type_template_id_2f136ced___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/ProvablyFair.vue":
/*!*********************************************!*\
  !*** ./resources/js/pages/ProvablyFair.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProvablyFair.vue?vue&type=template&id=b4f66eb8& */ "./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8&");
/* harmony import */ var _ProvablyFair_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProvablyFair.vue?vue&type=script&lang=js& */ "./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ProvablyFair.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ProvablyFair_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/ProvablyFair.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ProvablyFair.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************!*\
  !*** ./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ProvablyFair.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8&":
/*!****************************************************************************!*\
  !*** ./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ProvablyFair.vue?vue&type=template&id=b4f66eb8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ProvablyFair.vue?vue&type=template&id=b4f66eb8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProvablyFair_vue_vue_type_template_id_b4f66eb8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/ReferFriends.vue":
/*!*********************************************!*\
  !*** ./resources/js/pages/ReferFriends.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ReferFriends.vue?vue&type=template&id=088be704& */ "./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704&");
/* harmony import */ var _ReferFriends_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ReferFriends.vue?vue&type=script&lang=js& */ "./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ReferFriends.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ReferFriends_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/ReferFriends.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ReferFriends.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************!*\
  !*** ./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ReferFriends.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704&":
/*!****************************************************************************!*\
  !*** ./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ReferFriends.vue?vue&type=template&id=088be704& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ReferFriends.vue?vue&type=template&id=088be704&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ReferFriends_vue_vue_type_template_id_088be704___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Register.vue":
/*!*****************************************!*\
  !*** ./resources/js/pages/Register.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Register.vue?vue&type=template&id=364a2fac& */ "./resources/js/pages/Register.vue?vue&type=template&id=364a2fac&");
/* harmony import */ var _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Register.vue?vue&type=script&lang=js& */ "./resources/js/pages/Register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Register.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Register.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Register.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/pages/Register.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************!*\
  !*** ./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Register.vue?vue&type=template&id=364a2fac&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/Register.vue?vue&type=template&id=364a2fac& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Register.vue?vue&type=template&id=364a2fac& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Register.vue?vue&type=template&id=364a2fac&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Register_vue_vue_type_template_id_364a2fac___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/ResetPassword.vue":
/*!**********************************************!*\
  !*** ./resources/js/pages/ResetPassword.vue ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=template&id=31fcdd5e& */ "./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e&");
/* harmony import */ var _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=script&lang=js& */ "./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ResetPassword.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/ResetPassword.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js&":
/*!***********************************************************************!*\
  !*** ./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js& ***!
  \***********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&":
/*!********************************************************************************!*\
  !*** ./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss& ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e&":
/*!*****************************************************************************!*\
  !*** ./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e& ***!
  \*****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./ResetPassword.vue?vue&type=template&id=31fcdd5e& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/ResetPassword.vue?vue&type=template&id=31fcdd5e&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ResetPassword_vue_vue_type_template_id_31fcdd5e___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Settings.vue":
/*!*****************************************!*\
  !*** ./resources/js/pages/Settings.vue ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Settings.vue?vue&type=template&id=882405a8& */ "./resources/js/pages/Settings.vue?vue&type=template&id=882405a8&");
/* harmony import */ var _Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Settings.vue?vue&type=script&lang=js& */ "./resources/js/pages/Settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Settings.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Settings.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Settings.vue?vue&type=script&lang=js&":
/*!******************************************************************!*\
  !*** ./resources/js/pages/Settings.vue?vue&type=script&lang=js& ***!
  \******************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Settings.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&":
/*!***************************************************************************!*\
  !*** ./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss& ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Settings.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Settings.vue?vue&type=template&id=882405a8&":
/*!************************************************************************!*\
  !*** ./resources/js/pages/Settings.vue?vue&type=template&id=882405a8& ***!
  \************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Settings.vue?vue&type=template&id=882405a8& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Settings.vue?vue&type=template&id=882405a8&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Settings_vue_vue_type_template_id_882405a8___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Support.vue":
/*!****************************************!*\
  !*** ./resources/js/pages/Support.vue ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Support.vue?vue&type=template&id=e343ce54& */ "./resources/js/pages/Support.vue?vue&type=template&id=e343ce54&");
/* harmony import */ var _Support_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Support.vue?vue&type=script&lang=js& */ "./resources/js/pages/Support.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Support.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Support_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Support.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Support.vue?vue&type=script&lang=js&":
/*!*****************************************************************!*\
  !*** ./resources/js/pages/Support.vue?vue&type=script&lang=js& ***!
  \*****************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Support.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&":
/*!**************************************************************************!*\
  !*** ./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss& ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Support.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Support.vue?vue&type=template&id=e343ce54&":
/*!***********************************************************************!*\
  !*** ./resources/js/pages/Support.vue?vue&type=template&id=e343ce54& ***!
  \***********************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Support.vue?vue&type=template&id=e343ce54& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Support.vue?vue&type=template&id=e343ce54&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Support_vue_vue_type_template_id_e343ce54___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/pages/Transactions.vue":
/*!*********************************************!*\
  !*** ./resources/js/pages/Transactions.vue ***!
  \*********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Transactions.vue?vue&type=template&id=0f90fe44& */ "./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44&");
/* harmony import */ var _Transactions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Transactions.vue?vue&type=script&lang=js& */ "./resources/js/pages/Transactions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Transactions.vue?vue&type=style&index=0&lang=scss& */ "./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");






/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_3__["default"])(
  _Transactions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__["render"],
  _Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/pages/Transactions.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/pages/Transactions.vue?vue&type=script&lang=js&":
/*!**********************************************************************!*\
  !*** ./resources/js/pages/Transactions.vue?vue&type=script&lang=js& ***!
  \**********************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/babel-loader/lib??ref--4-0!../../../node_modules/vue-loader/lib??vue-loader-options!./Transactions.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&":
/*!*******************************************************************************!*\
  !*** ./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss& ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/style-loader!../../../node_modules/css-loader!../../../node_modules/vue-loader/lib/loaders/stylePostLoader.js!../../../node_modules/postcss-loader/src??ref--7-2!../../../node_modules/sass-loader/dist/cjs.js??ref--7-3!../../../node_modules/vue-loader/lib??vue-loader-options!./Transactions.vue?vue&type=style&index=0&lang=scss& */ "./node_modules/style-loader/index.js!./node_modules/css-loader/index.js!./node_modules/vue-loader/lib/loaders/stylePostLoader.js!./node_modules/postcss-loader/src/index.js?!./node_modules/sass-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=style&index=0&lang=scss&");
/* harmony import */ var _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__);
/* harmony reexport (unknown) */ for(var __WEBPACK_IMPORT_KEY__ in _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__) if(["default"].indexOf(__WEBPACK_IMPORT_KEY__) < 0) (function(key) { __webpack_require__.d(__webpack_exports__, key, function() { return _node_modules_style_loader_index_js_node_modules_css_loader_index_js_node_modules_vue_loader_lib_loaders_stylePostLoader_js_node_modules_postcss_loader_src_index_js_ref_7_2_node_modules_sass_loader_dist_cjs_js_ref_7_3_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_style_index_0_lang_scss___WEBPACK_IMPORTED_MODULE_0__[key]; }) }(__WEBPACK_IMPORT_KEY__));


/***/ }),

/***/ "./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44&":
/*!****************************************************************************!*\
  !*** ./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44& ***!
  \****************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../node_modules/vue-loader/lib??vue-loader-options!./Transactions.vue?vue&type=template&id=0f90fe44& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/pages/Transactions.vue?vue&type=template&id=0f90fe44&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_Transactions_vue_vue_type_template_id_0f90fe44___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ }),

/***/ "./resources/js/routes.js":
/*!********************************!*\
  !*** ./resources/js/routes.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
var routes = [{
  path: '/',
  name: 'Home',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Home */ "./resources/js/pages/Home.vue"));
  }
}, {
  path: '/howto',
  name: 'HowToPlay',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/HowToPlay.vue */ "./resources/js/pages/HowToPlay.vue"));
  }
}, {
  path: '/provablyfair',
  name: 'ProvablyFair',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/ProvablyFair.vue */ "./resources/js/pages/ProvablyFair.vue"));
  }
}, {
  path: '/support',
  name: 'Support',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Support.vue */ "./resources/js/pages/Support.vue"));
  }
}, {
  path: '/faq',
  name: 'Faq',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Faq.vue */ "./resources/js/pages/Faq.vue"));
  }
}, {
  path: '/register',
  name: 'Register',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Register.vue */ "./resources/js/pages/Register.vue"));
  }
}, {
  path: '/login',
  name: 'Login',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Login.vue */ "./resources/js/pages/Login.vue"));
  }
}, {
  path: '/login2fa',
  name: 'Login2fa',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Login2fa.vue */ "./resources/js/pages/Login2fa.vue"));
  }
}, {
  path: '/forgotpassword',
  name: 'Forgot',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/ForgotPassword.vue */ "./resources/js/pages/ForgotPassword.vue"));
  }
}, {
  path: '/reset/:token',
  name: 'Reset',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/ResetPassword.vue */ "./resources/js/pages/ResetPassword.vue"));
  }
}, {
  path: '/dashboard',
  name: 'Dashboard',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Dashboard.vue */ "./resources/js/pages/Dashboard.vue"));
  }
}, {
  path: '/transactions',
  name: 'Transactions',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Transactions.vue */ "./resources/js/pages/Transactions.vue"));
  }
}, {
  path: '/settings',
  name: 'Settings',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/Settings.vue */ "./resources/js/pages/Settings.vue"));
  }
}, {
  path: '/referfriend',
  name: 'ReferFriend',
  component: function component() {
    return Promise.resolve(/*! import() */).then(__webpack_require__.bind(null, /*! ./pages/ReferFriends.vue */ "./resources/js/pages/ReferFriends.vue"));
  }
}];
/* harmony default export */ __webpack_exports__["default"] = (routes);

/***/ }),

/***/ "./resources/js/store/index.js":
/*!*************************************!*\
  !*** ./resources/js/store/index.js ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! vue */ "./node_modules/vue/dist/vue.common.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(vue__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var vuex__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! vuex */ "./node_modules/vuex/dist/vuex.esm.js");
/* harmony import */ var vuex_persistedstate__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! vuex-persistedstate */ "./node_modules/vuex-persistedstate/dist/vuex-persistedstate.es.js");
/* harmony import */ var _app_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../app.js */ "./resources/js/app.js");
/* harmony import */ var _modules_auth__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./modules/auth */ "./resources/js/store/modules/auth.js");
/* harmony import */ var _modules_wallet__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modules/wallet */ "./resources/js/store/modules/wallet.js");
/* harmony import */ var _modules_profile__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./modules/profile */ "./resources/js/store/modules/profile.js");
/* harmony import */ var _modules_transaction__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modules/transaction */ "./resources/js/store/modules/transaction.js");









vue__WEBPACK_IMPORTED_MODULE_1___default.a.use(vuex__WEBPACK_IMPORTED_MODULE_2__["default"]);
/* harmony default export */ __webpack_exports__["default"] = (new vuex__WEBPACK_IMPORTED_MODULE_2__["default"].Store({
  plugins: [Object(vuex_persistedstate__WEBPACK_IMPORTED_MODULE_3__["default"])({
    key: 'HUATBET_STORE_VALUE',
    paths: ['isAuth', 'userData']
  })],
  state: {
    menuVisibility: 'COLLAPSED',
    isAuth: false,
    userData: null,
    websocket_session: '',
    websocket_token: ''
  },
  mutations: {
    SET_MENU_VISIBILITY: function SET_MENU_VISIBILITY(state, value) {
      state.menuVisibility = value;
    },
    SET_IS_AUTH: function SET_IS_AUTH(state, value) {
      state.isAuth = value;
    },
    SET_USER_DATA: function SET_USER_DATA(state, value) {
      if (value == null) return;
      localStorage.setItem('token', value.access_token);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.defaults.headers.common['Authorization'] = 'Bearer ' + value.access_token;
      state.userData = value;
    },
    SET_WEBSOCKET_SESSION: function SET_WEBSOCKET_SESSION(state, value) {
      state.websocket_session = value;
    },
    SET_WEBSOCKET_TOKEN: function SET_WEBSOCKET_TOKEN(state, value) {
      state.websocket_token = value;
    },
    REMOVE_WEBSOCKET: function REMOVE_WEBSOCKET(state, value) {
      state.websocket_session = '';
      state.websocket_token = '';
    }
  },
  getters: {
    /**
     * Get Websocket token
     */
    websocket_token: function websocket_token(state) {
      return state.websocket_token;
    },
    isAuth: function isAuth(state) {
      return state.isAuth;
    }
  },
  modules: {
    auth: _modules_auth__WEBPACK_IMPORTED_MODULE_5__["default"],
    wallet: _modules_wallet__WEBPACK_IMPORTED_MODULE_6__["default"],
    transaction: _modules_transaction__WEBPACK_IMPORTED_MODULE_8__["default"],
    profile: _modules_profile__WEBPACK_IMPORTED_MODULE_7__["default"]
  },
  actions: {
    WEBSOCKET: function WEBSOCKET(_ref) {
      var commit = _ref.commit,
          state = _ref.state;
      if (!state.isAuth) return;
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/websockets/session').then(function (res) {
        commit('SET_WEBSOCKET_SESSION', res.data.data.session_id);
        commit('SET_WEBSOCKET_TOKEN', res.data.data.access_token);
      })["catch"](function (err) {
        console.log(err.response.data);
      });
    }
  }
}));

/***/ }),

/***/ "./resources/js/store/modules/auth.js":
/*!********************************************!*\
  !*** ./resources/js/store/modules/auth.js ***!
  \********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../app.js */ "./resources/js/app.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var auth = {
  namespaced: true,
  state: {
    isError: false,
    errorMessage: '',
    resetPassword: {
      isError: false,
      errorMessage: ''
    }
  },
  getters: {},
  mutations: {
    SET_ERROR: function SET_ERROR(state, value) {
      state.isError = value.isError;
      state.errorMessage = value.errorMessage;
    },
    SET_ERROR_RESET_PASSWORD: function SET_ERROR_RESET_PASSWORD(state, value) {
      state.resetPassword.isError = value.isError;
      state.resetPassword.errorMessage = value.errorMessage;
    }
  },
  actions: {
    LOGIN: function LOGIN(context, credentials) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var data, isOtp;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                data = {
                  'username': credentials.username,
                  'password': credentials.password
                }; // Check if the login using OTP/Secret

                isOtp = false;

                if (credentials.secret != undefined) {
                  data.secret = credentials.secret;
                  isOtp = true;
                }

                _context.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/auth/login', data).then(function (res) {
                  context.commit("SET_IS_AUTH", true, {
                    root: true
                  });
                  context.commit("SET_USER_DATA", res.data.data, {
                    root: true
                  });
                  context.commit('SET_ERROR', {
                    isError: false,
                    errorMessage: ''
                  });
                  _app_js__WEBPACK_IMPORTED_MODULE_2__["router"].push({
                    path: "/"
                  });
                })["catch"](function (err) {
                  // Wrong Password
                  if (err.response.data.message == "user.wrong-password") {
                    var remaining = err.response.data.data.remaining_attempts;
                    context.commit('SET_ERROR', {
                      isError: true,
                      errorMessage: "Password Incorrect. Login Attempts remaining " + (3 - remaining) + "/3"
                    });
                    return;
                  } // Wrong 2FA


                  // Wrong 2FA
                  if (err.response.data.message == "user.wrong-2fa") {
                    var _remaining = err.response.data.data.remaining_attempts;
                    context.commit('SET_ERROR', {
                      isError: true,
                      errorMessage: "OTP Incorrect. Login Attempts remaining " + (3 - _remaining) + "/3"
                    });
                  } // User Locked


                  // User Locked
                  if (err.response.data.message == "user.locked") {
                    var _remaining2 = err.response.data.data.remaining_time;
                    context.commit('SET_ERROR', {
                      isError: true,
                      errorMessage: "Account Locked, maximum login attempts reached. " + _remaining2 + " more minutes to unlock"
                    });
                    return;
                  } // 2FA Required


                  // 2FA Required
                  if (err.response.data.message == "2fa-required") {
                    _app_js__WEBPACK_IMPORTED_MODULE_2__["router"].push({
                      name: "Login2fa",
                      params: {
                        username: credentials.username,
                        password: credentials.password
                      }
                    });
                    return;
                  }
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },
    REGISTER: function REGISTER(_ref, data) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var commit, credentials;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                commit = _ref.commit;
                credentials = {
                  username: data.username,
                  password: data.password,
                  confirm_password: data.confirm_password
                }; // Check Affiliate

                if (data.affiliate != undefined) {
                  credentials.affiliate = data.affiliate;
                }

                _context2.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/auth/register', credentials).then(function (res) {
                  // If success, go to login
                  _app_js__WEBPACK_IMPORTED_MODULE_2__["router"].push({
                    path: "/login"
                  });
                })["catch"](function (err) {
                  // Invalid Affiliate Error
                  if (err.response.data.message == "affiliate.error") {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: "Invalid Affiliate"
                    });
                    return;
                  } // Invalid Username


                  // Invalid Username
                  if (err.response.data.errors.hasOwnProperty("username")) {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: "The username is unavailable, please try again."
                    });
                    return;
                  } // Invalid Password


                  // Invalid Password
                  if (err.response.data.errors.hasOwnProperty("password")) {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: err.response.data.errors.password[0]
                    });
                    return;
                  }
                });

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },
    FORGOT_PASSWORD: function FORGOT_PASSWORD(context, data) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee3() {
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/auth/reset-password', {
                  username: data.username
                }).then(function (res) {
                  _app_js__WEBPACK_IMPORTED_MODULE_2__["router"].push({
                    name: "Login"
                  });
                })["catch"](function (err) {//
                });

              case 1:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3);
      }))();
    },

    /**
     * Logout
     */
    LOGOUT: function LOGOUT(_ref2, data) {
      var commit = _ref2.commit;
      commit("SET_IS_AUTH", false, {
        root: true
      });
      commit("SET_USER_DATA", null, {
        root: true
      });
      commit("REMOVE_WEBSOCKET", null, {
        root: true
      });
      if (data.redirectTo != undefined) window.location = data.redirectTo;
    },

    /**
     * Do Reset Password
     */
    RESET_PASSWORD: function RESET_PASSWORD(_ref3, data) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee4() {
        var commit;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                commit = _ref3.commit;
                _context4.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/auth/reset-telegram', {
                  password: data.password,
                  confirm_password: data.confirm_password,
                  token: data.token
                }).then(function (res) {
                  _app_js__WEBPACK_IMPORTED_MODULE_2__["router"].push({
                    name: "Login"
                  });
                })["catch"](function (err) {
                  commit('SET_ERROR_RESET_PASSWORD', {
                    isError: true,
                    errorMessage: err.response.data.message
                  });
                });

              case 3:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4);
      }))();
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (auth);

/***/ }),

/***/ "./resources/js/store/modules/profile.js":
/*!***********************************************!*\
  !*** ./resources/js/store/modules/profile.js ***!
  \***********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _app_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../app.js */ "./resources/js/app.js");
var _state;

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }



var profile = {
  namespaced: true,
  state: (_state = {
    username: '',
    displayName: '',
    privacy: false,
    timezone: ''
  }, _defineProperty(_state, "privacy", false), _defineProperty(_state, "isLoading", false), _defineProperty(_state, "error", {
    isError: false,
    errorMsg: ''
  }), _state),
  getters: {
    username: function username(state) {
      return state.username;
    },
    displayName: function displayName(state) {
      return state.displayName;
    },
    privacy: function privacy(state) {
      return state.privacy;
    },
    timezone: function timezone(state) {
      return state.timezone;
    }
  },
  mutations: {
    SET_PROFILE: function SET_PROFILE(state, value) {
      if (value.username != undefined) state.username = value.username;
      state.displayName = value.identifier;
      state.privacy = value.hide_identifier;
      if (value.timezone == '') state.timezone = "0";else state.timezone = parseInt(value.timezone);
    },
    SET_LOADING: function SET_LOADING(state, value) {
      state.isLoading = value;
    },
    SET_ERROR: function SET_ERROR(state, value) {
      state.error.isError = value.isError;
      state.error.errorMsg = value.errorMsg;
    },
    RESET_ERROR: function RESET_ERROR(state, _) {
      state.error.isError = false;
      state.error.errorMsg = '';
    }
  },
  actions: {
    /**
     * Get User Profile
     */
    GET_PROFILE: function GET_PROFILE(_ref) {
      var commit = _ref.commit;
      commit('SET_LOADING', true);
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.get('/api/profiles').then(function (res) {
        commit('SET_PROFILE', res.data.data);
        commit('SET_LOADING', false);
      })["catch"](function (err) {
        commit('SET_ERROR', {
          isError: true,
          errorMsg: 'Server Error'
        });
      });
    },

    /**
     * Update User Profile
     */
    UPDATE_PROFILE: function UPDATE_PROFILE(_ref2, credentials) {
      var commit = _ref2.commit;
      // Set Loading & Reset Error
      commit('SET_LOADING', true);
      commit('RESET_ERROR');
      axios__WEBPACK_IMPORTED_MODULE_0___default.a.put('/api/profiles', {
        display_name: credentials.displayName,
        privacy: credentials.privacy,
        timezone: credentials.timezone
      }).then(function (res) {
        commit('SET_PROFILE', {
          identifier: credentials.displayName,
          hide_identifier: credentials.privacy,
          timezone: credentials.timezone
        });
        commit('RESET_ERROR');
        commit('SET_LOADING', false);
      })["catch"](function (err) {
        var errorKey = Object.keys(err.response.data.errors)[0];
        commit('SET_ERROR', {
          isError: true,
          errorMsg: err.response.data.errors[errorKey][0]
        });
        commit('SET_LOADING', false);
      });
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (profile);

/***/ }),

/***/ "./resources/js/store/modules/transaction.js":
/*!***************************************************!*\
  !*** ./resources/js/store/modules/transaction.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../app.js */ "./resources/js/app.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var transaction = {
  namespaced: true,
  state: {
    credit: [],
    debit: [],
    tableOptions: {
      sortable: ["date", "status"],
      filterable: false,
      perPage: 10,
      pagination: {
        show: true,
        chunk: 4,
        dropdown: false,
        nav: "fixed"
      },
      sortIcon: {
        base: "fa",
        is: "fa-sort",
        up: "fa-sort-asc",
        down: "fa-sort-desc"
      },
      showChildRowToggler: true,
      childRowTogglerFirst: true
    },

    /**
     * Debit/Credit Options
     */
    columnDebitCredit: ["date", "transaction", "desc", "amount", "fees", "total", "status"],
    headingDebitCredit: {
      date: "Date",
      transaction: "Transaction #",
      desc: "Description",
      amount: "Amount",
      fees: "Fees",
      total: "Total"
    },

    /**
     * Bets Options
     */
    columnBets: ["date", "transaction", "game", "desc", "amount", "wl", "status"],
    headingBets: {
      date: "Date",
      transaction: "Transaction #",
      game: "Game-Event",
      desc: "Description",
      amount: "Amount",
      wl: "Win/Loss",
      status: "Status"
    },

    /**
     * Refer a Friend Options
     */
    columnFriends: ["date", "transaction #", "desc", "amount", "status"],
    headingFriends: {
      date: "Date",
      transaction: "Transaction #",
      desc: "Description",
      amount: "Amount",
      status: "Status"
    }
  },
  getters: {
    creditTableData: function creditTableData(state) {
      var tableData = [];
      state.credit.forEach(function (item) {
        tableData.push({
          date: item.created_at,
          transaction: item.hash,
          desc: "",
          amount: "".concat(item.add_currency, " ").concat(parseFloat(item.add_amount)),
          fees: "".concat(item.add_currency, " ").concat(parseFloat(item.fee)),
          total: "".concat(item.add_currency, " ").concat(parseFloat(item.add_amount) + parseFloat(item.fee)),
          status: item.status
        });
      });
      return tableData;
    },
    debitTableData: function debitTableData(state) {
      var tableData = [];
      state.debit.forEach(function (item) {
        tableData.push({
          date: item.created_at,
          transaction: item.hash,
          desc: "",
          amount: "".concat(item.deduct_currency, " ").concat(parseFloat(item.deduct_amount)),
          fees: "".concat(item.deduct_currency, " ").concat(parseFloat(item.fee)),
          total: "".concat(item.deduct_currency, " ").concat(parseFloat(item.deduct_amount) + parseFloat(item.fee)),
          status: item.status
        });
      });
      return tableData;
    }
  },
  mutations: {
    SET_CREDIT: function SET_CREDIT(state, value) {
      state.credit = value;
    },
    SET_DEBIT: function SET_DEBIT(state, value) {
      state.debit = value;
    }
  },
  actions: {
    /**
     * Get Credit / deposit
     */
    GET_CREDIT: function GET_CREDIT(_ref, params) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var commit;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                commit = _ref.commit;
                if (params == undefined) params = {};
                params.type = 'deposit';
                _context.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/transactions', {
                  params: params
                }).then(function (res) {
                  commit('SET_CREDIT', res.data.data);
                })["catch"](function (err) {
                  // TODO: Handle Error
                  console.log(err.response);
                });

              case 5:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },

    /**
     * Get Debit / withdraw
     */
    GET_DEBIT: function GET_DEBIT(_ref2, params) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var commit;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                commit = _ref2.commit;
                if (params == undefined) params = {};
                params.type = 'withdraw';
                _context2.next = 5;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/transactions', {
                  params: params
                }).then(function (res) {
                  commit('SET_DEBIT', res.data.data);
                })["catch"](function (err) {
                  // TODO: Handle Error
                  console.log(err.response);
                });

              case 5:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (transaction);

/***/ }),

/***/ "./resources/js/store/modules/wallet.js":
/*!**********************************************!*\
  !*** ./resources/js/store/modules/wallet.js ***!
  \**********************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @babel/runtime/regenerator */ "./node_modules/@babel/runtime/regenerator/index.js");
/* harmony import */ var _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! axios */ "./node_modules/axios/index.js");
/* harmony import */ var axios__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(axios__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _app_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../app.js */ "./resources/js/app.js");


function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { Promise.resolve(value).then(_next, _throw); } }

function _asyncToGenerator(fn) { return function () { var self = this, args = arguments; return new Promise(function (resolve, reject) { var gen = fn.apply(self, args); function _next(value) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value); } function _throw(err) { asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err); } _next(undefined); }); }; }



var wallet = {
  namespaced: true,
  state: {
    wallet: [],
    balances: [],
    minimal_withdraw: 0,
    isError: false,
    errorMessage: '',
    withdrawalFees: null
  },
  getters: {
    /**
     * Get Balance for a specific token name
     */
    balance: function balance(state) {
      return function (token) {
        return state.balances.find(function (item) {
          return item.token === token.toUpperCase();
        });
      };
    },

    /**
     * Get User Balances
     */
    balances: function balances(state) {
      return state.balances;
    },

    /**
     * Get User Wallets
     */
    wallet: function wallet(state) {
      return state.wallet;
    },

    /**
     * Get Withdrawal Fees
     */
    withdrawalFees: function withdrawalFees(state) {
      return state.withdrawalFees;
    }
  },
  mutations: {
    SET_WALLET: function SET_WALLET(state, value) {
      state.wallet = value;
    },
    SET_MINIMAL_WITHDRAW: function SET_MINIMAL_WITHDRAW(state, value) {
      state.minimal_withdraw = value;
    },
    SET_ERROR: function SET_ERROR(state, value) {
      state.isError = value.isError;
      state.errorMessage = value.errorMessage;
    },
    SET_BALANCES: function SET_BALANCES(state, value) {
      state.balances = value;
    },
    SET_WITHDRAWAL_FEES: function SET_WITHDRAWAL_FEES(state, value) {
      state.withdrawalFees = value;
    }
  },
  actions: {
    /**
     * Get Wallet/Deposit Addresses
     */
    GET_DEPOSIT_ADDRESS: function GET_DEPOSIT_ADDRESS(context) {
      if (context.state.wallet.length != 0) {
        return;
      }

      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/wallets/addresses').then(function (res) {
        context.commit('SET_WALLET', res.data.data);
      })["catch"](function (err) {
        context.commit('SET_WALLET', []);
        console.log(err);
      });
    },

    /**
     * Get User Balances
     */
    GET_BALANCES: function GET_BALANCES(_ref) {
      var commit = _ref.commit,
          dispatch = _ref.dispatch;
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get('/api/wallets/balances').then(function (res) {
        var balances = [];
        balances.push(res.data.data);
        commit('SET_BALANCES', balances);
      })["catch"](function (err) {
        /**
         * Auto Logout when 401/Unauthorized
         */
        if (err.response.status == 401) {
          dispatch("auth/LOGOUT", {
            redirectTo: '/'
          }, {
            root: true
          });
        }
      });
    },

    /**
     * Get Minimum Withdrawal
     * For a Given Token Name
     */
    GET_MINIMUM_WITHDRAW: function GET_MINIMUM_WITHDRAW(_ref2, token) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee() {
        var commit;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                commit = _ref2.commit;
                _context.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/site-settings/".concat(token.toLowerCase(), "_withdrawal_min")).then(function (res) {
                  commit('SET_MINIMAL_WITHDRAW', res.data.data.value);
                })["catch"](function (err) {
                  commit('SET_MINIMAL_WITHDRAW', 0);
                  console.log(err.response);
                });

              case 3:
              case "end":
                return _context.stop();
            }
          }
        }, _callee);
      }))();
    },

    /**
     * Do Withdraw Request
     */
    WITHDRAW: function WITHDRAW(_ref3, data) {
      return _asyncToGenerator( /*#__PURE__*/_babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.mark(function _callee2() {
        var commit;
        return _babel_runtime_regenerator__WEBPACK_IMPORTED_MODULE_0___default.a.wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                commit = _ref3.commit;
                _context2.next = 3;
                return axios__WEBPACK_IMPORTED_MODULE_1___default.a.post('/api/wallets/withdraw', {
                  address: data.address,
                  amount: data.amount,
                  currency: data.currency
                }).then(function (res) {
                  console.log(res.data);
                  commit('SET_ERROR', {
                    isError: false,
                    errorMessage: ''
                  });
                  return true;
                })["catch"](function (err) {
                  console.log(err.response.data); // Less than minimum amount

                  // Less than minimum amount
                  if (err.response.data.message == 'less-than.withdrawal-minimum') {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: 'Less Than Withdrawal Minimum'
                    });
                    return false;
                  } // Server Balance Error


                  // Server Balance Error
                  if (err.response.data.message == 'balance.error') {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: 'Server Error'
                    });
                    return false;
                  } // Balance is not enough


                  // Balance is not enough
                  if (err.response.data.message == 'balance.not-enough') {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: 'Balance not Enough'
                    });
                    return false;
                  } // Minimum balance is less than required


                  // Minimum balance is less than required
                  if (err.response.data.message == 'minimum-balance-required') {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: 'Minimum Balance Left is less than'
                    });
                    return false;
                  }

                  if (err.response.data.message = 'The given data was invalid.') {
                    commit('SET_ERROR', {
                      isError: true,
                      errorMessage: 'The given data was invalid.'
                    });
                    return false;
                  }

                  console.log(err.response.data);
                  commit('SET_ERROR', {
                    isError: true,
                    errorMessage: 'Internal Server Error'
                  });
                  return false;
                });

              case 3:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2);
      }))();
    },

    /**
     * Get Withdrawal Fees
     * from site settings
     */
    GET_WITHDRAWAL_FEES: function GET_WITHDRAWAL_FEES(_ref4, tokenName) {
      var commit = _ref4.commit;
      commit('SET_WITHDRAWAL_FEES', null);
      axios__WEBPACK_IMPORTED_MODULE_1___default.a.get("/api/site-settings/".concat(tokenName.toLowerCase(), "_withdrawal_fees")).then(function (res) {
        commit('SET_WITHDRAWAL_FEES', parseFloat(res.data.data.value).toFixed(8));
      })["catch"](function (err) {//
      });
    }
  }
};
/* harmony default export */ __webpack_exports__["default"] = (wallet);

/***/ }),

/***/ "./resources/sass/style.scss":
/*!***********************************!*\
  !*** ./resources/sass/style.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************!*\
  !*** multi ./resources/js/app.js ./resources/sass/style.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/afro/Documents/huatbet/resources/js/app.js */"./resources/js/app.js");
module.exports = __webpack_require__(/*! /home/afro/Documents/huatbet/resources/sass/style.scss */"./resources/sass/style.scss");


/***/ })

},[[0,"/js/manifest","/js/vendor"]]]);