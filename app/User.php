<?php

namespace App;

use App\Models\UserDevice;
use App\Traits\CustomNotifiable;
use App\Traits\TelegramNotifier;
use Laravel\Passport\HasApiTokens;
use App\Models\NotificationSetting;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, TelegramNotifier, CustomNotifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'enabled_2fa' => 'boolean',
        'hide_identifier' => 'boolean',
        'locked_until' => 'datetime',
    ];

    const STATUSES = [
        'active', 'inactive', 'disabled', 'locked'
    ];

    const LANGUAGES = [
        'en', 'id', 'ch'
    ];

    const DEFAULT_CURRENCIES = [
        'USDT', 'ALGO', 'BTC', 'ETH'
    ];

    public function devices()
    {
        return $this->hasMany(UserDevice::class);
    }

    public function notification_settings()
    {
        return $this->hasMany(NotificationSetting::class);
    }
}
