<?php

namespace App\Clients;

class Algorand extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'algorand.token';
        $this->baseUrl = config('algorand.ip');
        $this->email = config('algorand.email');
        $this->password = config('algorand.password');
    }

    /**
     * If needed, we can override the request() function
     */
}
