<?php

namespace App\Clients;

use App\User;

class Affiliate extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'affiliate.token';
        $this->baseUrl = config('affiliate.ip');
        $this->email = config('affiliate.email');
        $this->password = config('affiliate.password');
    }

    /**
     * Get User Affiliate Code
     */
    public function code(User $user): ?array
    {
        $res = $this->request('/api/code', 'post', ['user_id' => $user->id]);

        if ($res->getStatusCode() != 200) return null;

        return json_decode($res->getBody()->getContents(), true);
    }

    /**
     * Get Parent
     */
    public function parent(User $user): ?array
    {
        $res = $this->request('/api/parent', 'post', ['id' => $user->id]);

        if ($res->getStatusCode() != 200) return null;

        return json_decode($res->getBody()->getContents(), true);
    }

    /**
     * Get Ancestor
     */
    public function ancestor(User $user): ?array
    {
        $res = $this->request('/api/ancestors', 'post', ['id' => $user->id]);

        if ($res->getStatusCode() != 200) return null;

        return json_decode($res->getBody()->getContents(), true);
    }

    /**
     * Get Descendants
     */
    public function descendant(User $user): ?array
    {
        $res = $this->request('/api/descendants', 'post', ['id' => $user->id]);

        if ($res->getStatusCode() != 200) return null;

        return json_decode($res->getBody()->getContents(), true);
    }


    /**
     * If needed, we can override the request() function
     */
}
