<?php

namespace App\Clients;

class Btc extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'btc.token';
        $this->baseUrl = config('btc.ip');
        $this->email = config('btc.email');
        $this->password = config('btc.password');
    }

    /**
     * If needed, we can override the request() function
     */
}
