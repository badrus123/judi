<?php

namespace App\Clients;

use Exception;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;

abstract class Gateway
{
    public $baseUrl, $bearerCacheKey;

    public function request(string $endpoint, string $method, array $params = [])
    {
        return $this->sendRequest($endpoint, $method, $params);
    }

    /**
     * Send a request to the gateway
     */
    private function sendRequest(string $endpoint, string $method, array $params)
    {
        if (!Cache::has($this->bearerCacheKey)) {
            $this->auth();
        }

        $client = new \GuzzleHttp\Client(['http_errors' => false]);
        $headers = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json'
        ];

        $body['method'] = $method;
        $body['url'] = $this->baseUrl . $endpoint;
        $body['params'] = $params;
        $body['bearer'] = 'Bearer ' . Cache::get($this->bearerCacheKey);

        $res = $client->post(config('gateway.ip'), [
            'headers' => $headers,
            'body' => json_encode($body),
            'timeout' => 120,
            'connect_timeout' => 120
        ]);

        return $res;
    }

    /**
     * Get Bearer token
     * and store it in a cache
     */
    public function auth()
    {
        $client = new \GuzzleHttp\Client(['http_errors' => false]);

        $headers = [
            'Accept'        => 'application/json',
            'Content-Type'  => 'application/json'
        ];

        $body['method'] = 'post';
        $body['url'] = $this->baseUrl . '/api/login';
        $body['params'] = [
            'email' => $this->email,
            'password' => $this->password
        ];

        $res = $client->post(config('gateway.ip'), [
            'headers' => $headers,
            'body' => json_encode($body),
            'timeout' => 120,
            'connect_timeout' => 120
        ]);

        $json = json_decode($res->getBody()->getContents(), true);

        // If error happens when getting access token
        if ($res->getStatusCode() != 200 || !array_key_exists('access_token', $json)) {
            throw new AuthenticationFailed();
        };

        return Cache::put($this->bearerCacheKey, $json['access_token'], 3600);
    }
}

class AuthenticationFailed extends Exception
{
    public function message()
    {
        $errorMsg = 'Authentication Failed';
        return $errorMsg;
    }
}
