<?php

namespace App\Clients;

class Balance extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'balance.token';
        $this->baseUrl = config('balance.ip');
        $this->email = config('balance.email');
        $this->password = config('balance.password');
    }

    /**
     * If needed, we can override the request() or auth() function
     */
}
