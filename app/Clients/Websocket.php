<?php

namespace App\Clients;

class Websocket extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'websocket.token';
        $this->baseUrl = config('websocket.ip');
        $this->email = config('websocket.email');
        $this->password = config('websocket.password');
    }

    /**
     * If needed, we can override the request() function
     */
}
