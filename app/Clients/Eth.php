<?php

namespace App\Clients;

class Eth extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'eth.token';
        $this->baseUrl = config('eth.ip');
        $this->email = config('eth.email');
        $this->password = config('eth.password');
    }

    /**
     * If needed, we can override the request() function
     */
}
