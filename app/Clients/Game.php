<?php

namespace App\Clients;

class Game extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'game.token';
        $this->baseUrl = config('game.ip');
        $this->email = config('game.email');
        $this->password = config('game.password');
    }

    /**
     * If needed, we can override the request() function
     */
}
