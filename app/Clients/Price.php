<?php

namespace App\Clients;

class Price extends Gateway
{
    public function __construct()
    {
        $this->bearerCacheKey = 'price.token';
        $this->baseUrl = config('price.ip');
        $this->email = config('price.email');
        $this->password = config('price.password');
    }

    public function convert(string $from, string $to): ?array
    {
        $res = $this->request('/api/singlePrice', 'post', [
            'from_token' => $from,
            'to_token' => $to,
        ]);

        if ($res->getStatusCode() != 200) return null;

        return json_decode($res->getBody()->getContents(), true);
    }

    /**
     * If needed, we can override the request() or auth() function
     */
}
