<?php

namespace App\Utilities\TransactionFilter;

use App\Utilities\QueryFilter;
use App\Utilities\FilterContract;

class End_date extends QueryFilter implements FilterContract
{
    public function handle($value = null): void
    {
        if ($value == null || $value == 'null') return;
        $this->query->where('created_at', '<=', $value);
    }
}
