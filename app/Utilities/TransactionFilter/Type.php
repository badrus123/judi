<?php

namespace App\Utilities\TransactionFilter;

use App\Utilities\QueryFilter;
use App\Utilities\FilterContract;

class Type extends QueryFilter implements FilterContract
{
    public function handle($value = null): void
    {
        $this->query->where('type', '=', $value);
    }
}
