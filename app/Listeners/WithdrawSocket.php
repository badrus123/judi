<?php

namespace App\Listeners;

use App\Jobs\WebsocketJob;
use App\Models\Notification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\App;

class WithdrawSocket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        // First Call
        $data = ['message' => '{ "action" : "updateBalances" }', 'user_id' => $event->user->uuid, 'global' => 0];

        WebsocketJob::dispatch($data);

        // Second Call
        App::setLocale($event->user->language);

        // $notification = Notification::create([
        //     'user_id' => $event->user->id,
        //     'message' => trans('withdraw.notification', [
        //         'amount' => $event->transaction->deduct_amount,
        //         'currency' => $event->transaction->deduct_currency
        //     ]),
        //     'type'  => 'withdraw',
        //     'transaction_id' => $event->transaction->id,
        //     'trans_code' => 'notifications.withdraw.success',
        //     'payload' => []
        // ]);

        $notification = $event->user->notify(
            'notifications.withdraw.success',
            [
                'amount' => $event->transaction->deduct_amount,
                'currency' => $event->transaction->deduct_currency
            ],
            $event->transaction,
            trans('withdraw.notification', [
                'amount' => $event->transaction->deduct_amount,
                'currency' => $event->transaction->deduct_currency
            ]),
            'withdraw'
        );

        $format = ['global' => 0, 'user_id' => $event->user->uuid, 'message' => '{ "action" : "addNotification" , "notification": ' . $notification->toJson() . '}'];

        WebsocketJob::dispatch($format);
    }
}
