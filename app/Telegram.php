<?php

namespace App;

/**
 * Usages:
 * 1. Using Class:
 * $telegram = new  Telegram();
 * $telegram->to('chat-id-here')->send('message here');
 * 2. Using App Container:
 * app('telegram')->to('chat-id-here')->send('message here');
 */

class Telegram
{
    private $telegramToken;

    private $chatId;

    private $parseMode = 'markdown';

    public function __construct()
    {
        $this->telegramToken = config('telegram.token');
    }

    /**
     * Set receiver by telegram chat id
     * @param string $chatId
     */
    public function to(string $chatId)
    {
        $this->chatId = $chatId;
        return $this;
    }

    /**
     * Set mode
     * @param string $mode
     */
    public function mode(string $mode)
    {
        $this->parseMode = $mode;
        return $this;
    }

    /**
     * Send a message
     * @param string $message
     */
    public function send(string $message)
    {
        $client = new \GuzzleHttp\Client(['timeout'  => 5.0,]);

        $response =  $client->get('https://api.telegram.org/bot'
            . $this->telegramToken
            . '/sendMessage?chat_id='
            . $this->chatId . '&text='
            . $message
            . '&parse_mode='
            . $this->parseMode);

        return $response;
    }
}
