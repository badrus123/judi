<?php

namespace App\Events;

use App\Models\Transaction;
use App\Models\WithdrawTransaction;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\App;

class WithdrawalSuccess
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var User $user
     */
    public $user;

    /**
     * @var Transaction $transaction
     */
    public $transaction;

    /**
     * @var WithdrawTransaction $withdraw
     */
    public $withdraw;

    /**
     * @var string (Used for telegram message/content)
     */
    public $telegramMessage = '';

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, Transaction $transaction, WithdrawTransaction $withdraw)
    {
        $this->user = $user;
        $this->transaction = $transaction;
        $this->withdraw = $withdraw;

        // Set Telegram Message
        App::setLocale($user->language);
        $this->telegramMessage = trans('withdraw.success', [
            'currency' => $transaction->deduct_currency,
            'amount' => $transaction->deduct_amount,
            'txid' => $transaction->hash,
            'address' => $withdraw->to_address
        ]);
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
