<?php

namespace App\Telegram;

use App\Telegram\Responds\Invalid;
use Throwable;
use Illuminate\Support\Facades\Log;

class TelegramResponser
{
    public function respond(array $request)
    {
        // Get sender/chat ID 
        $senderId = $request['message']['chat']['id'];
        $message = str_replace('/', '', $request['message']['text']);

        // Get the commands
        $commands = explode(" ", $message);
        $mainCommand = ucfirst($commands[0]);
        $normalizedClass = ucfirst($mainCommand);

        $class = 'App\Telegram\Responds\\' . $normalizedClass;

        /**
         * Check if command is registered
         * (We have class to respond the commands)
         * 
         * If we don't have the command,
         * send Invalid reply (inside Invalid class)
         */
        try {
            if (!class_exists($class)) {
                (new Invalid($senderId, $request))->handle($senderId);
                return;
            }
        } catch (Throwable $e) {
            return;
        }

        // If we have the commands
        (new $class($senderId, $request))->handle($senderId);
    }
}
