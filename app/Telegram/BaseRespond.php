<?php

namespace App\Telegram;

abstract class BaseRespond
{
    protected $senderId;

    protected $request;

    public function __construct($senderId, $request)
    {
        $this->senderId = $senderId;
        $this->request = $request;
    }
}
