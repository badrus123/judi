<?php

namespace App\Telegram\Responds;

use App\Telegram\BaseRespond;
use App\User;

class Start extends BaseRespond
{
    /**
     * The start command will have this format
     * /start user-identifier
     * 1. Check if the format is exactly 2 words
     * 2. Check if there's a user that own that identifier
     * 3. Update the user if everything succeed, fallback if anything goes wrong
     */
    public function handle()
    {
        $message = str_replace('/', '', $this->request['message']['text']);

        $commands = explode(" ", $message);

        // If the message doesnt provide exactly 2 words
        if (count($commands) != 2) {
            $this->fallback();
            return;
        }

        $user = User::where('identifier', $commands[1])->first();

        // If the user identifier doesn't exists
        if (!$user) {
            $this->fallback();
            return;
        }

        /**
         * Sync success
         */
        $user->update([
            'telegram_chat_id' => $this->senderId
        ]);

        app('telegram')->to($this->senderId)
            ->send('Welcome! Telegram Successfully Synced with your Account');
    }

    private function fallback()
    {
        app('telegram')->to($this->senderId)
            ->send('Welcome! Use /update [USERNAME] [PASSWORD] to update your profile');
    }
}
