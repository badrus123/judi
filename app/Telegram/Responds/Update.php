<?php

namespace App\Telegram\Responds;

use App\Telegram\BaseRespond;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Throwable;

class Update extends BaseRespond
{
    public function handle()
    {
        $message = str_replace('/', '', $this->request['message']['text']);

        $commands = explode(" ", $message);

        try {
            // If the user doenst provide username and password
            if (count($commands) != 3) {
                app('telegram')->to($this->senderId)
                    ->send('Invalid command. Use /update [USERNAME] [PASSWORD] to update your player ID.');
            }

            // If the username and password doesnt match
            if (!Auth::attempt(['username' => $commands[1], 'password' => $commands[2]])) {
                app('telegram')->to($this->senderId)
                    ->send('Invalid Credentials');
            }

            // Update telegram chat ID
            Auth::user()->update([
                'telegram_chat_id' => $this->senderId
            ]);

            app('telegram')->to($this->senderId)
                ->send('Telegram Successfully Synced with your Account');
        } catch (Throwable $e) {
            Log::error($e->getMessage());
        }
    }
}
