<?php

namespace App\Telegram\Responds;

use App\Telegram\BaseRespond;

class Invalid extends BaseRespond
{
    public function handle()
    {
        app('telegram')->to($this->senderId)
            ->send('Invalid command. Use /update [USERNAME] [PASSWORD] to update your player ID.');
    }
}
