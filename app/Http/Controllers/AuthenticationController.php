<?php

namespace App\Http\Controllers;

use App\Models\PasswordReset;
use App\Services\WalletCreationFailed;
use App\User;
use Throwable;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Str;
use Jenssegers\Agent\Agent;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AuthenticationController extends Controller
{
    use ApiResponser;

    public function register(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|alpha_num|min:6|max:16|unique:users,username',
            'password' => 'required|string|min:6',
            'confirm_password' => 'required|string|min:6|same:password',
            'affiliate' => 'nullable'
        ]);

        $google2fa = new Google2FA();

        try {
            DB::beginTransaction();

            $user = User::create([
                'identifier' => app('unique')->identifier('App\User', 'identifier'),
                'status' => 'active',
                'username' => $attr['username'],
                'password' => bcrypt($attr['password']),
                'secret_2fa' => $google2fa->generateSecretKey(),
                'uuid' => app('unique')->uuid('App\User', 'uuid')
            ]);

            // Call Balance
            $response = app('balance')->request('/api/balances', 'post', ['user_id' => $user->uuid]);

            if ($response->getStatusCode() != 200) {
                DB::rollback();
                return $this->error('client.error', Response::HTTP_BAD_REQUEST);
            }

            /**
             * NODES
             * Done: Algo
             * TODO: Other NODES
             * 
             * If error happens, 
             * catch the WalletCreationFailed
             */
            app('wallet-service')->create($user);

            /**
             * Affiliate
             */

            // If Affiliate Code Exists
            if (array_key_exists('affiliate', $attr)) {
                $response = app('affiliate')->request('/api/create', 'post', ['user_id' => $user->id, 'code' => $attr['affiliate']]);
            } else {
                // If Affiliate Code not Exist, then create user
                $response = app('affiliate')->request('/api/create', 'post', ['user_id' => $user->id]);
            }

            // Check the response of affiliate
            if ($response->getStatusCode() != 201) {
                DB::rollback();
                Log::error($response->getBody()->getContents());
                return $this->error('affiliate.error', Response::HTTP_BAD_REQUEST);
            }

            $data = app('login-service')->token($user);

            DB::commit();

            return $this->success($data, null, Response::HTTP_CREATED);
        } catch (WalletCreationFailed $e) {
            DB::rollback();
            Log::error([
                'AuthenticationController.register',
                $e->message(),
                json_encode($request->all())
            ]);
            return $this->error($e->message(), Response::HTTP_INTERNAL_SERVER_ERROR);
        } catch (Throwable $e) {
            DB::rollback();
            Log::error([
                'AuthenticationController.register',
                $e->getMessage(),
                json_encode($request->all())
            ]);
            return $this->error($e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    public function login(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required',
            'password' => 'required',
            'secret' => 'nullable'
        ]);

        $credentials = request(['username', 'password']);

        /**
         * Unauthenticated / Wrong Password
         */
        if (!Auth::attempt($credentials)) {

            [$message, $data] = app('login-service')->wrongPassword($attr['username']);

            return $this->error($message, Response::HTTP_UNAUTHORIZED, $data);
        }

        $user = Auth::user();

        /**
         * Check if user can be unlocked
         * If the locked_until has been passed,
         * then unlock user
         */
        if (now()->gt($user->locked_until)) {
            $user->update([
                'status' => 'active',
                'locked_until' => null
            ]);
        }

        /**
         * User is Locked
         */
        if ($user->status == 'locked') {
            return $this->error('user.locked', Response::HTTP_UNAUTHORIZED, [
                'remaining_time' => now()->diffInMinutes($user->locked_until)
            ]);
        }

        $google2fa = new Google2FA();

        /**
         * Authenticated WITHOUT 2FA
         */
        if (!$user->enabled_2fa) {
            $data = app('login-service')->token($user, true);
            return $this->success($data);
        }

        // Check if user 2FA required, but doesnt send 2FA
        if ($user->enabled_2fa && !array_key_exists('secret', $attr)) {
            return $this->error('2fa-required', Response::HTTP_UNAUTHORIZED);
        }

        /**
         * Authenticated WITH 2FA
         */
        if (array_key_exists('secret', $attr) && $google2fa->verifyKey($user->secret_2fa, $attr['secret'])) {
            $data = app('login-service')->token($user, true);
            return $this->success($data);
        }

        /**
         * UNAuthenticated / Wrong 2FA
         */
        [$message, $data] = app('login-service')->wrong2fa($user);

        return $this->error($message, Response::HTTP_UNAUTHORIZED, $data);
    }
}
