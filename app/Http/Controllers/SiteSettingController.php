<?php

namespace App\Http\Controllers;

use App\Models\SiteSetting;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class SiteSettingController extends Controller
{
    use ApiResponser;

    public function index()
    {
        $settings = SiteSetting::all();

        return $this->success($settings);
    }

    public function show(Request $request, $key)
    {
        $setting = SiteSetting::where('key', $key)->first();

        if (!$setting) {
            return $this->error('not-found', Response::HTTP_NOT_FOUND);
        }

        return $this->success($setting);
    }
}
