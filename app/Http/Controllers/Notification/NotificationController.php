<?php

namespace App\Http\Controllers\Notification;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    use ApiResponser;

    /**
     * Get All Notifications
     */
    public function index(Request $request)
    {
        $attr = $request->validate([
            'length' => 'nullable|numeric'
        ]);

        $user = Auth::user();

        return $this->success($user->getAllNotifications());
    }

    /**
     * Get All Unread Notifications
     */
    public function unread(Request $request)
    {
        $attr = $request->validate([
            'length' => 'nullable|numeric'
        ]);

        $user = Auth::user();

        return $this->success($user->getUnreadNotifications());
    }

    /**
     * Update All Unread Notifications to be Read
     */
    public function readAll()
    {
        $user = Auth::user();

        $user->readAllNotifications();

        return $this->success($user->getAllNotifications());
    }
}
