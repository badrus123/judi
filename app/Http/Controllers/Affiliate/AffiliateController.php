<?php

namespace App\Http\Controllers\Affiliate;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class AffiliateController extends Controller
{
    use ApiResponser;

    public function code()
    {
        $user = Auth::user();

        $res = app('affiliate')->code($user);

        if (!$res) {
            return $this->error('affiliate.error', Response::HTTP_BAD_REQUEST);
        }

        return $this->success($res);
    }
}
