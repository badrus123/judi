<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\Models\NotificationSetting;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProfileTelegramController extends Controller
{
    use ApiResponser;

    /**
     * Update telegram notification settings
     */
    public function update(Request $request)
    {
        $attr = $request->validate([
            '*.key' => 'required|in:' . implode(',', NotificationSetting::KEYS),
            '*.value' => 'required',
        ]);

        $user = Auth::user();

        foreach ($attr as $setting) {
            NotificationSetting::updateOrCreate([
                'user_id' => $user->id,
                'key' => $setting['key']
            ], [
                'value' => $setting['value']
            ]);
        }

        return $this->success($user->fresh()->notification_settings);
    }

    /**
     * Unlink / disabled
     * telegram notification
     */
    public function destroy()
    {
        $user = Auth::user();

        $user->update([
            'telegram_chat_id' => null
        ]);

        return $this->success(null, 'telegram-disabled');
    }
}
