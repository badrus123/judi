<?php

namespace App\Http\Controllers\Profile;

use App\User;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class Profile2FAController extends Controller
{
    use ApiResponser;

    /**
     * Get 2FA Barcode URL
     */
    public function show()
    {
        $user = Auth::user();

        $google2fa = new Google2FA();

        $qrCodeUrl = $google2fa->getQRCodeUrl(
            config('app.name'),
            $user->username,
            $user->secret_2fa
        );

        return $this->success([
            'qr' => $qrCodeUrl,
            'qr_img' => 'https://chart.googleapis.com/chart?chs=200x200&chld=M%70&cht=qr&chl=' . $qrCodeUrl
        ]);
    }

    /**
     * Link 2FA
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'secret' => 'required'
        ]);

        $user = Auth::user();

        $valid = $this->check2fa($user, $attr['secret']);

        // If secret is invalid
        if (!$valid) {
            return $this->error('wrong-otp', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->update([
            'enabled_2fa' => true
        ]);

        return $this->success(null, '2fa-enabled');
    }

    /**
     * Unlink 2FA
     */
    public function destroy(Request $request)
    {
        $attr = $request->validate([
            'secret' => 'required'
        ]);

        $user = Auth::user();

        $valid = $this->check2fa($user, $attr['secret']);

        // If secret is invalid
        if (!$valid) {
            return $this->error('wrong-otp', Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $user->update([
            'enabled_2fa' => false
        ]);

        return $this->success(null, '2fa-disabled');
    }

    /**
     * Helper function to check 2FA Validity
     * 
     * @param User $user
     * @param string $secret
     * @return bool
     */
    private function check2fa(User $user, string $secret): bool
    {
        $google2fa = new Google2FA();

        $valid = $google2fa->verifyKey($user->secret_2fa, $secret);

        return $valid;
    }
}
