<?php

namespace App\Http\Controllers\Profile;

use App\User;
use App\Models\SiteSetting;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProfileController extends Controller
{
    use ApiResponser;

    public function show()
    {
        $user = Auth::user();

        return $this->success($user);
    }

    public function update(Request $request)
    {
        $user = Auth::user();

        $attr = $request->validate([
            'display_name' => 'nullable|min:5|unique:users,identifier,' . $user->id,
            'privacy' => 'nullable|boolean',
            'timezone' => 'nullable',
            'default_currency' => 'nullable|in:' . implode(',', array_keys(SiteSetting::CURRENCIES)),
            'language' => 'nullable|in:' . implode(',', User::LANGUAGES)
        ]);

        if (array_key_exists('display_name', $attr)) {
            $user->identifier = $attr['display_name'];
        }

        if (array_key_exists('privacy', $attr)) {
            $user->hide_identifier = $attr['privacy'];
        }

        if (array_key_exists('timezone', $attr)) {
            $user->timezone = $attr['timezone'];
        }

        if (array_key_exists('default_currency', $attr)) {
            $user->default_currency = $attr['default_currency'];
        }

        if (array_key_exists('language', $attr)) {
            $user->language = $attr['language'];
        }

        $user->save();

        return $this->success($user);
    }

    public function destroy()
    {
        $user = Auth::user();

        $user->delete();

        return $this->success([]);
    }
}
