<?php

namespace App\Http\Controllers\Websocket;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class WebsocketController extends Controller
{
    use ApiResponser;

    public function session()
    {
        $user = Auth::user();

        $res = app('websocket')->request('/api/createSession', 'post', ['user_id' => $user->uuid]);

        if ($res->getStatusCode() != 200) {
            return $this->error('websocket.error', Response::HTTP_BAD_REQUEST);
        }

        $res = json_decode($res->getBody()->getContents(), true);

        return $this->success($res);
    }
}
