<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Str;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    use ApiResponser;

    /**
     * Send Password Reset URL
     * Via Telegram
     */
    public function request(Request $request)
    {
        $attr = $request->validate([
            'username' => 'required|exists:users,username'
        ]);

        $user = User::where('username', $attr['username'])->first();

        $token = str_replace('-', '', Str::uuid()->toString());

        PasswordReset::create([
            'user_id' => $user->id,
            'token' => $token
        ]);

        App::setLocale($user->language);

        $user->telegram(trans(
            'user.auth.reset-password',
            [
                'url' => config('app.url_password_reset') . '/' . $token
            ]
        ));

        return $this->success('reset-password-sent');
    }

    /**
     * Update Password via Telegram Request
     */
    public function telegramUpdate(Request $request)
    {
        $attr = $request->validate([
            'token' => 'required|exists:password_resets,token',
            'password' => 'required|string|min:6',
            'confirm_password' => 'required|string|min:6|same:password',
        ]);

        $reset = PasswordReset::where('token', $attr['token'])->first();

        $user = $reset->user;

        $user->update([
            'password' => bcrypt($attr['password'])
        ]);

        $reset = PasswordReset::where('token', $attr['token'])->delete();

        App::setLocale($user->language);

        $user->telegram(trans(
            'user.auth.password-resetted'
        ));

        return $this->success('password-reset');
    }

    /**
     * Update Password via Settings
     */
    public function settingUpdate(Request $request)
    {
        $attr = $request->validate([
            'password' => 'required|string|min:6',
            'confirm_password' => 'required|string|min:6|same:password',
        ]);

        $user = Auth::user();

        $user->update([
            'password' => bcrypt($attr['password'])
        ]);

        App::setLocale($user->language);

        $user->telegram(trans(
            'user.auth.password-resetted'
        ));

        return $this->success('password-reset');
    }
}
