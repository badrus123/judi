<?php

namespace App\Http\Controllers\Telegram;

use App\Http\Controllers\Controller;
use App\Telegram\TelegramResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class TelegramCallback extends Controller
{
    public function index(Request $request)
    {
        $t = new TelegramResponser();

        // Log::info($request->all());
        $t->respond($request->all());
    }
}
