<?php

namespace App\Http\Controllers\Transaction;

use App\Http\Controllers\Controller;
use App\Models\Transaction;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    use ApiResponser;

    public function index(Request $request)
    {
        $attr = $request->validate([
            'type' => 'required|in:' . implode(',', Transaction::TYPES),
            'start_date' => 'nullable|date_format:Y-m-d',
            'end_date' => 'nullable|date_format:Y-m-d',
            'status' => 'nullable|in:' . implode(',', Transaction::STATUSES)
        ]);

        $user = Auth::user();
        $transactions = Transaction::query()->where('user_id', $user->id);

        // Filtering logic
        $transactions->filterBy($request->all());

        return $this->success($transactions->get());
    }
}
