<?php

namespace App\Http\Controllers\Wallet;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WalletController extends Controller
{
    use ApiResponser;

    public function address()
    {
        $user = Auth::user();

        $addresses = app('wallet-service')->create($user);

        return $this->success($addresses);
    }

    public function balance()
    {
        $user = Auth::user();

        // Get All User Balances
        $response = app('balance')->request('/api/balances', 'post', [
            'user_id' => $user->uuid
        ]);
        $response = json_decode($response->getBody()->getContents(), true);

        $balances = $response['user']['balances'];

        // Format the output
        $formattedOutput = [];
        foreach ($balances as $balance) {

            $balanceNominal = $balance['balance'];

            // If it ALGO, then get the actual. Override the value of balanceNominal
            if ($balance['token']['symbol'] == 'ALGO') {
                $res = app('algorand')->request('/api/getEndUserBalance', 'post', [
                    'end_user_id' => $user->uuid
                ]);
                $res = json_decode($res->getBody()->getContents());
                $balanceNominal  = $res->algo_balance;
            }

            /**
             * Do the conversion
             * based on the user's default currency 
             */
            $balanceConverted = 0;
            $daily = 0;

            $converter = app('price')->convert(
                strtoupper($balance['token']['symbol']),
                strtoupper($user->default_currency)
            );

            // Convert if there's response
            if ($converter != null) {
                $balanceConverted = (float) number_format(
                    $converter['price'] * $balanceNominal,
                    8,
                    ".",
                    ""
                );

                $daily = (float) number_format(
                    $converter['daily'],
                    2,
                    ".",
                    ""
                );
            }

            $formattedOutput = array_merge($formattedOutput, [
                'token' => $balance['token']['symbol'],
                'name' => $balance['token']['name'],
                'balance' => (float) $balanceNominal,
                'converted' => $balanceConverted,
                'converted_to' => $user->default_currency,
                'converted_from' => $balance['token']['symbol'],
                'daily' => $daily
            ]);
        }

        return $this->success($formattedOutput);
    }
}
