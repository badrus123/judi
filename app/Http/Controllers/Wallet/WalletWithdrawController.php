<?php

namespace App\Http\Controllers\Wallet;

use App\Events\WithdrawalSuccess;
use App\Models\SiteSetting;
use App\Models\Transaction;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use App\Models\WithdrawTransaction;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\Response;

class WalletWithdrawController extends Controller
{
    use ApiResponser;

    /**
     * Logic for Withdrawal
     * 1. Check Minimum Withdrawal
     * 2. Check the balance (reject if belance is not enough)
     * 3. Send the request to Nodes Microservices (i.e. Algo)
     * 4. Update / deduct the balance from Balance microservices
     * 5. If everything succeed, save the records (Transaction & WithdrawTransaction)
     */
    public function store(Request $request)
    {
        $attr = $request->validate([
            'address' => 'required|string|max:255',
            'amount' => 'required|numeric|min:0|not_in:0',
            'currency' => 'required|in:' . implode(',', array_keys(SiteSetting::CURRENCIES))
        ]);

        $user = Auth::user();

        /**
         * 1. Check Minimum Withdrawal
         */
        $minimum = SiteSetting::where('key', strtolower($attr['currency']) . '_withdrawal_min')->first();
        if (!$minimum) $minimum = 0;
        else
            $minimum = (float) $minimum->value;

        if ($attr['amount'] < $minimum) {
            return $this->error('less-than.withdrawal-minimum', Response::HTTP_BAD_REQUEST);
        }

        /**
         * 2. Check the balance from Balance MS
         */
        $balanceResponse = app('balance')->request('/api/balance', 'post', [
            'user_id' => $user->uuid,
            'currency' => $attr['currency']
        ]);
        $json = json_decode($balanceResponse->getBody()->getContents(), true);

        // Check the balance response
        if ($balanceResponse->getStatusCode() != 200 || !array_key_exists('balance', $json)) {
            return $this->error('balance.error', Response::HTTP_BAD_REQUEST);
        }

        $balance = (float) $json['balance'];

        // Check the balance
        if ($balance < $attr['amount']) {
            return $this->error('balance.not-enough', Response::HTTP_BAD_REQUEST);
        }


        /**
         * Check minimum balance left
         * Only applicable for ALGO
         * Get the minimum balance from the site setting
         */
        if ($attr['currency'] == 'ALGO') {
            $minBalance = SiteSetting::where('key', 'algo_min_balance_left')->first();

            // Get Min Balance
            if (!$minBalance)
                $minBalance = (float) 0;
            else
                $minBalance = (float) $minBalance->value;

            if (($balance - $attr['amount']) < $minBalance) {
                return $this->error('minimum-balance-required', Response::HTTP_BAD_REQUEST);
                // return response()->json(array('message' => 'Withdrawal amount exceed the minimum balance required (1.1 ALGO)'), 422);
            }
        }

        /**
         * 3. Send the request to Nodes Microservices
         */
        $class = SiteSetting::CURRENCIES[$attr['currency']];
        $nodeResponse = (new $class())->request('/api/sendUserAlgo', 'post', [
            'to' => $attr['address'],
            'amount' => $attr['amount'],
            'uuid' => $user->uuid
        ]);

        if ($nodeResponse->getStatusCode() != 200) {
            return $this->error('nodes.error', Response::HTTP_BAD_REQUEST);
        }
        $jsonRes = json_decode($nodeResponse->getBody()->getContents(), true);

        // Log::info('3');
        // Log::info($jsonRes);

        $txid = $jsonRes['hash'];
        $fee = $jsonRes['fee'] / 1000000;

        $total = $attr['amount'] + $fee;

        /**
         * 4. Update / deduct the balance from Balance microservices
         */
        $balanceResponse = app('balance')->request('/api/deduct', 'post', [
            'user_id' => $user->uuid,
            'amount' => $total,
            'token' => $attr['currency']
        ]);

        // Log::info([
        //     'user_id' => $user->uuid,
        //     'amount' => $total,
        //     'token' => $attr['currency']
        // ]);

        if ($balanceResponse->getStatusCode() != 201) {
            // Log::info('4a');
            // Log::info($balanceResponse->getBody()->getContents());
            return $this->error('balance-deduction.error', Response::HTTP_BAD_REQUEST);
        }

        // Log::info('4b');
        // Log::info($balanceResponse->getBody()->getContents());

        /**
         * 5. Save the records (Transaction & WithdrawTransaction)
         */
        $tx = Transaction::create([
            'hash' => uniqid(),
            'user_id' => $user->id,
            'type' => 'withdraw',
            'status' => 'completed',
            'add_amount' => '0',
            'add_currency' => $attr['currency'],
            'fee' => $fee,
            'deduct_amount' => $attr['amount'],
            'deduct_currency' => $attr['currency']
        ]);

        $withdraw = WithdrawTransaction::create([
            'transaction_id' => $tx->id,
            'to_address' => $attr['address'],
            'block_hash' => $txid
        ]);

        event(new WithdrawalSuccess($user, $tx, $withdraw));

        return $this->success(null, 'withdraw-success');
    }
}
