<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Crypt;

use App\User;


use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class HomeController extends Controller
{
    public function index(){
        return view('index');
    }
}
