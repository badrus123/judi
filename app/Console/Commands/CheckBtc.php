<?php

namespace App\Console\Commands;

use App\User;
use App\Jobs\WebsocketJob;
use App\Models\Transaction;
use App\Models\Notification;
use Illuminate\Console\Command;
use App\Models\DepositTransaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckBtc extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:btc';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Incoming BTC Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try {
            $transactions = Transaction::with('deposit')
                ->where('status', 'pending')
                ->where('type', 'deposit')->orderBy('created_at')->get();
            $hashes = [];
            foreach ($transactions as $transaction) {
                $hashes[] = $transaction->deposit()->first()->hash;
            }

            $created_at = null;
            $latest_transaction = Transaction::with('deposit')
                ->where('type', 'deposit')
                ->where('add_currency', 'BTC')
                ->orderBy('created_at', 'desc')->first();
            if ($latest_transaction != null) {
                $created_at = $latest_transaction->created_at->format('Y-m-d H:i:s');
            }

            // Call Status Scanning
            if (sizeof($hashes) > 0) {

                $response = app('btc')->request('/api/scanByHash', 'post', ['hashes' => $hashes]);

                if ($response->getStatusCode() == 200) {
                    $response = json_decode($response->getBody(), true);
                    if (isset($response)) {
                        foreach ($response as $ms_tx) {
                            if (isset($ms_tx['to_user']['end_user_id'])) {
                                $user = User::where('uuid', $ms_tx['to_user']['end_user_id'])->first();
                                $deposit = DepositTransaction::where('block_hash', $ms_tx['hash'])->first();
                                $transaction = Transaction::where('id', $deposit->transaction_id)->first();

                                if ($ms_tx['status'] == 'completed') {

                                    $update_balance = app('balance')->request('/api/add', 'post', [
                                        'user_id' => $user->uuid,
                                        'amount' => $ms_tx['amount'],
                                        'token' => $ms_tx['token']
                                    ]);

                                    if ($update_balance->getStatusCode() == 201) {
                                        $transaction->update(['status' => $ms_tx['status']]);
                                        $data = [
                                            'global' => 0,
                                            'message' => '{ "action" : "updateBalances" }', 'user_id' => $user->uuid
                                        ];

                                        // Dispath Update Balance Websocket
                                        WebsocketJob::dispatch($data);

                                        // Notify User Telegram
                                        $user->telegram("You have received " . $ms_tx['amount'] . " " . $ms_tx['token'] . "\n\nTransaction ID: " . $transaction->hash . "\nAmount: " . $ms_tx['amount'] . " " . $ms_tx['token'] . "\nFrom: " . $ms_tx['from_address'] . "\nTransaction Hash: " . $ms_tx['hash']);

                                        $data = [
                                            'transaction_hash' => $transaction->hash,
                                            'old_user_id' => $user->id,
                                            'type' => 'deposit',
                                            'transaction_id' => $transaction->id,
                                            'message' => "Your deposit request of " . $ms_tx['amount'] . " " . $ms_tx['token'] . " has been submitted.",
                                            'user_id' => $user->uuid
                                        ];

                                        // Create Notifications
                                        $notification = $user->notify(
                                            'notifications.deposit.success',
                                            [
                                                'amount' => $ms_tx['amount'],
                                                'currency' => $ms_tx['token']
                                            ],
                                            $transaction,
                                            $data['message'],
                                            'deposit'
                                        );

                                        // $notification = Notification::create([
                                        //     'user_id' => $data['old_user_id'],
                                        //     'message' => $data['message'],
                                        //     'type'  => $data['type'],
                                        //     'transaction_id' => $data['transaction_id']
                                        // ]);

                                        $format = ['global' => 0, 'user_id' => $data['user_id'], 'message' => '{ "action" : "addNotification" , "notification": ' . $notification->toJson() . '}'];

                                        // Dispath Add Notification Websocket
                                        WebsocketJob::dispatch($format);
                                    }
                                } else {
                                    $transaction->update(['status' => $ms_tx['status']]);
                                }
                            }
                        }
                    }
                }
            }

            // Call Transactions more than latest
            $response = app('btc')->request('/api/scanByDate', 'post', ['datetime' => $created_at]);
            // $response = callBtcNode(env('BTC_IP') . '/api/scanByDate', 'post', ['datetime' => $created_at]);
            if ($response->getStatusCode() == 200) {
                $response = json_decode($response->getBody(), true);
                if (isset($response)) {
                    foreach ($response as $ms_tx) {
                        if (isset($ms_tx['to_user']['end_user_id'])) {
                            $user = User::where('uuid', $ms_tx['to_user']['end_user_id'])->first();
                            $deposit = DepositTransaction::where('block_hash', $ms_tx['hash'])->first();
                            if ($deposit == null) {
                                $transaction = Transaction::create([
                                    'hash' => uniqid(),
                                    'user_id' => $user->id,
                                    'type' => 'deposit',
                                    'status' => 'completed',
                                    'add_amount' => $ms_tx['amount'],
                                    'add_currency' => $ms_tx['token'],
                                    'deduct_amount' => '0',
                                    'deduct_currency' => $ms_tx['token'],
                                    'created_at' => $ms_tx['created_at'],
                                    'updated_at' => $ms_tx['updated_at']
                                ]);

                                DepositTransaction::create([
                                    'transaction_id' => $transaction->id,
                                    'block_hash' => $ms_tx['hash'],
                                    'from_address' => $ms_tx['from_address']
                                ]);
                            } else {
                                $transaction = Transaction::where('id', $deposit->transaction_id)->first();
                            }

                            if ($ms_tx['status'] == 'completed' && $deposit == null) {
                                $update_balance = app('balance')->request('/api/add', 'post', ['user_id' => $user->uuid, 'amount' => $ms_tx['amount'], 'token' => $ms_tx['token']]);
                                if ($update_balance->getStatusCode() == 201) {
                                    $transaction->update(['status' => $ms_tx['status']]);

                                    // Dispath Update Balance Websocket
                                    $data = [
                                        'global' => 0,
                                        'message' => '{ "action" : "updateBalances" }',
                                        'user_id' => $user->uuid
                                    ];
                                    WebsocketJob::dispatch($data);

                                    // Notify via Telegram
                                    $user->telegram("You have received " . $ms_tx['amount'] . " " . $ms_tx['token'] . "\n\nTransaction ID: " . $transaction->hash . "\nAmount: " . $ms_tx['amount'] . " " . $ms_tx['token'] . "\nFrom: " . $ms_tx['from_address'] . "\nTransaction Hash: " . $ms_tx['hash']);

                                    $data = [
                                        'transaction_hash' => $transaction->hash,
                                        'old_user_id' => $user->id,
                                        'type' => 'deposit',
                                        'transaction_id' => $transaction->id,
                                        'message' => "Your deposit request of " . $ms_tx['amount'] . " " . $ms_tx['token'] . " has been submitted.",
                                        'user_id' => $user->uuid
                                    ];

                                    // Create Notifications
                                    $notification = $user->notify(
                                        'notifications.deposit.success',
                                        [
                                            'amount' => $ms_tx['amount'],
                                            'currency' => $ms_tx['token']
                                        ],
                                        $transaction,
                                        $data['message'],
                                        'deposit'
                                    );

                                    // $notification = Notification::create([
                                    //     'user_id' => $data['old_user_id'],
                                    //     'message' => $data['message'],
                                    //     'type'  => $data['type'],
                                    //     'transaction_id' => $data['transaction_id']
                                    // ]);

                                    // Dispath Add Notification Websocket
                                    $format = [
                                        'global' => 0,
                                        'user_id' => $data['user_id'],
                                        'message' => '{ "action" : "addNotification" , "notification": ' . $notification->toJson() . '}'
                                    ];
                                    WebsocketJob::dispatch($format);
                                }
                            } else {
                                $transaction->update(['status' => $ms_tx['status']]);
                            }
                        }
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
            dd($e->getMessage());
        }
    }
}
