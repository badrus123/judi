<?php

namespace App\Console\Commands;

use App\User;
use App\Models\Transaction;
use Illuminate\Console\Command;
use App\Models\DepositTransaction;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class CheckAlgo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:algo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Incoming ALGO Transactions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::beginTransaction();
        try {

            // Transactions with deposit pending
            $transactions = Transaction::where('status', 'pending')
                ->where('type', 'deposit')->get();

            $hashes = [];
            $processed = [];
            foreach ($transactions as $transaction) {
                $hashes[] = $transaction->hash;
            }

            // Get transactions based on hashes
            $response = app('algorand')->request('/api/scan', 'post', ['hashes' => $hashes]);

            // Exit if status not success
            if ($response->getStatusCode() != 200) return 0;

            $response = json_decode($response->getBody(), true);

            // Exit if response doesnt exists
            if (!isset($response)) return 0;

            foreach ($response as $tx) {

                if (isset($tx['to_user']['end_user_id'])) {
                    if ($tx['type'] == 'optin' || $tx['type'] == 'optout' || $tx['type'] == 'app_call') {
                        Log::info('picked up app call: ' . $tx['type']);

                        // We have to deduct the fees from the user as these transactions are picked up after the fact
                        $fees = $tx['fee'];
                        $end_user = User::where("uuid", $tx['to_user']['end_user_id'])->first();

                        $update_balance = app('balance')->request('/api/deduct', 'post', [
                            'user_id' => $end_user->uuid,
                            'amount' => $fees,
                            'token' => 'ALGO'
                        ]);

                        if ($update_balance->getStatusCode() == 201) {
                            Log::info('updated balance for app calls');
                            $this->comment("status 201");
                            if (!$tx['processed']) {
                                $processed[] = $tx['hash'];
                            }
                        }
                    } else {
                        // Type Deposit
                        $end_user = User::where("uuid", $tx['to_user']['end_user_id'])->first();

                        $transaction = Transaction::create([
                            'hash' => uniqid(),
                            'user_id' => $end_user->id,
                            'type' => 'deposit',
                            'status' => 'completed',
                            'add_amount' => $tx['amount'],
                            'add_currency' => 'ALGO',
                            'deduct_amount' => '0',
                            'deduct_currency' => 'ALGO',
                            'created_at' => $tx['created_at'],
                            'updated_at' => $tx['updated_at']
                        ]);

                        $deposit = DepositTransaction::create([
                            'transaction_id' => $transaction->id,
                            'block_hash' => $tx['hash'],
                            'from_address' => $tx['from']
                        ]);

                        if ($tx['status'] == 'completed') {
                            //UPDATE BALANCE OF USER IN BALANCE MS
                            $update_balance = app('balance')->request('/api/incoming', 'post', [
                                'hash' => $tx['hash'],
                                'from_address' => $tx['from'],
                                'to_user_id' => $tx['to_user']['end_user_id'],
                                'amount' => $tx['amount'],
                                'token' => 'ALGO'
                            ]);

                            if ($update_balance->getStatusCode() == 201) {
                                $this->comment("status 201");
                                if (!$tx['processed']) {
                                    $processed[] = $tx['hash'];
                                }

                                // Notify via Telegram
                                $end_user->telegram("You have received " . $tx['amount'] . " ALGO\n\nTransaction ID: " . $tx['hash'] . "\nAmount: " . $tx['amount'] . " ALGO\nFrom: " . $tx['from'] . "\nTransaction Hash: " . $tx['hash'], 'wallet');
                            }
                        }
                    }
                }
            }
            DB::commit();
            if (sizeof($processed) > 0) {
                $response = app('algorand')->request('/api/processed', 'post', ['hashes' => $processed]);
                // $response = callAlgoNode(env('ALGORAND_IP') . '/api/processed', 'post', ['hashes' => $processed]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error($e);
        }
    }
}
