<?php

namespace App;

use Illuminate\Support\Facades\Auth;

class CheckNotificationSetting
{
    /**
     * Check if authenticated user
     * can/have the setting
     * 
     * @param string $key setting
     * @return bool
     */
    public function check(string $keySetting): bool
    {
        $user = Auth::user();

        $setting =  $user->notification_settings()->where('key', $keySetting)->first();

        if (!$setting) return false;

        return $setting->value;
    }
}
