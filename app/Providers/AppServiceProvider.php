<?php

namespace App\Providers;

use App\CheckNotificationSetting;
use App\Clients\Affiliate;
use App\Clients\Algorand;
use App\Clients\Balance;
use App\Clients\Btc;
use App\Clients\Eth;
use App\Clients\Game;
use App\Clients\Price;
use App\Clients\Websocket;
use App\Services\LoginService;
use App\Services\WalletService;
use App\Telegram;
use App\Unique;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Clients / Nodes / MS
         */
        $this->app->singleton('balance', function ($app) {
            return new Balance();
        });

        $this->app->singleton('game', function ($app) {
            return new Game();
        });

        $this->app->singleton('affiliate', function ($app) {
            return new Affiliate();
        });

        $this->app->singleton('algorand', function ($app) {
            return new Algorand();
        });

        $this->app->singleton('btc', function ($app) {
            return new Btc();
        });

        $this->app->singleton('eth', function ($app) {
            return new Eth();
        });

        $this->app->singleton('websocket', function ($app) {
            return new Websocket();
        });

        $this->app->singleton('price', function ($app) {
            return new Price();
        });

        $this->app->singleton('unique', function ($app) {
            return new Unique();
        });

        /**
         * Services
         */
        $this->app->singleton('login-service', function ($app) {
            return new LoginService();
        });

        $this->app->singleton('wallet-service', function ($app) {
            return new WalletService();
        });

        $this->app->singleton('telegram', function ($app) {
            return new Telegram();
        });

        $this->app->singleton('notification-setting', function ($app) {
            return new CheckNotificationSetting();
        });
    }
}
