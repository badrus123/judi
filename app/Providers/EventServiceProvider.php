<?php

namespace App\Providers;

use App\Events\WithdrawalSuccess;
use App\Listeners\TelegramNotification;
use App\Listeners\WithdrawSocket;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],

        /**
         * When withdraw succeed
         * 1. Send Web Socket (Job/Queue)
         * 2. Notify via Telegram
         */
        WithdrawalSuccess::class => [
            WithdrawSocket::class,
            TelegramNotification::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
