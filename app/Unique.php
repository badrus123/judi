<?php

namespace App;

use Illuminate\Support\Str;

class Unique
{
    public function uuid($model, $column)
    {
        $exists = true;
        $uuid = '';

        while ($exists) {
            $uuid = Str::uuid();
            $exists = $model::where($column, $uuid)->exists();
        }

        return $uuid->toString();
    }

    public function identifier($model, $column)
    {
        $exists = true;
        $identifier = '';

        while ($exists) {
            $identifier = uniqid();

            $exists = $model::where($column, $identifier)->exists();
        }

        return $identifier;
    }
}
