<?php

namespace App\Models;

use App\Utilities\QueryFilterBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Transaction extends Model
{
    protected $table = 'transactions';

    protected $guarded = [];

    protected $appends = ['timezone', 'created_at_timezone'];

    const TYPES = [
        'withdraw', 'deposit'
    ];

    const STATUSES = [
        'pending', 'completed'
    ];

    public function getTimezoneAttribute()
    {
        if (Auth::user()) {
            return Auth::user()->timezone;
        } else {
            return null;
        }
    }

    public function getCreatedAtTimezoneAttribute()
    {
        if (Auth::user()) {
            return $this->created_at->addHours(Auth::user()->timezone)->format('Y-m-d H:i:s');
        } else {
            return $this->created_at;
        }
    }

    /**
     * Filter class
     */
    public function scopeFilterBy($query, $filters)
    {
        $namespace = 'App\Utilities\TransactionFilter';
        $filter = new QueryFilterBuilder($query, $filters, $namespace);

        return $filter->apply();
    }
}
