<?php

namespace App\Models;

use App\Clients\Algorand;
use Illuminate\Database\Eloquent\Model;

class SiteSetting extends Model
{
    protected $table = 'site_settings';

    protected $fillable = [
        'key', 'value'
    ];

    /**
     * Available Currencies,
     * that hold clients
     */
    const CURRENCIES = [
        'ETH' => null,
        'ALGO' => Algorand::class
    ];

    const KEYS = [
        'currencies', 
        'algo_withdrawal_min',
        'algo_min_balance_left',
        'algo_withdrawal_fees'
    ];
}
