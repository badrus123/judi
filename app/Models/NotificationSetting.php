<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotificationSetting extends Model
{
    protected $table = 'notification_settings';

    protected $guarded = [];

    const KEYS = [
        'wallet', 'affiliate', 'promotion', 'login-attempt'
    ];

    protected $casts = [
        'value' => 'boolean',
    ];
}
