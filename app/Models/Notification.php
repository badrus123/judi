<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notifications';

    protected $guarded = [];

    public function transMessage()
    {
        if ($this->trans_code == null) {
            return;
        }

        $payload = $this->payload;

        return trans($this->trans_code, $payload);
    }

    public function getPayloadAttribute($value)
    {
        return json_decode($value, true);
    }
}
