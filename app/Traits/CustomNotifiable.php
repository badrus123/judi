<?php

namespace App\Traits;

use App\Models\Notification;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\App;

trait CustomNotifiable
{
    /**
     * Save/Create new notification
     * for users
     * 
     * @return Notification
     */
    public function notify(string $transCode, array $payload, Transaction $transaction, string $message = '', string $type = ''): Notification
    {
        $notification = Notification::create([
            'user_id' => $this->id,
            'message' => $message,
            'type'  => $type,
            'transaction_id' => $transaction->id,
            'trans_code' => $transCode,
            'payload' => json_encode($payload)
        ]);

        return $notification;
    }

    /**
     * Get Unread Notifications
     * 
     * @return Collection
     */
    public function getUnreadNotifications(): Collection
    {
        App::setLocale($this->language);

        $notif = $this->allNotifications()
            ->where('read', false)->get();

        foreach ($notif as $item) {
            $item->trans_message = $item->transMessage();
        }

        return $notif;
    }

    /**
     * Get Read Notifications
     * 
     * @return Collection
     */
    public function getReadNotifications(): Collection
    {
        App::setLocale($this->language);

        $notif = Notification::where('user_id', $this->id)
            ->where('read', true)->get();

        foreach ($notif as $item) {
            $item->trans_message = $item->transMessage();
        }

        return $notif;
    }

    /**
     * Get All Notifications
     * 
     * @return Collection
     */
    public function getAllNotifications(): Collection
    {
        App::setLocale($this->language);

        $notif = Notification::where('user_id', $this->id)->get();

        foreach ($notif as $item) {
            $item->trans_message = $item->transMessage();
        }

        return $notif;
    }

    /**
     * Set All Unread Notifications to be read
     * 
     * @return void
     */
    public function readAllNotifications()
    {
        Notification::where('user_id', $this->id)
            ->where('read', false)->update(['read' => 1]);
    }

    /**
     * Return Query Builder for Notifications
     * 
     * @return QueryBuilder
     */
    public function allNotifications(string $order = 'desc')
    {
        return Notification::where('user_id', $this->id)
            ->orderBy('created_at', $order);
    }
}
