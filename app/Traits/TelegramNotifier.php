<?php

namespace App\Traits;

use App\Models\NotificationSetting;

trait TelegramNotifier
{
    public function telegram(string $message, string $key = '')
    {
        // Check if user has telegram
        if ($this->telegram_chat_id == null) return;

        // If the key doesnt specified, send user notification
        if ($key == '') $this->send($message);

        // Check if user has telegram setting notification
        $setting = NotificationSetting::where('user_id', $this->id)
            ->where('key', $key)->first();

        // If setting doesnt exists, then send message to user
        if (!$setting) return $this->send($message);

        if ($setting->value == true) return $this->send($message);

        return null;
    }

    public function send($message)
    {
        $response = app('telegram')
            ->to($this->telegram_chat_id)
            ->send($message);

        return $response;
    }
}
