<?php

namespace App\Services;

use App\User;
use Exception;
use App\Models\SiteSetting;
use Illuminate\Support\Facades\Log;

class WalletService
{
    /**
     * Create wallets for users,
     * based on available currencies
     * from the site settings
     * 
     * @param User $user
     * @return array $addresses // Hold Nodes Address
     */
    public function create(User $user): array
    {
        // Get available currencies this platform offer
        $currencies = SiteSetting::where('key', 'currencies')->first()->value;
        $currencies = json_decode($currencies, true);

        // Hold Nodes Addresses
        $addresses = [];

        // Loop for each currencies to register the user
        foreach ($currencies as $currency) {

            // Get Class Based on SiteSetting const
            $class = SiteSetting::CURRENCIES[$currency];

            // Continue if class null/unavailable
            if ($class == null) continue;

            /**
             * Do request to create the wallet/nodes
             */
            $response = (new $class())->request('/api/create', 'post', ['user_id' => $user->uuid]);
            $decodedResponse = json_decode($response->getBody()->getContents(), true);

            // Throw WalletCreationFailed Error when creation failed
            if ($response->getStatusCode() != 201 || !array_key_exists('address', $decodedResponse)) {
                Log::error([
                    'WalletService.create',
                    json_encode($decodedResponse),
                    $class
                ]);
                throw new WalletCreationFailed();
            }

            $addresses = array_merge($addresses, [
                $currency => $decodedResponse['address']
            ]);
        }

        return $addresses;
    }
}

class WalletCreationFailed extends Exception
{
    public function message()
    {
        $errorMsg = 'wallet-creation.failed';
        return $errorMsg;
    }
}
