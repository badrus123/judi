<?php

namespace App\Services;

use App\User;
use App\Models\UserDevice;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\App;

class LoginService
{
    /**
     * When password wrong
     * do these:
     * - Show x tries remaining before account is locked
     * - Max of 3 wrong password attempts
     * - Locked for 60 minutes
     * - Notify on telegram for account lock
     * - Account will remain locked even after resetting password
     * 
     * @param string $username
     * @return array [$message, $data]
     */
    public function wrongPassword(String $username): array
    {
        $user = User::where('username', $username)->first();

        // User with username not found
        if (!$user) {
            return ['user.not-found', []];
        }

        // Check if user is being locked
        if ($user->status == 'locked') {
            return [
                'user.locked', [
                    'remaining_time' => now()->diffInMinutes($user->locked_until)
                ]
            ];
        }

        $attempts = $user->login_attempts;

        // Check if attempts more than 3 times
        if ($attempts >= 3) {
            // Locked account for 60 minutes
            // Notify Telegram
            $user->update([
                'status' => 'locked',
                'locked_until' => now()->addMinutes(60)
            ]);

            App::setLocale($user->language);

            $user->telegram(trans('user.auth.user-locked', ['minutes' => 60]), 'login-attempt');

            return ['user.locked', [
                'remaining_time' => now()->diffInMinutes($user->locked_until)
            ]];
        }

        $user->update([
            'login_attempts' => $attempts + 1
        ]);

        return ['user.wrong-password', [
            'remaining_attempts' => (3 - $attempts)
        ]];
    }

    /**
     * When wrong 2FA
     * do these:
     * - Show x tries remaining before account is locked
     * - Max of 3 wrong password attempts
     * - Locked for 60 minutes
     * - Notify on telegram for account lock
     * - Account will remain locked even after resetting password
     */
    public function wrong2fa(User $user)
    {
        $attempts = $user->login_attempts;

        // Check if attempts more than 3 attempts
        if ($attempts >= 3) {
            // Locked account for 60 minutes
            // Notify Telegram
            $user->update([
                'status' => 'locked',
                'locked_until' => now()->addMinutes(60)
            ]);

            App::setLocale($user->language);

            $user->telegram(trans('user.auth.user-locked', ['minutes' => 60]), 'login-attempt');

            return ['user.locked', [
                'remaining_time' => now()->diffInMinutes($user->locked_until)
            ]];
        }

        $user->update([
            'login_attempts' => $attempts + 1
        ]);

        return ['user.wrong-2fa', [
            'remaining_attempts' => (3 - $attempts)
        ]];
    }

    /**
     * Get token to login
     * and check new device (optional)
     * 
     * @param User $user
     * @param bool $checkNewDevice
     * @return array
     */
    public function token(User $user, bool $checkNewDevice = false): array
    {
        $token = $user->createToken('Personal Access Token');
        $token->token->update(['expires_at' => now()->addHours(1)]);

        $user->update([
            'login_attempts' => 0,
        ]);

        // If check new device is true
        if ($checkNewDevice) $this->checkNewDevice($user);

        return [
            'access_token' => $token->accessToken,
            'expires_at' => $token->token->expires_at->format('Y-m-d H:i')
        ];
    }

    /**
     * If the user login
     * with new device, then:
     * 1. Inform via Telegram
     * 2. Add record to user_devices
     * 
     * @param User $user
     * @return bool $deviceExists
     */
    private function checkNewDevice(User $user): bool
    {
        $agent = new Agent();

        $device = $agent->device();
        $platform = $agent->platform();
        $ip = $this->getIp();

        $deviceExists = UserDevice::where('user_id', $user->id)
            ->where('device', $device)
            ->where('platform', $platform)
            ->exists();

        // If device exists, then do nothing
        if ($deviceExists) return $deviceExists;

        /**
         * If device doesn't exists,
         * then Inform via telegram and 
         * add record to user_devices
         */

        UserDevice::create([
            'user_id' => $user->id,
            'platform' => $platform,
            'device' => $device,
            'ip_address' => $ip
        ]);

        App::setLocale($user->language);

        $user->telegram(trans('user.auth.new-device', [
            'device' => $device,
            'platform' => $platform,
            'ip' => $ip
        ]));

        return $deviceExists;
    }

    /**
     * Helper function to get IP Address
     * 
     * @return mixed|string $ipAddress
     */
    private function getIp()
    {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }
        return request()->ip();
    }
}
