## Requirements
- PHP 7.2
- Laravel 6.0
- Node 14.16.1
- NPM 6.14.12

## Installation Steps
```
composer install
php artisan migrate
php artisan passport:install
php artisan db:seed // @required For Site Setting
npm install
npm run dev
```

## Tests
```composer test``` or ```vendor/bin/phpunit```