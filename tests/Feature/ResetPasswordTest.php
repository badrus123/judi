<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ResetPasswordTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_reset_password_via_setting()
    {
        $this->login();

        $res = $this->postJson('/api/password/reset-setting', [
            'password' => 'newsecret',
            'confirm_password' => 'newsecret',
        ], $this->header)->assertStatus(200);
        $res = $this->convertResponseToArray($res);
    }

    /** @test */
    public function user_can_reset_password_via_telegram()
    {
        $this->login();

        /**
         * Request password reset via telegram
         */
        $res = $this->postJson('/api/auth/reset-password', [
            'username' => Auth::user()->username
        ], $this->header)->assertStatus(200);

        /** 
         * Get the token
         * and reset password
         */
        $token = DB::table('password_resets')->first()->token;

        $res = $this->postJson('/api/auth/reset-telegram', [
            'token' => $token,
            'password' => 'newsecret',
            'confirm_password' => 'newsecret',
        ])->assertStatus(200);
    }
}
