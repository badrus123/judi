<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_get_their_own_profile()
    {
        $this->login();

        $response = $this->getJson('/api/profiles', $this->header)
            ->assertStatus(200);
    }

    /** @test */
    public function user_can_update_their_own_profile()
    {
        $this->login();

        $res = $this->putJson('/api/profiles', [
            'display_name' => 'John Doe',
            'privacy' => true,
            'timezone' => '+8'
        ], $this->header)
            ->assertStatus(200);
        $res = $this->convertResponseToArray($res);

        $this->assertEquals('John Doe', $res['data']['identifier']);
        $this->assertEquals(true, $res['data']['hide_identifier']);
        $this->assertEquals('+8', $res['data']['timezone']);
    }

    /** @test */
    public function user_can_get_2fa()
    {
        $this->login();

        $res = $this->getJson('/api/profiles/2fa', $this->header)
            ->assertStatus(200);
        $res = $this->convertResponseToArray($res);

        $this->assertArrayHasKey('qr', $res['data']);
        $this->assertArrayHasKey('qr_img', $res['data']);
    }
}
