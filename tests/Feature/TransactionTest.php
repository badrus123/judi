<?php

namespace Tests\Feature;

use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class TransactionTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_get_transactions()
    {
        $this->login();

        /**
         * Deposit
         */
        factory(Transaction::class)->create([
            'user_id' => Auth::user()->id,
            'type' => 'deposit'
        ]);

        $res = $this->getJson('/api/transactions?type=deposit', $this->header)
            ->assertStatus(200);
        $res = $this->convertResponseToArray($res);

        $this->assertEquals(1, count($res['data']));

        /**
         * Withdraw
         */
        factory(Transaction::class)->create([
            'user_id' => Auth::user()->id,
            'type' => 'withdraw'
        ]);

        $res = $this->getJson('/api/transactions?type=withdraw', $this->header)
            ->assertStatus(200);
        $res = $this->convertResponseToArray($res);

        $this->assertEquals(1, count($res['data']));
    }
}
