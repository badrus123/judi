<?php

namespace Tests\Feature;

use Mockery;
use Tests\TestCase;
use App\Clients\Balance;
use App\User;
use GuzzleHttp\Psr7\Stream;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function user_can_register()
    {
        $this->mockBalance();
        $this->mockAffiliate();
        $this->mockWalletService();

        $res = $this->postJson('/api/auth/register', [
            'username' => 'JohnDoe123',
            'password' => 'secret',
            'confirm_password' => 'secret',
        ])->assertStatus(201);

        $res = $this->convertResponseToArray($res);

        $this->assertArrayHasKey('access_token', $res['data']);
    }

    /** @test */
    public function user_can_login()
    {
        $user = factory(User::class)->create();

        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret',
            'confirm_password' => 'secret',
        ])->assertStatus(200);

        $res = $this->convertResponseToArray($res);

        $this->assertArrayHasKey('access_token', $res['data']);
    }

    /** @test */
    public function user_locked_cannot_login()
    {
        $user = factory(User::class)->create([
            'status' => 'locked',
            'locked_until' => now()->addHours(1)
        ]);

        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret',
            'confirm_password' => 'secret',
        ])->assertStatus(401);

        $res = $this->convertResponseToArray($res);

        $this->assertEquals('user.locked', $res['message']);

        /**
         * User Unlocked
         * When locked until has passed
         */

        $user->update([
            'locked_until' => now()->addHours(-2)
        ]);

        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret',
            'confirm_password' => 'secret',
        ])->assertStatus(200);

        $res = $this->convertResponseToArray($res);

        $this->assertArrayHasKey('access_token', $res['data']);
    }

    /** @test */
    public function will_notify_telegram_when_user_locked()
    {
        $user = factory(User::class)->create();

        /**
         * First attempt
         */
        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret1',
            'confirm_password' => 'secret1',
        ])->assertStatus(401);

        $res = $this->convertResponseToArray($res);

        $this->assertEquals('user.wrong-password', $res['message']);
        $this->assertEquals(3, $res['data']['remaining_attempts']);

        /**
         * Second attempt
         */
        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret1',
            'confirm_password' => 'secret1',
        ])->assertStatus(401);

        $res = $this->convertResponseToArray($res);

        $this->assertEquals('user.wrong-password', $res['message']);
        $this->assertEquals(2, $res['data']['remaining_attempts']);

         /**
         * Third attempt
         */
        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret1',
            'confirm_password' => 'secret1',
        ])->assertStatus(401);

        $res = $this->convertResponseToArray($res);

        $this->assertEquals('user.wrong-password', $res['message']);
        $this->assertEquals(1, $res['data']['remaining_attempts']);

         /**
         * Fourth attempt - Locked the user
         */
        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret1',
            'confirm_password' => 'secret1',
        ])->assertStatus(401);

        $res = $this->convertResponseToArray($res);

        $this->assertEquals('user.locked', $res['message']);
       
    }
}
