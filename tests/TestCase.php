<?php

namespace Tests;

use App\Clients\Affiliate;
use App\Services\WalletService;
use App\Telegram;
use App\User;
use Mockery;
use GuzzleHttp\Psr7\Response;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public $header = [];

    public function setUp(): void
    {
        parent::setUp();
        \Artisan::call('passport:install');
    }

    public function mockBalance()
    {
        $this->instance('balance', Mockery::mock(Balance::class, function ($mock) {
            $response = new Response(200, ['Content-Type' => 'application/json'], '');

            $mock->shouldReceive('request')->once()->andReturn($response);
        }));
    }

    public function mockAffiliate()
    {
        $this->instance('affiliate', Mockery::mock(Affiliate::class, function ($mock) {
            $response = new Response(201, ['Content-Type' => 'application/json'], '');

            $mock->shouldReceive('request')->once()->andReturn($response);
        }));
    }

    public function mockTelegram()
    {
        $this->instance('telegram', Mockery::mock(Telegram::class, function ($mock) {
            $response = new Response(200, ['Content-Type' => 'application/json'], '');

            $mock->shouldReceive('to')->once()->andReturn($response);
        }));
    }

    public function mockWalletService()
    {
        $this->instance('wallet-service', Mockery::mock(WalletService::class, function ($mock) {
            $mock->shouldReceive('create')->once()->andReturn([]);
        }));
    }

    public function login()
    {
        $user = factory(User::class)->create();

        $this->actingAs($user);

        $res = $this->postJson('/api/auth/login', [
            'username' => $user->username,
            'password' => 'secret',
            'confirm_password' => 'secret',
        ])->assertStatus(200);
        $res = $this->convertResponseToArray($res);

        $this->header = [
            'Authorization' => 'Bearer ' . $res['data']['access_token']
        ];
    }

    /**
     * Helper function to convert JsonResponse into an array
     * 
     * @param JsonResponse $response
     * @return array 
     */
    public function convertResponseToArray($response): array
    {
        return json_decode(json_encode($response->getData()), true);
    }
}
