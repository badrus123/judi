<?php

return [
    'success' => "Your Withdrawal request of :amount :currency has been submitted.\n\nTransaction ID: :txid\nWithdrawal Address: :address",
    'notification' => 'Your withdrawal request of :amount :currency has been submitted'
];
