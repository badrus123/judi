<?php

return [
    'auth' => [
        'user-locked' => 'Your Account has been locked. Try again in :minutes minutes',
        'new-device' => 'You logged in with new device :device :platform, with IP :ip',
        'reset-password' => "Click Link below to Reset Your Password \n\n :url",
        'password-resetted' => 'Your Password has been changed'
    ]
];
