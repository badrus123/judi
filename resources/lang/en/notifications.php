<?php

return [
    'deposit' => [
        'success' => 'Your deposit request of :amount :currency has been submitted.'
    ],
    'withdraw' => [
        'success' => 'Your withdrawal request of :amount :currency has been submitted'
    ]
];
