const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('./pages/Home')
    },
    {
        path: '/howto',
        name: 'HowToPlay',
        component: () => import('./pages/HowToPlay.vue')
    },
    {
        path: '/provablyfair',
        name: 'ProvablyFair',
        component: () => import('./pages/ProvablyFair.vue')
    },
    {
        path: '/support',
        name: 'Support',
        component: () => import('./pages/Support.vue')
    },
    {
        path: '/faq',
        name: 'Faq',
        component: () => import('./pages/Faq.vue')
    },
    {
        path: '/register',
        name: 'Register',
        component: () => import('./pages/Register.vue')
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('./pages/Login.vue')
    },
    {
        path: '/login2fa',
        name: 'Login2fa',
        component: () => import('./pages/Login2fa.vue')
    },
    {
        path: '/forgotpassword',
        name: 'Forgot',
        component: () => import('./pages/ForgotPassword.vue')
    },
    {
        path: '/reset/:token',
        name: 'Reset',
        component: () => import('./pages/ResetPassword.vue')
    },
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: () => import('./pages/Dashboard.vue')
    },
    {
        path: '/transactions',
        name: 'Transactions',
        component: () => import('./pages/Transactions.vue')
    },
    {
        path: '/settings',
        name: 'Settings',
        component: () => import('./pages/Settings.vue')
    },
    {
        path: '/referfriend',
        name: 'ReferFriend',
        component: () => import('./pages/ReferFriends.vue')
    }

]

export default routes;
