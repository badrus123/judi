import axios from "axios";
import Vue from "vue";
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
import { router } from "../app.js"
import auth from './modules/auth'
import wallet from './modules/wallet'
import profile from './modules/profile'
import transaction from './modules/transaction'

Vue.use(Vuex)

export default new Vuex.Store({
    plugins: [createPersistedState(
        {
            key: 'HUATBET_STORE_VALUE',
            paths: ['isAuth', 'userData']
        }
    )],
    state: {
        menuVisibility: 'COLLAPSED',
        isAuth: false,
        userData: null,
        websocket_session: '',
        websocket_token: ''
    },
    mutations: {
        SET_MENU_VISIBILITY(state, value) {
            state.menuVisibility = value
        },
        SET_IS_AUTH(state, value) {
            state.isAuth = value
        },
        SET_USER_DATA(state, value) {
            if (value == null) return;

            localStorage.setItem('token', value.access_token);

            axios.defaults.headers.common['Authorization'] = 'Bearer ' + value.access_token;

            state.userData = value
        },
        SET_WEBSOCKET_SESSION(state, value) {
            state.websocket_session = value;
        },
        SET_WEBSOCKET_TOKEN(state, value) {
            state.websocket_token = value;
        },
        REMOVE_WEBSOCKET(state, value) {
            state.websocket_session = '';
            state.websocket_token = '';
        }
    },
    getters: {
        /**
         * Get Websocket token
         */
        websocket_token: state => {
            return state.websocket_token
        },
        isAuth: state => {
            return state.isAuth
        }
    },
    modules: {
        auth,
        wallet,
        transaction,
        profile
    },
    actions: {
        WEBSOCKET({ commit, state }) {
            if (!state.isAuth) return;

            axios.get('/api/websockets/session')
                .then((res) => {
                    commit('SET_WEBSOCKET_SESSION', res.data.data.session_id);
                    commit('SET_WEBSOCKET_TOKEN', res.data.data.access_token);
                })
                .catch((err) => {
                    console.log(err.response.data)
                })
        }
    }
})
