import axios from "axios";
import { router } from "../../app.js"

const wallet = {
    namespaced: true,
    state: {
        wallet: [],
        balances: [],
        minimal_withdraw: 0,
        isError: false,
        errorMessage: '',
        withdrawalFees: null
    },
    getters: {
        /**
         * Get Balance for a specific token name
         */
        balance: (state) => (token) => {
            return state.balances.find(item => item.token === token.toUpperCase())
        },
        /**
         * Get User Balances
         */
        balances: state => {
            return state.balances
        },
        /**
         * Get User Wallets
         */
        wallet: state => {
            return state.wallet
        },
        /**
         * Get Withdrawal Fees
         */
        withdrawalFees: state => {
            return state.withdrawalFees
        }
    },
    mutations: {
        SET_WALLET(state, value) {
            state.wallet = value;
        },
        SET_MINIMAL_WITHDRAW(state, value) {
            state.minimal_withdraw = value;
        },
        SET_ERROR(state, value) {
            state.isError = value.isError;
            state.errorMessage = value.errorMessage;
        },
        SET_BALANCES(state, value) {
            state.balances = value;
        },
        SET_WITHDRAWAL_FEES(state, value) {
            state.withdrawalFees = value;
        }
    },
    actions: {

        /**
         * Get Wallet/Deposit Addresses
         */
        GET_DEPOSIT_ADDRESS(context) {
            if (context.state.wallet.length != 0) {
                return;
            }

            axios.get('/api/wallets/addresses')
                .then((res) => {
                    context.commit('SET_WALLET', res.data.data);
                })
                .catch((err) => {
                    context.commit('SET_WALLET', []);
                    console.log(err)
                })
        },
        /**
         * Get User Balances
         */
        GET_BALANCES({ commit, dispatch }) {
            axios.get('/api/wallets/balances')
                .then((res) => {
                    let balances = [];
                    balances.push(res.data.data)
                    commit('SET_BALANCES', balances);
                })
                .catch((err) => {
                    /**
                     * Auto Logout when 401/Unauthorized
                     */
                    if (err.response.status == 401) {
                        dispatch("auth/LOGOUT", {
                            redirectTo: '/'
                        }, { root: true });
                    }
                });
        },
        /**
         * Get Minimum Withdrawal
         * For a Given Token Name
         */
        async GET_MINIMUM_WITHDRAW({ commit }, token) {
            await axios.get(`/api/site-settings/${token.toLowerCase()}_withdrawal_min`)
                .then((res) => {
                    commit('SET_MINIMAL_WITHDRAW', res.data.data.value);
                })
                .catch((err) => {
                    commit('SET_MINIMAL_WITHDRAW', 0);
                    console.log(err.response);
                })
        },
        /**
         * Do Withdraw Request
         */
        async WITHDRAW({ commit }, data) {
            await axios.post('/api/wallets/withdraw', {
                address: data.address,
                amount: data.amount,
                currency: data.currency
            })
                .then((res) => {
                    console.log(res.data);
                    commit('SET_ERROR', {
                        isError: false,
                        errorMessage: ''
                    })
                    return true;
                })
                .catch((err) => {
                    console.log(err.response.data);

                    // Less than minimum amount
                    if (err.response.data.message == 'less-than.withdrawal-minimum') {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: 'Less Than Withdrawal Minimum'
                        })
                        return false;
                    }

                    // Server Balance Error
                    if (err.response.data.message == 'balance.error') {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: 'Server Error'
                        })
                        return false;
                    }

                    // Balance is not enough
                    if (err.response.data.message == 'balance.not-enough') {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: 'Balance not Enough'
                        })
                        return false;
                    }

                    // Minimum balance is less than required
                    if (err.response.data.message == 'minimum-balance-required') {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: 'Minimum Balance Left is less than'
                        })
                        return false;
                    }

                    if (err.response.data.message = 'The given data was invalid.') {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: 'The given data was invalid.'
                        })
                        return false;
                    }

                    console.log(err.response.data)
                    commit('SET_ERROR', {
                        isError: true,
                        errorMessage: 'Internal Server Error'
                    })

                    return false;
                })
        },
        /**
         * Get Withdrawal Fees
         * from site settings
         */
        GET_WITHDRAWAL_FEES({ commit }, tokenName) {

            commit('SET_WITHDRAWAL_FEES', null);

            axios.get(`/api/site-settings/${tokenName.toLowerCase()}_withdrawal_fees`)
                .then((res) => {
                    commit('SET_WITHDRAWAL_FEES', parseFloat(res.data.data.value).toFixed(8))
                })
                .catch((err) => {
                    //
                })
        }
    }
}

export default wallet