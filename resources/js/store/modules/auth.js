import axios from "axios";
import { router } from "../../app.js"

const auth = {
    namespaced: true,
    state: {
        isError: false,
        errorMessage: '',
        resetPassword: {
            isError: false,
            errorMessage: ''
        }
    },
    getters: {
    },
    mutations: {
        SET_ERROR(state, value) {
            state.isError = value.isError;
            state.errorMessage = value.errorMessage;
        },
        SET_ERROR_RESET_PASSWORD(state, value) {
            state.resetPassword.isError = value.isError
            state.resetPassword.errorMessage = value.errorMessage
        }
    },
    actions: {
        async LOGIN(context, credentials) {

            let data = {
                'username': credentials.username,
                'password': credentials.password
            };

            // Check if the login using OTP/Secret
            let isOtp = false;
            if (credentials.secret != undefined) {
                data.secret = credentials.secret
                isOtp = true;
            }

            await axios.post('/api/auth/login', data)
                .then((res) => {
                    context.commit("SET_IS_AUTH", true, { root: true });
                    context.commit("SET_USER_DATA", res.data.data, { root: true });
                    context.commit('SET_ERROR', {
                        isError: false,
                        errorMessage: ''
                    })
                    router.push({ path: `/` });
                })
                .catch((err) => {

                    // Wrong Password
                    if (err.response.data.message == "user.wrong-password") {
                        let remaining = err.response.data.data.remaining_attempts;

                        context.commit('SET_ERROR', {
                            isError: true,
                            errorMessage: "Password Incorrect. Login Attempts remaining " +
                                (3 - remaining) +
                                "/3"
                        })
                        return;
                    }

                    // Wrong 2FA
                    if (err.response.data.message == "user.wrong-2fa") {
                        let remaining = err.response.data.data.remaining_attempts;
                        context.commit('SET_ERROR', {
                            isError: true,
                            errorMessage: "OTP Incorrect. Login Attempts remaining " +
                                (3 - remaining) +
                                "/3"
                        })
                    }

                    // User Locked
                    if (err.response.data.message == "user.locked") {
                        let remaining = err.response.data.data.remaining_time;

                        context.commit('SET_ERROR', {
                            isError: true,
                            errorMessage: "Account Locked, maximum login attempts reached. " +
                                remaining +
                                " more minutes to unlock"
                        })
                        return;
                    }

                    // 2FA Required
                    if (err.response.data.message == "2fa-required") {
                        router.push({
                            name: `Login2fa`,
                            params: {
                                username: credentials.username,
                                password: credentials.password,
                            },
                        });
                        return;
                    }

                })
        },
        async REGISTER({ commit }, data) {

            let credentials = {
                username: data.username,
                password: data.password,
                confirm_password: data.confirm_password,
            };

            // Check Affiliate
            if (data.affiliate != undefined) {
                credentials.affiliate = data.affiliate;
            }

            await axios.post('/api/auth/register', credentials)
                .then((res) => {
                    // If success, go to login
                    router.push({ path: `/login` });
                })
                .catch((err) => {
                    // Invalid Affiliate Error
                    if (err.response.data.message == "affiliate.error") {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: "Invalid Affiliate"
                        })
                        return;
                    }

                    // Invalid Username
                    if (err.response.data.errors.hasOwnProperty("username")) {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: "The username is unavailable, please try again."
                        })
                        return;
                    }

                    // Invalid Password
                    if (err.response.data.errors.hasOwnProperty("password")) {
                        commit('SET_ERROR', {
                            isError: true,
                            errorMessage: err.response.data.errors.password[0]
                        })
                        return;
                    }

                })
        },
        async FORGOT_PASSWORD(context, data) {
            axios.post('/api/auth/reset-password', {
                username: data.username
            })
                .then((res) => {
                    router.push({
                        name: `Login`,
                    });
                })
                .catch((err) => {
                    //
                })
        },
        /**
         * Logout
         */
        LOGOUT({ commit }, data) {
            commit("SET_IS_AUTH", false, { root: true });
            commit("SET_USER_DATA", null, { root: true });
            commit("REMOVE_WEBSOCKET", null, { root: true });

            if (data.redirectTo != undefined)
                window.location = data.redirectTo;
        },
        /**
         * Do Reset Password
         */
        async RESET_PASSWORD({ commit }, data) {
            await axios.post('/api/auth/reset-telegram', {
                password: data.password,
                confirm_password: data.confirm_password,
                token: data.token
            })
                .then((res) => {
                    router.push({
                        name: `Login`,
                    });
                })
                .catch((err) => {
                    commit('SET_ERROR_RESET_PASSWORD', {
                        isError: true,
                        errorMessage: err.response.data.message
                    })
                })
        }
    }
}

export default auth