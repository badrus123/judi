import axios from "axios";
import { router } from "../../app.js"

const profile = {
    namespaced: true,
    state: {
        username: '',
        displayName: '',
        privacy: false,
        timezone: '',
        privacy: false,
        isLoading: false,
        error: {
            isError: false,
            errorMsg: ''
        }
    },
    getters: {
        username: state => {
            return state.username
        },
        displayName: state => {
            return state.displayName
        },
        privacy: state => {
            return state.privacy
        },
        timezone: state => {
            return state.timezone
        },
    },
    mutations: {
        SET_PROFILE(state, value) {
            if (value.username != undefined)
                state.username = value.username

            state.displayName = value.identifier
            state.privacy = value.hide_identifier

            if (value.timezone == '')
                state.timezone = "0"
            else
                state.timezone = parseInt(value.timezone)
        },
        SET_LOADING(state, value) {
            state.isLoading = value
        },
        SET_ERROR(state, value) {
            state.error.isError = value.isError
            state.error.errorMsg = value.errorMsg
        },
        RESET_ERROR(state, _) {
            state.error.isError = false
            state.error.errorMsg = ''
        }

    },
    actions: {
        /**
         * Get User Profile
         */
        GET_PROFILE({ commit }) {
            commit('SET_LOADING', true)

            axios.get('/api/profiles')
                .then((res) => {
                    commit('SET_PROFILE', res.data.data)
                    commit('SET_LOADING', false)
                })
                .catch((err) => {
                    commit('SET_ERROR', {
                        isError: true,
                        errorMsg: 'Server Error'
                    })
                })
        },
        /**
         * Update User Profile
         */
        UPDATE_PROFILE({ commit }, credentials) {

            // Set Loading & Reset Error
            commit('SET_LOADING', true)
            commit('RESET_ERROR')

            axios.put('/api/profiles', {
                display_name: credentials.displayName,
                privacy: credentials.privacy,
                timezone: credentials.timezone,
            })
                .then((res) => {
                    commit('SET_PROFILE', {
                        identifier: credentials.displayName,
                        hide_identifier: credentials.privacy,
                        timezone: credentials.timezone
                    })
                    commit('RESET_ERROR')
                    commit('SET_LOADING', false)

                })
                .catch((err) => {
                    let errorKey = Object.keys(err.response.data.errors)[0]

                    commit('SET_ERROR', {
                        isError: true,
                        errorMsg: err.response.data.errors[errorKey][0]
                    })

                    commit('SET_LOADING', false)
                })

        }

    }
}

export default profile