import axios from "axios";
import { router } from "../../app.js"

const transaction = {
    namespaced: true,
    state: {
        credit: [],
        debit: [],
        tableOptions: {
            sortable: ["date", "status"],
            filterable: false,
            perPage: 10,
            pagination: { show: true, chunk: 4, dropdown: false, nav: "fixed" },
            sortIcon: {
                base: "fa",
                is: "fa-sort",
                up: "fa-sort-asc",
                down: "fa-sort-desc",
            },

            showChildRowToggler: true,
            childRowTogglerFirst: true,
        },
        /**
         * Debit/Credit Options
         */
        columnDebitCredit: [
            "date",
            "transaction",
            "desc",
            "amount",
            "fees",
            "total",
            "status"
        ],
        headingDebitCredit: {
            date: "Date",
            transaction: "Transaction #",
            desc: "Description",
            amount: "Amount",
            fees: "Fees",
            total: "Total",
        },
        /**
         * Bets Options
         */
        columnBets: [
            "date",
            "transaction",
            "game",
            "desc",
            "amount",
            "wl",
            "status",
        ],
        headingBets: {
            date: "Date",
            transaction: "Transaction #",
            game: "Game-Event",
            desc: "Description",
            amount: "Amount",
            wl: "Win/Loss",
            status: "Status",
        },
        /**
         * Refer a Friend Options
         */
        columnFriends: [
            "date",
            "transaction #",
            "desc",
            "amount",
            "status",
        ],
        headingFriends: {
            date: "Date",
            transaction: "Transaction #",
            desc: "Description",
            amount: "Amount",
            status: "Status",
        },
    },
    getters: {
        creditTableData: state => {
            let tableData = [];
            state.credit.forEach(item => {
                tableData.push({
                    date: item.created_at,
                    transaction: item.hash,
                    desc: "",
                    amount: `${item.add_currency} ${parseFloat(item.add_amount)}`,
                    fees: `${item.add_currency} ${parseFloat(item.fee)}`,
                    total: `${item.add_currency} ${(parseFloat(item.add_amount) + parseFloat(item.fee))}`,
                    status: item.status,
                });
            });
            return tableData;
        },
        debitTableData: state => {
            let tableData = [];
            state.debit.forEach(item => {
                tableData.push({
                    date: item.created_at,
                    transaction: item.hash,
                    desc: "",
                    amount: `${item.deduct_currency} ${parseFloat(item.deduct_amount)}`,
                    fees: `${item.deduct_currency} ${parseFloat(item.fee)}`,
                    total: `${item.deduct_currency} ${(parseFloat(item.deduct_amount) + parseFloat(item.fee))}`,
                    status: item.status,
                });
            });
            return tableData;
        }
    },
    mutations: {
        SET_CREDIT(state, value) {
            state.credit = value
        },
        SET_DEBIT(state, value) {
            state.debit = value
        },
    },
    actions: {
        /**
         * Get Credit / deposit
         */
        async GET_CREDIT({ commit }, params) {
            if (params == undefined) params = {};
            params.type = 'deposit';
            await axios.get('/api/transactions', {
                params
            })
                .then((res) => {
                    commit('SET_CREDIT', res.data.data)
                })
                .catch((err) => {
                    // TODO: Handle Error
                    console.log(err.response)
                })
        },
        /**
         * Get Debit / withdraw
         */
        async GET_DEBIT({ commit }, params) {
            if (params == undefined) params = {};

            params.type = 'withdraw';
            await axios.get('/api/transactions', {
                params
            })
                .then((res) => {
                    commit('SET_DEBIT', res.data.data)
                })
                .catch((err) => {
                    // TODO: Handle Error
                    console.log(err.response)
                })
        },
    }
}

export default transaction