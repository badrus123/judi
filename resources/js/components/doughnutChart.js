import { Doughnut } from 'vue-chartjs'

var plugin = function(chart) {
  var width = chart.chart.width;
  var height = chart.chart.height;
  var ctx = chart.chart.ctx;

  ctx.restore();
  var fontSize = (height / 114).toFixed(2);
  ctx.font = fontSize + "em sans-serif";
  ctx.textBaseline = "middle";

  var text = 800;
  var textX = Math.round((width - ctx.measureText(text).width) / 2);
  var textY = height / 2;

   ctx.fillText(text, textX, textY);
   ctx.save();
}

export default {
  extends: Doughnut,

  data() {
    return {
    chartdata: {
      datasets: [{
        label: 'My First Dataset',
        data: [this.rateDay, 100-this.rateDay],

        backgroundColor: [
          'green',
          'red'
        ],
        borderColor:'transparent',
        borderRadius: 100,
        borderWidth: 0,
        weight: 0.5,
        offset: 100
      }],
    },
    options: {
      cutoutPercentage: 70,
      borderRadius:20
    },
  }
},
  props:{
    rateDay: Number
  },
  mounted () {
    this.addPlugin({
      id: 'my-plugin',
      beforeDraw: plugin,

    })
    this.renderChart(this.chartdata, this.options);
  }
}
