import { Line } from 'vue-chartjs'

// var plugin = function(chart) {
//     var ctx = chart.chart.ctx;
//     var gradient = ctx.createLinearGradient(0, 0, 0, 510);
//     gradient.addColorStop(0, 'rgba(250,174,50,1)');   
//     gradient.addColorStop(1, 'rgba(250,174,50,0)');
//     chart.data.datasets.backgroundColor = gradient;
//     //this.chartdata.datasets.backgroundColor= gradient;
// }

export default {
  extends: Line,
  data() {
    return {
    gradient: null,
    chartdata: {      
      labels: this.labelChart,
      datasets: [
        {         
          backgroundColor : this.gradient, //'#f87979'
          data: this.dataChart,
          borderColor: '#FFCD3E',
       
        }
      ]
      ,
    },
    options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        elements: {
            point:{
                radius: 0
            }
        },
        tooltips: {
            callbacks: {
              label: function(tooltipItem) {
                      return tooltipItem.yLabel;
              }
            }
        },
        // scaleShowLabels:false,
        // scaleFontSize:0,
        scales: {
          xAxes: [
            {
              time: {
                unit: 'Areas',
              },
              gridLines: {
                display: false,
                drawBorder: false,
                color:'#1D1E21',
              },
              ticks: {
                maxTicksLimit: 7,
                display: true,
                autoSkip: true,
                fontFamily: 'Open Sans',
                fontSize: 12,              
                fontColor: '#797979',
                paddingLeft: 20
              },
              //'dataset.maxBarThickness': 5,
            },
          ],
          yAxes: [
            {
            //   afterTickToLabelConversion: function(scaleInstance) {               
            //       scaleInstance.ticks[0] = null;
            //       scaleInstance.ticks[scaleInstance.ticks.length - 1] = null;
                  
            //       scaleInstance.ticksAsNumbers[0] = null;
            //       scaleInstance.ticksAsNumbers[scaleInstance.ticksAsNumbers.length - 1] = null;
            //     },
              time: {
                unit: 'Areas',
              },
              gridLines: {
                display: true,
                drawBorder: false,                
                color:'#EEEEEE',
                zeroLineWidth: 2,
                zeroLineColor: '#DDDDDD',
                
              },
              ticks: {              
                //precision: 5,
                //stepSize: 40,
                 // sampleSize: 1,
                // min: -100,
                // max: 100,
                display: true,
                beginAtZero: true,
                maxTicksLimit:8,
                autoSkip: true,
                fontFamily: 'Open Sans',
                fontSize: 12,              
                fontColor: '#797979',
                padding: 10,              
              },
              minor:{
                fontColor: 'yellow',
              },
              //'dataset.maxBarThickness': 5,
            },
          ],
          
        
        },
        
      },
  }
},
props:{    
    labelChart: Array,
    dataChart: Array
  },
  mounted () {
    // this.addPlugin({
    //     id: 'my-plugin',
    //     beforeDraw: plugin,
  
    //   })
    this.gradient = this.$refs.canvas.getContext('2d').createLinearGradient(0, 0, 0, 450);
    this.gradient.addColorStop(0, 'rgba(255, 0,0, 0.5)');
    this.gradient.addColorStop(0.5, 'rgba(255, 0, 0, 0.25)');
    this.gradient.addColorStop(1, 'rgba(255, 0, 0, 0)');
    
    this.renderChart(this.chartdata, this.options);
  }
}
