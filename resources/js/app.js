require('./bootstrap');
import Vue from "vue";
import VueRouter from 'vue-router'
Vue.use(VueRouter);

import routes from './routes'
import store from './store'
import App from "./components/App";
/*
*
* BOOTSTRAP VUE
*
 */
import {
    CollapsePlugin,
    NavPlugin,    
    ButtonPlugin,
    NavbarPlugin,
    ModalPlugin,
    CardPlugin,
    DropdownPlugin,
    ListGroupPlugin,
    InputGroupPlugin,
    FormInputPlugin,
    FormSelectPlugin,
    TabsPlugin,
    BadgePlugin,
    TooltipPlugin,
    FormCheckboxPlugin,
    JumbotronPlugin,
    CarouselPlugin,
    LayoutPlugin,
    PopoverPlugin,
    IconsPlugin,
    FormPlugin,
    FormGroupPlugin,
    AlertPlugin
} from 'bootstrap-vue'

Vue.use(CollapsePlugin)
Vue.use(ButtonPlugin)
Vue.use(NavPlugin)
Vue.use(NavbarPlugin)
Vue.use(ModalPlugin)
Vue.use(CardPlugin)
Vue.use(DropdownPlugin)
Vue.use(ListGroupPlugin)
Vue.use(InputGroupPlugin)
Vue.use(FormInputPlugin)
Vue.use(TabsPlugin)
Vue.use(BadgePlugin)
Vue.use(TooltipPlugin)
Vue.use(FormCheckboxPlugin)
Vue.use(FormSelectPlugin)
Vue.use(JumbotronPlugin)
Vue.use(CarouselPlugin)
Vue.use(LayoutPlugin)
Vue.use(IconsPlugin)
Vue.use(FormPlugin)
Vue.use(PopoverPlugin)
Vue.use(FormGroupPlugin)
Vue.use(AlertPlugin)
// Optionally install the BootstrapVue icon components plugin
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { ClientTable, ServerTable } from 'vue-tables-2'
Vue.use(ClientTable)
Vue.use(ServerTable)


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

export const router = new VueRouter({
    mode: 'history',
    routes
});
router.beforeEach((to, from, next) => {
    next()
});
window.BASE_API = '/api/'

const app = new Vue({
    el: '#app',
    router,
    store,
    render: (h) => h(App)
});

