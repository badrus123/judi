<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('hash')->unique();
            $table->unsignedBigInteger('user_id');
            $table->unsignedDecimal('add_amount', 20, 8)->default('0.00');
            $table->string('add_currency');
            $table->unsignedDecimal('deduct_amount', 20, 8)->default('0.00');
            $table->string('deduct_currency');
            $table->unsignedDecimal('fee', 20, 8)->default('0.00');
            $table->string('type');
            $table->string('status');
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
