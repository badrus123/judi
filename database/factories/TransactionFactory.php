<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Transaction;
use App\User;
use Faker\Generator as Faker;

$factory->define(Transaction::class, function (Faker $faker) {
    return [
        'hash' => uniqid(),
        'user_id' => factory(User::class)->create()->id,
        'add_amount' => "0.00000",
        'add_currency' => 'ALGO',
        "deduct_amount" => "0.00100000",
        "deduct_currency" => "ALGO",
        "fee" => "0.00100000",
        "type" => "withdraw",
        "status" => "completed",
    ];
});
