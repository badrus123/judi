<?php

use App\Models\SiteSetting;
use Illuminate\Database\Seeder;

class SiteSettingSeeder extends Seeder
{
    /**
                 * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'key' => 'currencies',
                'value' => json_encode([
                    'ALGO', 'ETH'
                ])
            ],
            [
                /**
                 * Algorand Withdrawal Minimum
                 */
                'key' => 'algo_withdrawal_min',
                'value' => 1
            ],
            [
                /**
                 * Minimum Balance Left, 
                 * check when doing the withdrawal
                 */
                'key' => 'algo_min_balance_left',
                'value' => '1'
            ],
            [
                /**
                 * Algorand Withdrawal Fees 
                 */
                'key' => 'algo_withdrawal_fees',
                'value' => 0.001
            ]
        ];

        foreach ($data as $item) SiteSetting::firstOrCreate($item);
    }
}
