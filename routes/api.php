<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthenticationController@register');
    Route::post('login', 'AuthenticationController@login');
    Route::post('reset-password', 'ResetPasswordController@request');
    Route::post('reset-telegram', 'ResetPasswordController@telegramUpdate');
});

Route::middleware(['auth:api'])->group(function () {

    // Reset Password
    Route::post('password/reset-setting', 'ResetPasswordController@settingUpdate');

    // Profile
    Route::get('/profiles', 'Profile\ProfileController@show');
    Route::put('/profiles', 'Profile\ProfileController@update');
    Route::delete('/profiles', 'Profile\ProfileController@destroy');

    // Profile - 2FA
    Route::get('/profiles/2fa', 'Profile\Profile2FAController@show');
    Route::post('/profiles/2fa', 'Profile\Profile2FAController@store');
    Route::delete('/profiles/2fa', 'Profile\Profile2FAController@destroy');

    // Profile - Telegram
    Route::put('/profiles/telegram', 'Profile\ProfileTelegramController@update');
    Route::delete('/profiles/telegram', 'Profile\ProfileTelegramController@destroy');

    // Wallet
    Route::get('/wallets/addresses', 'Wallet\WalletController@address');
    Route::get('/wallets/balances', 'Wallet\WalletController@balance');
    Route::post('/wallets/withdraw', 'Wallet\WalletWithdrawController@store');

    // Websocket
    Route::get('/websockets/session', 'Websocket\WebsocketController@session');

    // Affiliate
    Route::get('/affiliates/code', 'Affiliate\AffiliateController@code');

    // Transactions
    Route::get('/transactions', 'Transaction\TransactionController@index');

    // Notifications
    Route::get('/notifications', 'Notification\NotificationController@index');
    Route::get('/notifications/unread', 'Notification\NotificationController@unread');
    Route::post('/notifications/readAll', 'Notification\NotificationController@readAll');
});

// Site Settings
Route::get('/site-settings', 'SiteSettingController@index');
Route::get('/site-settings/{key}', 'SiteSettingController@show');

Route::post('/telegram/callback', 'Telegram\TelegramCallback@index');
