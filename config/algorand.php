<?php

return [
    'ip' => env('ALGORAND_IP'),
    'email' => env('ALGORAND_EMAIL'),
    'password' => env('ALGORAND_PASSWORD'),
];
