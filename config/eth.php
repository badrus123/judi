<?php

return [
    'ip' => env('ETH_IP'),
    'email' => env('ETH_EMAIL'),
    'password' => env('ETH_PASSWORD'),
];
