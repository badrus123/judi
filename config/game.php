<?php

return [
    'ip' => env('GAME_IP'),
    'email' => env('GAME_EMAIL'),
    'password' => env('GAME_PASSWORD')
];
