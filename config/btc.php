<?php

return [
    'ip' => env('BTC_IP'),
    'email' => env('BTC_EMAIL'),
    'password' => env('BTC_PASSWORD'),
];
