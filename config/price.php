<?php

return [
    'ip' => env('PRICE_IP'),
    'email' => env('PRICE_EMAIL'),
    'password' => env('PRICE_PASSWORD'),
];
