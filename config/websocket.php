<?php

return [
    'ip' => env('WEBSOCKET_IP'),
    'email' => env('WEBSOCKET_EMAIL'),
    'password' => env('WEBSOCKET_PASSWORD')
];
