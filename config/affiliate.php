<?php

return [
    'ip' => env('AFFILIATE_IP'),
    'email' => env('AFFILIATE_EMAIL'),
    'password' => env('AFFILIATE_PASSWORD')
];
