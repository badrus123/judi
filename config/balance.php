<?php

return [
    'ip' => env('BALANCE_IP'),
    'email' => env('BALANCE_EMAIL'),
    'password' => env('BALANCE_PASSWORD'),
];
